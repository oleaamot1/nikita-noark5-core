-- Record / RegistryEntry
insert into system_id_entity(system_id, created_date, created_by, owned_by, version)
values ('8f6b084f-d727-4b46-bbe2-14bed2135fa9', '2019-04-08 00:00:00', 'admin@example.com',
        'e23e9106-aab3-426c-ac7b-65c65bfc1a85', 0);

-- Record / RecordNote
insert into system_id_entity(system_id, created_date, created_by, owned_by, version)
values ('11b32a9e-802d-43de-9bb5-c951e3bbe95b', '2019-04-08 00:00:00', 'admin@example.com',
        'e23e9106-aab3-426c-ac7b-65c65bfc1a85', 0);

-- DocumentFlow / RegistryEntry
insert into system_id_entity(system_id, created_date, created_by, owned_by, version)
values ('cf0f41f7-65e8-4471-85f3-18ff223cbdb0', '2019-04-08 00:00:00', 'admin@example.com',
        'e23e9106-aab3-426c-ac7b-65c65bfc1a85', 0);

insert into system_id_entity(system_id, created_date, created_by, owned_by, version)
values ('5f117c28-8a33-4d65-bc5a-8e74b3ba635b', '2019-04-08 00:00:00', 'admin@example.com',
        'e23e9106-aab3-426c-ac7b-65c65bfc1a85', 0);
insert into system_id_entity(system_id, created_date, created_by, owned_by, version)
values ('554e1e9e-db86-4f1c-a505-43bc76207b09', '2019-04-08 00:00:00', 'admin@example.com',
        'e23e9106-aab3-426c-ac7b-65c65bfc1a85', 0);


