delete
from as_document_flow
where system_id = 'cf0f41f7-65e8-4471-85f3-18ff223cbdb0';
delete
from as_document_flow
where system_id = 'cf0f41f7-65e8-4471-85f3-18ff223cbdb0';
delete
from as_document_flow
where system_id = 'cf0f41f7-65e8-4471-85f3-18ff223cbdb0';
delete
from as_document_flow
where system_id = 'cf0f41f7-65e8-4471-85f3-18ff223cbdb0';
delete
from as_document_flow
where system_id = '554e1e9e-db86-4f1c-a505-43bc76207b09';
delete
from as_document_flow
where system_id = '5f117c28-8a33-4d65-bc5a-8e74b3ba635b';

-- delete joins

delete
from as_record_document_description
where f_pk_record_id = 'dc600862-3298-4ec0-8541-3e51fb900054';
delete
from as_record_document_description
where f_pk_record_id = 'dc600862-3298-4ec0-8541-3e51fb900054';
delete
from as_series_classification_system
where f_pk_series_id = 'f1102ae8-6c4c-4d93-aaa5-7c6220e50c4d';

-- delete arkivenheter

delete
from as_document_description
where system_id = '66b92e78-b75d-4b0f-9558-4204ab31c2d1';
delete
from as_document_description
where system_id = '9493c357-1345-4a55-98fd-fcba13b8a6dd';
delete
from as_correspondence_part_person
where system_id = '7f000101-730c-1c94-8173-0c0ded71003c';
delete
from as_correspondence_part
where system_id = '7f000101-730c-1c94-8173-0c0ded71003c';
delete
from as_part_person
where system_id = '8131049d-dcac-43d8-bee4-656e72842da9';
delete
from as_part_unit
where system_id = 'ebcefc44-73e5-485e-94c9-1b210359c125';
delete
from as_part
where system_id = '8131049d-dcac-43d8-bee4-656e72842da9';
delete
from as_part
where system_id = 'ebcefc44-73e5-485e-94c9-1b210359c125';
delete
from as_record_part
where system_id = 'dc600862-3298-4ec0-8541-3e51fb900054';

delete
from ad_administrative_unit
where system_id = 'c3d4affc-66a0-4663-b63a-6ecc4f3d6009';

delete
from sa_registry_entry
where system_id = '8f6b084f-d727-4b46-bbe2-14bed2135fa9';
delete
from sa_record_note
where system_id = '11b32a9e-802d-43de-9bb5-c951e3bbe95b';
delete
from as_record
where system_id = '8f6b084f-d727-4b46-bbe2-14bed2135fa9';
delete
from as_record
where system_id = '11b32a9e-802d-43de-9bb5-c951e3bbe95b';
delete
from as_record
where system_id = 'dc600862-3298-4ec0-8541-3e51fb900054';

delete
from as_file_part
where system_id = 'f1677c47-99e1-42a7-bda2-b0bbc64841b7';
delete
from as_file_part
where system_id = '43d305de-b3c8-4922-86fd-45bd26f3bf01';

delete
from sa_sequence_generator
where system_id = 'c3d4affc-66a0-4663-b63a-6ecc4f3d6009';
delete
from sa_case_file
where system_id = 'fed888c6-83e1-4ed0-922a-bd5770af3fad';

delete
from as_file
where system_id = '43d305de-b3c8-4922-86fd-45bd26f3bf01';
delete
from as_file
where system_id = 'fed888c6-83e1-4ed0-922a-bd5770af3fad';
delete
from as_file
where system_id = 'f1677c47-99e1-42a7-bda2-b0bbc64841b7';

delete
from as_classification_system
where system_id = '2d0b2dc1-f3bb-4239-bf04-582b1085581c'

delete
from as_series
where system_id = 'f1102ae8-6c4c-4d93-aaa5-7c6220e50c4d';
delete
from as_series
where system_id = 'f32c1fa0-8e42-4236-8f40-e006940ea70b';
delete
from as_fonds
where system_id = '3318a63f-11a7-4ec9-8bf1-4144b7f281cf';

-- delete systemID objects
delete
from ad_organisation
where system_id = 'e23e9106-aab3-426c-ac7b-65c65bfc1a85';
delete
from system_id_entity
where system_id = 'e23e9106-aab3-426c-ac7b-65c65bfc1a85';
delete
from system_id_entity
where system_id = '3318a63f-11a7-4ec9-8bf1-4144b7f281cf';
delete
from system_id_entity
where system_id = 'f1102ae8-6c4c-4d93-aaa5-7c6220e50c4d';
delete
from system_id_entity
where system_id = 'f1677c47-99e1-42a7-bda2-b0bbc64841b7';
delete
from system_id_entity
where system_id = '43d305de-b3c8-4922-86fd-45bd26f3bf01';
delete
from system_id_entity
where system_id = 'fed888c6-83e1-4ed0-922a-bd5770af3fad';
delete
from system_id_entity
where system_id = '2d0b2dc1-f3bb-4239-bf04-582b1085581c';
delete
from system_id_entity
where system_id = 'dc600862-3298-4ec0-8541-3e51fb900054';
delete
from system_id_entity
where system_id = '7f000101-730c-1c94-8173-0c0ded71003c';
delete
from system_id_entity
where system_id = '8131049d-dcac-43d8-bee4-656e72842da9';
delete
from system_id_entity
where system_id = '8f6b084f-d727-4b46-bbe2-14bed2135fa9';
delete
from system_id_entity
where system_id = '11b32a9e-802d-43de-9bb5-c951e3bbe95b';
delete
from system_id_entity
where system_id = 'cf0f41f7-65e8-4471-85f3-18ff223cbdb0';
delete
from system_id_entity
where system_id = '5f117c28-8a33-4d65-bc5a-8e74b3ba635b';
delete
from system_id_entity
where system_id = '554e1e9e-db86-4f1c-a505-43bc76207b09';
delete
from system_id_entity
where system_id = 'e9eef604-c540-456e-bf46-62ddddaa68af';
delete
from system_id_entity
where system_id = '66b92e78-b75d-4b0f-9558-4204ab31c2d1';
delete
from system_id_entity
where system_id = '9493c357-1345-4a55-98fd-fcba13b8a6dd';
delete
from system_id_entity
where system_id = 'ebcefc44-73e5-485e-94c9-1b210359c125';
delete
from system_id_entity
where system_id = 'f32c1fa0-8e42-4236-8f40-e006940ea70b';
delete
from system_id_entity
where system_id = 'c3d4affc-66a0-4663-b63a-6ecc4f3d6009';
