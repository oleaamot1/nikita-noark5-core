package app.domain.noark5.metadata;

import jakarta.persistence.Entity;
import jakarta.persistence.Inheritance;
import jakarta.persistence.Table;

import static app.utils.constants.Constants.REL_METADATA_CASE_STATUS;
import static app.utils.constants.Constants.TABLE_CASE_STATUS;
import static app.utils.constants.N5ResourceMappings.CASE_STATUS;
import static jakarta.persistence.InheritanceType.SINGLE_TABLE;

// Noark 5v5 saksstatus
@Entity
@Inheritance(strategy = SINGLE_TABLE)
@Table(name = TABLE_CASE_STATUS)
public class CaseStatus
        extends Metadata {

    private static final long serialVersionUID = 1L;

    public CaseStatus() {
    }

    public CaseStatus(String code, String codename) {
        super(code, codename);
    }

    public CaseStatus(String code) {
        super(code, (String)null);
    }

    @Override
    public String getBaseTypeName() {
        return CASE_STATUS;
    }

    @Override
    public String getBaseRel() {
        return REL_METADATA_CASE_STATUS;
    }
}
