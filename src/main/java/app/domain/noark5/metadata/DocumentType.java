package app.domain.noark5.metadata;

import jakarta.persistence.Entity;
import jakarta.persistence.Inheritance;
import jakarta.persistence.Table;

import static app.utils.constants.Constants.REL_METADATA_DOCUMENT_TYPE;
import static app.utils.constants.Constants.TABLE_DOCUMENT_TYPE;
import static app.utils.constants.N5ResourceMappings.DOCUMENT_TYPE;
import static jakarta.persistence.InheritanceType.SINGLE_TABLE;

// Noark 5v5 dokumenttype
@Entity
@Inheritance(strategy = SINGLE_TABLE)
@Table(name = TABLE_DOCUMENT_TYPE)
public class DocumentType
        extends Metadata {

    private static final long serialVersionUID = 1L;

    public DocumentType() {
    }

    public DocumentType(String code, String codename) {
        super(code, codename);
    }

    public DocumentType(String code) {
        super(code, (String)null);
    }

    @Override
    public String getBaseTypeName() {
        return DOCUMENT_TYPE;
    }

    @Override
    public String getBaseRel() {
        return REL_METADATA_DOCUMENT_TYPE;
    }
}
