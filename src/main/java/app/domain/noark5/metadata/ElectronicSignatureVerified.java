package app.domain.noark5.metadata;

import jakarta.persistence.Entity;
import jakarta.persistence.Inheritance;
import jakarta.persistence.Table;

import static app.utils.constants.Constants.REL_METADATA_ELECTRONIC_SIGNATURE_VERIFIED;
import static app.utils.constants.Constants.TABLE_ELECTRONIC_SIGNATURE_VERIFIED;
import static app.utils.constants.N5ResourceMappings.ELECTRONIC_SIGNATURE_VERIFIED;
import static jakarta.persistence.InheritanceType.SINGLE_TABLE;

// Noark 5v5 elektroniskSignaturVerifisert
@Entity
@Inheritance(strategy = SINGLE_TABLE)
@Table(name = TABLE_ELECTRONIC_SIGNATURE_VERIFIED)
public class ElectronicSignatureVerified
        extends Metadata {

    private static final long serialVersionUID = 1L;

    public ElectronicSignatureVerified() {
    }

    public ElectronicSignatureVerified(String code, String codename) {
        super(code, codename);
    }

    public ElectronicSignatureVerified(String code) {
        super(code, (String)null);
    }

    @Override
    public String getBaseTypeName() {
        return ELECTRONIC_SIGNATURE_VERIFIED;
    }

    @Override
    public String getBaseRel() {
        return REL_METADATA_ELECTRONIC_SIGNATURE_VERIFIED;
    }
}
