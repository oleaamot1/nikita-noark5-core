package app.domain.noark5.metadata;

import jakarta.persistence.Entity;
import jakarta.persistence.Inheritance;
import jakarta.persistence.Table;

import static app.utils.constants.Constants.REL_METADATA_REGISTRY_ENTRY_TYPE;
import static app.utils.constants.Constants.TABLE_REGISTRY_ENTRY_TYPE;
import static app.utils.constants.N5ResourceMappings.REGISTRY_ENTRY_TYPE;
import static jakarta.persistence.InheritanceType.SINGLE_TABLE;

// Noark 5v5 journaltype
@Entity
@Inheritance(strategy = SINGLE_TABLE)
@Table(name = TABLE_REGISTRY_ENTRY_TYPE)
public class RegistryEntryType
        extends Metadata {

    private static final long serialVersionUID = 1L;

    public RegistryEntryType() {
    }

    public RegistryEntryType(String code, String codename) {
        super(code, codename);
    }

    public RegistryEntryType(String code) {
        super(code, (String)null);
    }

    @Override
    public String getBaseTypeName() {
        return REGISTRY_ENTRY_TYPE;
    }

    @Override
    public String getBaseRel() {
        return REL_METADATA_REGISTRY_ENTRY_TYPE;
    }
}
