package app.domain.noark5.metadata;

import jakarta.persistence.Entity;
import jakarta.persistence.Inheritance;
import jakarta.persistence.Table;

import static app.utils.constants.Constants.REL_METADATA_CLASSIFICATION_TYPE;
import static app.utils.constants.Constants.TABLE_CLASSIFICATION_TYPE;
import static app.utils.constants.N5ResourceMappings.CLASSIFICATION_TYPE;
import static jakarta.persistence.InheritanceType.SINGLE_TABLE;

// Noark 5v5 Klassifikasjonstype
@Entity
@Inheritance(strategy = SINGLE_TABLE)
@Table(name = TABLE_CLASSIFICATION_TYPE)
public class ClassificationType
        extends Metadata {

    private static final long serialVersionUID = 1L;

    public ClassificationType() {
    }

    public ClassificationType(String code, String codename) {
        super(code, codename);
    }

    public ClassificationType(String code) {
        super(code, (String)null);
    }

    @Override
    public String getBaseTypeName() {
        return CLASSIFICATION_TYPE;
    }

    @Override
    public String getBaseRel() {
        return REL_METADATA_CLASSIFICATION_TYPE;
    }
}
