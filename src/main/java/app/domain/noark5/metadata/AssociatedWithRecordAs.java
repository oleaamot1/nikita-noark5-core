package app.domain.noark5.metadata;

import jakarta.persistence.Entity;
import jakarta.persistence.Inheritance;
import jakarta.persistence.Table;

import static app.utils.constants.Constants.REL_METADATA_ASSOCIATED_WITH_RECORD_AS;
import static app.utils.constants.Constants.TABLE_ASSOCIATED_WITH_RECORD_AS;
import static app.utils.constants.N5ResourceMappings.ASSOCIATED_WITH_RECORD_AS;
import static jakarta.persistence.InheritanceType.SINGLE_TABLE;

// Noark 5v5 TilknyttetRegistreringSom
@Entity
@Inheritance(strategy = SINGLE_TABLE)
@Table(name = TABLE_ASSOCIATED_WITH_RECORD_AS)
public class AssociatedWithRecordAs
        extends Metadata {

    private static final long serialVersionUID = 1L;

    public AssociatedWithRecordAs() {
    }

    public AssociatedWithRecordAs(String code, String codename) {
        super(code, codename);
    }

    public AssociatedWithRecordAs(String code) {
        super(code, (String)null);
    }

    @Override
    public String getBaseTypeName() {
        return ASSOCIATED_WITH_RECORD_AS;
    }

    @Override
    public String getBaseRel() {
        return REL_METADATA_ASSOCIATED_WITH_RECORD_AS;
    }
}
