package app.domain.noark5.metadata;

import jakarta.persistence.Entity;
import jakarta.persistence.Inheritance;
import jakarta.persistence.Table;

import static app.utils.constants.Constants.REL_METADATA_DOCUMENT_MEDIUM;
import static app.utils.constants.Constants.TABLE_DOCUMENT_MEDIUM;
import static app.utils.constants.N5ResourceMappings.DOCUMENT_MEDIUM;
import static jakarta.persistence.InheritanceType.SINGLE_TABLE;

// Noark 5v5 dokumentmedium
@Entity
@Inheritance(strategy = SINGLE_TABLE)
@Table(name = TABLE_DOCUMENT_MEDIUM)
public class DocumentMedium
        extends Metadata {

    private static final long serialVersionUID = 1L;

    public DocumentMedium() {
    }

    public DocumentMedium(String code, String codename) {
        super(code, codename);
    }

    public DocumentMedium(String code) {
        super(code, (String)null);
    }

    @Override
    public String getBaseTypeName() {
        return DOCUMENT_MEDIUM;
    }

    @Override
    public String getBaseRel() {
        return REL_METADATA_DOCUMENT_MEDIUM;
    }
}
