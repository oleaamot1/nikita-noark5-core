package app.domain.noark5.metadata;

import jakarta.persistence.Entity;
import jakarta.persistence.Inheritance;
import jakarta.persistence.Table;

import static app.utils.constants.Constants.TABLE_MEETING_REGISTRATION_TYPE;
import static app.utils.constants.N5ResourceMappings.MEETING_REGISTRATION_TYPE;
import static jakarta.persistence.InheritanceType.SINGLE_TABLE;

// Noark 5v5 Møteregistreringstype
@Entity
@Inheritance(strategy = SINGLE_TABLE)
@Table(name = TABLE_MEETING_REGISTRATION_TYPE)
public class MeetingRegistrationType
        extends Metadata {

    private static final long serialVersionUID = 1L;

    public MeetingRegistrationType() {
    }

    public MeetingRegistrationType(String code, String codename) {
        super(code, codename);
    }

    public MeetingRegistrationType(String code) {
        super(code, (String)null);
    }

    @Override
    public String getBaseTypeName() {
        return MEETING_REGISTRATION_TYPE;
    }
}
