package app.domain.noark5.metadata;


import jakarta.persistence.Entity;
import jakarta.persistence.Inheritance;
import jakarta.persistence.Table;

import static app.utils.constants.Constants.REL_METADATA_FONDS_STATUS;
import static app.utils.constants.Constants.TABLE_FONDS_STATUS;
import static app.utils.constants.N5ResourceMappings.FONDS_STATUS;
import static jakarta.persistence.InheritanceType.SINGLE_TABLE;

// Noark 5v5 arkivstatus
@Entity
@Inheritance(strategy = SINGLE_TABLE)
@Table(name = TABLE_FONDS_STATUS)
public class FondsStatus
        extends Metadata {

    private static final long serialVersionUID = 1L;

    public FondsStatus() {
    }

    public FondsStatus(String code, String codename) {
        super(code, codename);
    }

    public FondsStatus(String code) {
        super(code, (String)null);
    }

    @Override
    public String getBaseTypeName() {
        return FONDS_STATUS;
    }

    @Override
    public String getBaseRel() {
        return REL_METADATA_FONDS_STATUS;
    }
}
