package app.domain.noark5.metadata;

import jakarta.persistence.Entity;
import jakarta.persistence.Inheritance;
import jakarta.persistence.Table;

import static app.utils.constants.Constants.REL_METADATA_COORDINATE_SYSTEM;
import static app.utils.constants.Constants.TABLE_COORDINATE_SYSTEM;
import static app.utils.constants.N5ResourceMappings.COORDINATE_SYSTEM;
import static jakarta.persistence.InheritanceType.SINGLE_TABLE;

// Noark 5v5 Koordinatsystem
@Entity
@Inheritance(strategy = SINGLE_TABLE)
@Table(name = TABLE_COORDINATE_SYSTEM)
public class CoordinateSystem extends Metadata {

    private static final long serialVersionUID = 1L;

    public CoordinateSystem() {
    }

    public CoordinateSystem(String code, String codename) {
        super(code, codename);
    }

    public CoordinateSystem(String code) {
        super(code, (String)null);
    }

    @Override
    public String getBaseTypeName() {
        return COORDINATE_SYSTEM;
    }

    @Override
    public String getBaseRel() {
        return REL_METADATA_COORDINATE_SYSTEM;
    }
}
