package app.domain.noark5.metadata;

import jakarta.persistence.Entity;
import jakarta.persistence.Inheritance;
import jakarta.persistence.Table;

import static app.utils.constants.Constants.REL_METADATA_ACCESS_CATEGORY;
import static app.utils.constants.Constants.TABLE_ACCESS_CATEGORY;
import static app.utils.constants.N5ResourceMappings.ACCESS_CATEGORY;
import static jakarta.persistence.InheritanceType.SINGLE_TABLE;

// Noark 5v5 Tilgangskategori
@Entity
@Inheritance(strategy = SINGLE_TABLE)
@Table(name = TABLE_ACCESS_CATEGORY)
public class AccessCategory
        extends Metadata {

    private static final long serialVersionUID = 1L;

    public AccessCategory() {
    }

    public AccessCategory(String code, String codename) {
        super(code, codename);
    }

    public AccessCategory(String code) {
        super(code, (String)null);
    }

    @Override
    public String getBaseTypeName() {
        return ACCESS_CATEGORY;
    }

    @Override
    public String getBaseRel() {
        return REL_METADATA_ACCESS_CATEGORY;
    }
}
