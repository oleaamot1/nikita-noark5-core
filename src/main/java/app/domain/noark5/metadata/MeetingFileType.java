package app.domain.noark5.metadata;

import jakarta.persistence.Entity;
import jakarta.persistence.Inheritance;
import jakarta.persistence.Table;

import static app.utils.constants.Constants.TABLE_MEETING_FILE_TYPE;
import static app.utils.constants.N5ResourceMappings.MEETING_FILE_TYPE;
import static jakarta.persistence.InheritanceType.SINGLE_TABLE;

// Noark 5v5 Møtesakstype
@Entity
@Inheritance(strategy = SINGLE_TABLE)
@Table(name = TABLE_MEETING_FILE_TYPE)
public class MeetingFileType
        extends Metadata {

    private static final long serialVersionUID = 1L;

    public MeetingFileType() {
    }

    public MeetingFileType(String code, String codename) {
        super(code, codename);
    }

    public MeetingFileType(String code) {
        super(code, (String)null);
    }

    @Override
    public String getBaseTypeName() {
        return MEETING_FILE_TYPE;
    }
}
