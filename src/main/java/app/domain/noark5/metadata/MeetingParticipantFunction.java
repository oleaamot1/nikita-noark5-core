package app.domain.noark5.metadata;

import jakarta.persistence.Entity;
import jakarta.persistence.Inheritance;
import jakarta.persistence.Table;

import static app.utils.constants.Constants.TABLE_MEETING_PARTICIPANT_FUNCTION;
import static app.utils.constants.N5ResourceMappings.MEETING_PARTICIPANT_FUNCTION;
import static jakarta.persistence.InheritanceType.SINGLE_TABLE;

// Noark 5v5 Møtedeltakerfunksjon
@Entity
@Inheritance(strategy = SINGLE_TABLE)
@Table(name = TABLE_MEETING_PARTICIPANT_FUNCTION)
public class MeetingParticipantFunction
        extends Metadata {

    private static final long serialVersionUID = 1L;

    public MeetingParticipantFunction() {
    }

    public MeetingParticipantFunction(String code, String codename) {
        super(code, codename);
    }

    public MeetingParticipantFunction(String code) {
        super(code, (String)null);
    }

    @Override
    public String getBaseTypeName() {
        return MEETING_PARTICIPANT_FUNCTION;
    }
}
