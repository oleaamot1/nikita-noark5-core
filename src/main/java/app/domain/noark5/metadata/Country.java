package app.domain.noark5.metadata;

import jakarta.persistence.Entity;
import jakarta.persistence.Inheritance;
import jakarta.persistence.Table;

import static app.utils.constants.Constants.REL_METADATA_COUNTRY;
import static app.utils.constants.Constants.TABLE_COUNTRY;
import static app.utils.constants.N5ResourceMappings.COUNTRY;
import static jakarta.persistence.InheritanceType.SINGLE_TABLE;

// Noark 5v5 Land
@Entity
@Inheritance(strategy = SINGLE_TABLE)
@Table(name = TABLE_COUNTRY)
public class Country extends Metadata {

    private static final long serialVersionUID = 1L;

    public Country() {
    }

    public Country(String code, String codename) {
        super(code, codename);
    }

    public Country(String code) {
        super(code, (String)null);
    }

    @Override
    public String getBaseTypeName() {
        return COUNTRY;
    }

    @Override
    public String getBaseRel() {
        return REL_METADATA_COUNTRY;
    }
}
