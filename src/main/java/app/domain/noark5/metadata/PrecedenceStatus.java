package app.domain.noark5.metadata;

import jakarta.persistence.Entity;
import jakarta.persistence.Inheritance;
import jakarta.persistence.Table;

import static app.utils.constants.Constants.REL_METADATA_PRECEDENCE_STATUS;
import static app.utils.constants.Constants.TABLE_PRECEDENCE_STATUS;
import static app.utils.constants.N5ResourceMappings.PRECEDENCE_STATUS;
import static jakarta.persistence.InheritanceType.SINGLE_TABLE;

// Noark 5v5 Presedensstatus
@Entity
@Inheritance(strategy = SINGLE_TABLE)
@Table(name = TABLE_PRECEDENCE_STATUS)
public class PrecedenceStatus
        extends Metadata {

    private static final long serialVersionUID = 1L;

    public PrecedenceStatus() {
    }

    public PrecedenceStatus(String code, String codename) {
        super(code, codename);
    }

    public PrecedenceStatus(String code) {
        super(code, (String)null);
    }

    @Override
    public String getBaseTypeName() {
        return PRECEDENCE_STATUS;
    }

    @Override
    public String getBaseRel() {
        return REL_METADATA_PRECEDENCE_STATUS;
    }
}
