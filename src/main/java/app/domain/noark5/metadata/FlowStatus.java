package app.domain.noark5.metadata;

import jakarta.persistence.Entity;
import jakarta.persistence.Inheritance;
import jakarta.persistence.Table;

import static app.utils.constants.Constants.REL_METADATA_FLOW_STATUS;
import static app.utils.constants.Constants.TABLE_FLOW_STATUS;
import static app.utils.constants.N5ResourceMappings.FLOW_STATUS;
import static jakarta.persistence.InheritanceType.SINGLE_TABLE;

// Noark 5v5 Flytstatus
@Entity
@Inheritance(strategy = SINGLE_TABLE)
@Table(name = TABLE_FLOW_STATUS)
public class FlowStatus
        extends Metadata {

    private static final long serialVersionUID = 1L;

    public FlowStatus() {
    }

    public FlowStatus(String code, String codename) {
        super(code, codename);
    }

    public FlowStatus(String code) {
        super(code, (String)null);
    }

    @Override
    public String getBaseTypeName() {
        return FLOW_STATUS;
    }

    @Override
    public String getBaseRel() {
        return REL_METADATA_FLOW_STATUS;
    }
}
