package app.domain.noark5.metadata;

import jakarta.persistence.Entity;
import jakarta.persistence.Inheritance;
import jakarta.persistence.Table;

import static app.utils.constants.Constants.REL_METADATA_CORRESPONDENCE_PART_TYPE;
import static app.utils.constants.Constants.TABLE_CORRESPONDENCE_PART_TYPE;
import static app.utils.constants.N5ResourceMappings.CORRESPONDENCE_PART_TYPE;
import static jakarta.persistence.InheritanceType.SINGLE_TABLE;

// Noark 5v5 korrespondanseparttype
@Entity
@Inheritance(strategy = SINGLE_TABLE)
@Table(name = TABLE_CORRESPONDENCE_PART_TYPE)
//@Audited
public class CorrespondencePartType
        extends Metadata {

    private static final long serialVersionUID = 1L;

    public CorrespondencePartType() {
    }

    public CorrespondencePartType(String code, String codename) {
        super(code, codename);
    }

    public CorrespondencePartType(String code) {
        super(code, (String)null);
    }

    @Override
    public String getBaseTypeName() {
        return CORRESPONDENCE_PART_TYPE;
    }

    @Override
    public String getBaseRel() {
        return REL_METADATA_CORRESPONDENCE_PART_TYPE;
    }
}
