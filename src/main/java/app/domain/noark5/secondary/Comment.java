package app.domain.noark5.secondary;

import app.domain.annotation.LinksObject;
import app.domain.annotation.LinksPacker;
import app.domain.interfaces.entities.secondary.ICommentEntity;
import app.domain.noark5.DocumentDescription;
import app.domain.noark5.File;
import app.domain.noark5.RecordEntity;
import app.domain.noark5.SystemIdEntity;
import app.domain.noark5.metadata.CommentType;
import app.webapp.payload.builder.noark5.secondary.CommentLinksBuilder;
import app.webapp.payload.deserializers.noark5.secondary.CommentDeserializer;
import app.webapp.payload.links.secondary.CommentLinks;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.Table;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.OffsetDateTime;
import java.util.HashSet;
import java.util.Set;

import static app.utils.constants.Constants.REL_FONDS_STRUCTURE_COMMENT;
import static app.utils.constants.Constants.TABLE_COMMENT;
import static app.utils.constants.N5ResourceMappings.*;
import static org.springframework.format.annotation.DateTimeFormat.ISO.DATE_TIME;

@Entity
@Table(name = TABLE_COMMENT)
@JsonDeserialize(using = CommentDeserializer.class)
@LinksPacker(using = CommentLinksBuilder.class)
@LinksObject(using = CommentLinks.class)

public class Comment
        extends SystemIdEntity
        implements ICommentEntity {

    private static final long serialVersionUID = 1L;

    /**
     * M310 - merknadstekst (xs:string)
     */
    @Column(name = COMMENT_TEXT_ENG, length = 8192)
    @JsonProperty(COMMENT_TEXT)
    //@Audited
    private String commentText;

    /**
     * M??? - merknadstype code (xs:string)
     */
    @Column(name = COMMENT_TYPE_CODE_ENG)
    @JsonProperty(COMMENT_TYPE_CODE)
    //@Audited
    private String commentTypeCode;

    /**
     * M084 - merknadstype code name (xs:string)
     */
    @Column(name = COMMENT_TYPE_CODE_NAME_ENG)
    @JsonProperty(COMMENT_TYPE_CODE_NAME)
    //@Audited
    private String commentTypeCodeName;

    /**
     * M611 - merknadsdato (xs:dateTime)
     */
    @Column(name = COMMENT_TIME_ENG)
    @JsonProperty(COMMENT_TIME)
    @DateTimeFormat(iso = DATE_TIME)
    //@Audited
    private OffsetDateTime commentDate;

    /**
     * M612 - merknadRegistrertAv (xs:string)
     */
    @Column(name = COMMENT_REGISTERED_BY_ENG)
    @JsonProperty(COMMENT_REGISTERED_BY)
    //@Audited

    private String commentRegisteredBy;

    // Link to File
    @ManyToMany(mappedBy = REFERENCE_COMMENT)
    private final Set<File> referenceFile = new HashSet<>();

    // Links to Record
    @ManyToMany(mappedBy = REFERENCE_COMMENT)
    private final Set<RecordEntity> referenceRecordEntity = new HashSet<>();

    // Link to DocumentDescription
    @ManyToMany(mappedBy = REFERENCE_COMMENT)
    private final Set<DocumentDescription> referenceDocumentDescription =
            new HashSet<>();

    public String getCommentText() {
        return commentText;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    public CommentType getCommentType() {
        if (null == commentTypeCode)
            return null;
        return new CommentType(commentTypeCode, commentTypeCodeName);
    }

    public void setCommentType(CommentType commentType) {
        if (null != commentType) {
            this.commentTypeCode = commentType.getCode();
            this.commentTypeCodeName = commentType.getCodeName();
        } else {
            this.commentTypeCode = null;
            this.commentTypeCodeName = null;
        }
    }

    public OffsetDateTime getCommentDate() {
        return commentDate;
    }

    public void setCommentDate(OffsetDateTime commentDate) {
        this.commentDate = commentDate;
    }

    public String getCommentRegisteredBy() {
        return commentRegisteredBy;
    }

    public void setCommentRegisteredBy(String commentRegisteredBy) {
        this.commentRegisteredBy = commentRegisteredBy;
    }

    @Override
    public String getBaseTypeName() {
        return COMMENT;
    }

    @Override
    public String getBaseRel() {
        return REL_FONDS_STRUCTURE_COMMENT;
    }

    public Set<File> getReferenceFile() {
        return referenceFile;
    }

    public void addFile(File file) {
        this.referenceFile.add(file);
    }

    public void removeFile(File file) {
        this.referenceFile.remove(file);
        file.getReferenceComment().remove(this);
    }

    public Set<RecordEntity> getReferenceRecordEntity() {
        return referenceRecordEntity;
    }

    public void addRecordEntity(RecordEntity record) {
        this.referenceRecordEntity.add(record);
    }

    public void removeRecordEntity(RecordEntity record) {
        this.referenceRecordEntity.remove(record);
        record.getReferenceComment().remove(this);
    }

    public Set<DocumentDescription> getReferenceDocumentDescription() {
        return referenceDocumentDescription;
    }

    public void addDocumentDescription(
            DocumentDescription documentDescription) {
        this.referenceDocumentDescription.add(documentDescription);
    }

    public void removeDocumentDescription(
            DocumentDescription documentDescription) {
        this.referenceDocumentDescription.remove(documentDescription);
        documentDescription.getReferenceComment().remove(this);
    }

    @Override
    public String toString() {
        return "Comment{" + super.toString() +
                ", commentText='" + commentText + '\'' +
                ", commentTypeCode='" + commentTypeCode + '\'' +
                ", commentTypeCodeName='" + commentTypeCodeName + '\'' +
                ", commentDate=" + commentDate +
                ", commentRegisteredBy='" + commentRegisteredBy + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (other.getClass() != getClass()) {
            return false;
        }
        Comment rhs = (Comment) other;
        return new EqualsBuilder()
                .appendSuper(super.equals(other))
                .append(commentText, rhs.commentText)
                .append(commentTypeCode, rhs.commentTypeCode)
                .append(commentTypeCodeName, rhs.commentTypeCodeName)
                .append(commentDate, rhs.commentDate)
                .append(commentRegisteredBy, rhs.commentRegisteredBy)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .appendSuper(super.hashCode())
                .append(commentText)
                .append(commentTypeCode)
                .append(commentTypeCodeName)
                .append(commentDate)
                .append(commentRegisteredBy)
                .toHashCode();
    }
}
