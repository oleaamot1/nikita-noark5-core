package app.domain.noark5.secondary;

import app.domain.annotation.LinksObject;
import app.domain.annotation.LinksPacker;
import app.domain.interfaces.entities.secondary.IKeywordEntity;
import app.domain.noark5.Class;
import app.domain.noark5.File;
import app.domain.noark5.RecordEntity;
import app.domain.noark5.SystemIdEntity;
import app.webapp.payload.builder.noark5.secondary.KeywordLinksBuilder;
import app.webapp.payload.deserializers.noark5.secondary.KeywordDeserializer;
import app.webapp.payload.links.secondary.KeywordLinks;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.Table;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.util.HashSet;
import java.util.Set;

import static app.utils.constants.Constants.REL_FONDS_STRUCTURE_KEYWORD;
import static app.utils.constants.Constants.TABLE_KEYWORD;
import static app.utils.constants.N5ResourceMappings.*;

@Entity
@Table(name = TABLE_KEYWORD)
@JsonDeserialize(using = KeywordDeserializer.class)
@LinksPacker(using = KeywordLinksBuilder.class)
@LinksObject(using = KeywordLinks.class)

public class Keyword
        extends SystemIdEntity
        implements IKeywordEntity {
    /**
     * M022 - noekkelord (xs:string)
     */
    @Column(name = KEYWORD_ENG_OBJECT, nullable = false)
    //@Audited
    @JsonProperty(KEYWORD)

    private String keyword;

    // Links to Class
    @ManyToMany(mappedBy = REFERENCE_KEYWORD)
    private final Set<Class> referenceClass = new HashSet<>();
    // Links to File
    @ManyToMany(mappedBy = REFERENCE_KEYWORD)
    private final Set<File> referenceFile = new HashSet<>();
    // Links to Record
    @ManyToMany(mappedBy = REFERENCE_KEYWORD)
    private final Set<RecordEntity> referenceRecordEntity = new HashSet<>();

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    @Override
    public String getBaseTypeName() {
        return KEYWORD;
    }

    @Override
    public String getBaseRel() {
        return REL_FONDS_STRUCTURE_KEYWORD;
    }

    @Override
    public Set<Class> getReferenceClass() {
        return referenceClass;
    }

    @Override
    public void addReferenceClass(Class klass) {
        referenceClass.add(klass);
        klass.getReferenceKeyword().add(this);
    }

    @Override
    public void removeReferenceClass(Class klass) {
        referenceClass.remove(klass);
        klass.getReferenceKeyword().remove(this);
    }

    @Override
    public Set<File> getReferenceFile() {
        return referenceFile;
    }

    @Override
    public void addReferenceFile(File file) {
        referenceFile.add(file);
        file.getReferenceKeyword().add(this);
    }

    @Override
    public void removeReferenceFile(File file) {
        referenceFile.remove(file);
        file.getReferenceKeyword().remove(this);
    }

    @Override
    public Set<RecordEntity> getReferenceRecordEntity() {
        return referenceRecordEntity;
    }

    @Override
    public void addReferenceRecord(RecordEntity record) {
        referenceRecordEntity.add(record);
        record.getReferenceKeyword().add(this);
    }

    @Override
    public void removeReferenceRecord(RecordEntity record) {
        referenceRecordEntity.remove(record);
        record.getReferenceKeyword().remove(this);
    }

    @Override
    public String toString() {
        return "Keyword{" + super.toString() +
                "keyword='" + keyword + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (other.getClass() != getClass()) {
            return false;
        }
        Keyword rhs = (Keyword) other;
        return new EqualsBuilder()
                .appendSuper(super.equals(other))
                .append(keyword, rhs.keyword)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .appendSuper(super.hashCode())
                .append(keyword)
                .toHashCode();
    }
}
