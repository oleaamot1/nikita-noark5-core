package app.domain.noark5.secondary;

import app.domain.annotation.LinksObject;
import app.domain.annotation.LinksPacker;
import app.domain.interfaces.entities.secondary.IPartPersonEntity;
import app.domain.noark5.casehandling.secondary.ContactInformation;
import app.domain.noark5.casehandling.secondary.PostalAddress;
import app.domain.noark5.casehandling.secondary.ResidingAddress;
import app.webapp.payload.builder.noark5.secondary.PartPersonLinksBuilder;
import app.webapp.payload.deserializers.noark5.secondary.PartPersonDeserializer;
import app.webapp.payload.links.secondary.PartPersonLinks;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import jakarta.persistence.*;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import static app.utils.constants.Constants.*;
import static app.utils.constants.N5ResourceMappings.*;
import static jakarta.persistence.CascadeType.ALL;
import static jakarta.persistence.FetchType.LAZY;

@Entity
@Table(name = TABLE_PART_PERSON)
@JsonDeserialize(using = PartPersonDeserializer.class)
@LinksPacker(using = PartPersonLinksBuilder.class)
@LinksObject(using = PartPersonLinks.class)

public class PartPerson
        extends Part
        implements IPartPersonEntity {

    /**
     * M??? - foedselsnummer (xs:string)
     */
    @Column(name = SOCIAL_SECURITY_NUMBER_ENG)
//@Audited
    @JsonProperty(SOCIAL_SECURITY_NUMBER)

    private String socialSecurityNumber;

    /**
     * M??? - DNummer (xs:string)
     */
    @Column(name = D_NUMBER_FIELD_ENG)
//@Audited
    @JsonProperty(D_NUMBER_FIELD)

    private String dNumber;

    @OneToOne(mappedBy = "partPerson", fetch = LAZY, cascade = ALL)
    @JoinColumn(name = PRIMARY_KEY_SYSTEM_ID,
            referencedColumnName = PRIMARY_KEY_SYSTEM_ID)
    private PostalAddress postalAddress;

    @OneToOne(mappedBy = "partPerson", fetch = LAZY, cascade = ALL)
    @JoinColumn(name = PRIMARY_KEY_SYSTEM_ID,
            referencedColumnName = PRIMARY_KEY_SYSTEM_ID)
    private ResidingAddress residingAddress;

    @OneToOne(mappedBy = "partPerson", fetch = LAZY, cascade = ALL)
    @JoinColumn(name = PRIMARY_KEY_SYSTEM_ID,
            referencedColumnName = PRIMARY_KEY_SYSTEM_ID)
    private ContactInformation contactInformation;

    @Override
    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public String getdNumber() {
        return dNumber;
    }

    public void setdNumber(String dNumber) {
        this.dNumber = dNumber;
    }

    public PostalAddress getPostalAddress() {
        return postalAddress;
    }

    public void setPostalAddress(PostalAddress postalAddress) {
        this.postalAddress = postalAddress;
        if (null != postalAddress) {
            this.postalAddress.setPartPerson(this);
        }
    }

    public ResidingAddress getResidingAddress() {
        return residingAddress;
    }

    public void setResidingAddress(ResidingAddress residingAddress) {
        this.residingAddress = residingAddress;
        if (null != residingAddress) {
            residingAddress.setPartPerson(this);
        }
    }

    public ContactInformation getContactInformation() {
        return contactInformation;
    }

    public void setContactInformation(ContactInformation contactInformation) {
        this.contactInformation = contactInformation;
        if (null != contactInformation) {
            contactInformation.setPartPerson(this);
        }
    }

    @Override
    public String getBaseTypeName() {
        return PART_PERSON;
    }

    @Override
    public String getBaseRel() {
        return REL_FONDS_STRUCTURE_PART_PERSON;
    }

    @Override
    public String toString() {
        return "PartPerson{" + super.toString() +
                ", socialSecurityNumber='" + socialSecurityNumber + '\'' +
                ", dNumber='" + dNumber + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) return false;
        if (other == this) return true;
        if (other.getClass() != getClass()) return false;
        PartPerson rhs = (PartPerson) other;
        return new EqualsBuilder()
                .appendSuper(super.equals(other))
                .append(socialSecurityNumber, rhs.socialSecurityNumber)
                .append(dNumber, rhs.dNumber)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .appendSuper(super.hashCode())
                .append(socialSecurityNumber)
                .append(dNumber)
                .toHashCode();
    }
}
