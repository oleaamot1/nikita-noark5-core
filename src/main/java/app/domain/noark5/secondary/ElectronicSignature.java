package app.domain.noark5.secondary;

import app.domain.noark5.DocumentDescription;
import app.domain.noark5.DocumentObject;
import app.domain.noark5.SystemIdEntity;
import app.domain.noark5.casehandling.RegistryEntry;
import app.domain.noark5.metadata.ElectronicSignatureSecurityLevel;
import app.domain.noark5.metadata.ElectronicSignatureVerified;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.OffsetDateTime;

import static app.utils.constants.Constants.TABLE_ELECTRONIC_SIGNATURE;
import static app.utils.constants.N5ResourceMappings.*;
import static jakarta.persistence.FetchType.LAZY;
import static org.springframework.format.annotation.DateTimeFormat.ISO.DATE_TIME;

@Entity
@Table(name = TABLE_ELECTRONIC_SIGNATURE)

public class ElectronicSignature
        extends SystemIdEntity {

    /**
     * M001 - systemID (xs:string)
     */
//    @Id
//    @Column(name = SYSTEM_ID_ENG, updatable = false, nullable = false)
//    @JdbcTypeCode(VARCHAR)
//    private UUID systemId;

    /**
     * M622 - verifisertDato (xs:date)
     */
    @Column(name = ELECTRONIC_SIGNATURE_VERIFIED_DATE_ENG)
    @JsonProperty(ELECTRONIC_SIGNATURE_VERIFIED_DATE)
    @DateTimeFormat(iso = DATE_TIME)
    //@Audited
    private OffsetDateTime verifiedDate;

    /**
     * M623 - verifisertAv (xs:string)
     */
    @Column(name = ELECTRONIC_SIGNATURE_VERIFIED_BY_ENG)
    @JsonProperty(ELECTRONIC_SIGNATURE_VERIFIED_BY)
    //@Audited

    private String verifiedBy;

    /**
     * M??? - elektronisksignatursikkerhetsnivaa code (xs:string)
     */
    @Column(name = ELECTRONIC_SIGNATURE_SECURITY_LEVEL_CODE_ENG)
    @JsonProperty(ELECTRONIC_SIGNATURE_SECURITY_LEVEL_CODE)
    //@Audited
    private String electronicSignatureSecurityLevelCode;

    /**
     * M507 - elektronisksignatursikkerhetsnivaa code name (xs:string)
     */
    @Column(name = ELECTRONIC_SIGNATURE_SECURITY_LEVEL_CODE_NAME_ENG)
    @JsonProperty(ELECTRONIC_SIGNATURE_SECURITY_LEVEL_CODE_NAME)
    //@Audited
    private String electronicSignatureSecurityLevelCodeName;

    /**
     * M??? - elektronisksignaturverifisert code (xs:string)
     */
    @Column(name = ELECTRONIC_SIGNATURE_VERIFIED_CODE_ENG)
    @JsonProperty(ELECTRONIC_SIGNATURE_VERIFIED_CODE)
    //@Audited
    private String electronicSignatureVerifiedCode;

    // Link to RegistryEntry
//    @OneToOne(fetch = LAZY)
    @OneToOne
    //@JoinColumn(name = PRIMARY_KEY_SYSTEM_ID)
    private RegistryEntry referenceRegistryEntry;

    // Link to DocumentObject
    @OneToOne(fetch = LAZY)
    // @JoinColumn(name = PRIMARY_KEY_SYSTEM_ID)
    private DocumentObject referenceDocumentObject;

    // Link to DocumentDescription
    @OneToOne(fetch = LAZY)
    //@JoinColumn(name = PRIMARY_KEY_SYSTEM_ID)
    private DocumentDescription referenceDocumentDescription;
    /**
     * M508 - elektronisksignaturverifisert code name (xs:string)
     */
    @Column(name = ELECTRONIC_SIGNATURE_VERIFIED_CODE_NAME_ENG)
    @JsonProperty(ELECTRONIC_SIGNATURE_VERIFIED_CODE_NAME + "." + CODE_NAME)
    //@Audited
    private String electronicSignatureVerifiedCodeName;

    public ElectronicSignatureSecurityLevel
    getElectronicSignatureSecurityLevel() {
        if (null == electronicSignatureSecurityLevelCode)
            return null;
        return new ElectronicSignatureSecurityLevel
                (electronicSignatureSecurityLevelCode,
                        electronicSignatureSecurityLevelCodeName);
    }

    public void setElectronicSignatureSecurityLevel(
            ElectronicSignatureSecurityLevel electronicSignatureSecurityLevel) {
        if (null != electronicSignatureSecurityLevel) {
            this.electronicSignatureSecurityLevelCode =
                    electronicSignatureSecurityLevel.getCode();
            this.electronicSignatureSecurityLevelCodeName =
                    electronicSignatureSecurityLevel.getCodeName();
        } else {
            this.electronicSignatureSecurityLevelCode = null;
            this.electronicSignatureSecurityLevelCodeName = null;
        }
    }

    public ElectronicSignatureVerified getElectronicSignatureVerified() {
        if (null == electronicSignatureVerifiedCode)
            return null;
        return new ElectronicSignatureVerified
                (electronicSignatureVerifiedCode,
                        electronicSignatureVerifiedCodeName);
    }

    public void setElectronicSignatureVerified(
            ElectronicSignatureVerified electronicSignatureVerified) {
        if (null != electronicSignatureVerified) {
            this.electronicSignatureVerifiedCode =
                    electronicSignatureVerified.getCode();
            this.electronicSignatureVerifiedCodeName =
                    electronicSignatureVerified.getCodeName();
        } else {
            this.electronicSignatureVerifiedCode = null;
            this.electronicSignatureVerifiedCodeName = null;
        }
    }

    public OffsetDateTime getVerifiedDate() {
        return verifiedDate;
    }

    public void setVerifiedDate(OffsetDateTime verifiedDate) {
        this.verifiedDate = verifiedDate;
    }

    public String getVerifiedBy() {
        return verifiedBy;
    }

    public void setVerifiedBy(String verifiedBy) {
        this.verifiedBy = verifiedBy;
    }

    @Override
    public String getBaseTypeName() {
        return ELECTRONIC_SIGNATURE;
    }

    @Override
    public String getBaseRel() {
        return ELECTRONIC_SIGNATURE; // TODO, should it have a relation key?
    }

    public RegistryEntry getReferenceRegistryEntry() {
        return referenceRegistryEntry;
    }

    public void setReferenceRegistryEntry(
            RegistryEntry referenceRegistryEntry) {
        this.referenceRegistryEntry = referenceRegistryEntry;
    }

    public DocumentObject getReferenceDocumentObject() {
        return referenceDocumentObject;
    }

    public void setReferenceDocumentObject(
            DocumentObject referenceDocumentObject) {
        this.referenceDocumentObject = referenceDocumentObject;
    }

    public DocumentDescription getReferenceDocumentDescription() {
        return referenceDocumentDescription;
    }

    public void setReferenceDocumentDescription(
            DocumentDescription referenceDocumentDescription) {
        this.referenceDocumentDescription = referenceDocumentDescription;
    }

    @Override
    public String toString() {
        return "ElectronicSignature{" + super.toString() +
                ", electronicSignatureSecurityLevelCode='" +
                electronicSignatureSecurityLevelCode + '\'' +
                ", electronicSignatureSecurityLevelCodeName='" +
                electronicSignatureSecurityLevelCodeName + '\'' +
                ", electronicSignatureVerifiedCode='" +
                electronicSignatureVerifiedCode + '\'' +
                ", electronicSignatureVerifiedCodeName='" +
                electronicSignatureVerifiedCodeName + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (other.getClass() != getClass()) {
            return false;
        }
        ElectronicSignature rhs = (ElectronicSignature) other;
        return new EqualsBuilder()
                .appendSuper(super.equals(other))
                .append(electronicSignatureSecurityLevelCode,
                        rhs.electronicSignatureSecurityLevelCode)
                .append(electronicSignatureSecurityLevelCodeName,
                        rhs.electronicSignatureSecurityLevelCodeName)
                .append(electronicSignatureVerifiedCode,
                        rhs.electronicSignatureVerifiedCode)
                .append(electronicSignatureVerifiedCodeName,
                        rhs.electronicSignatureVerifiedCodeName)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .appendSuper(super.hashCode())
                .append(electronicSignatureSecurityLevelCode)
                .append(electronicSignatureSecurityLevelCodeName)
                .append(electronicSignatureVerifiedCode)
                .append(electronicSignatureVerifiedCodeName)
                .toHashCode();
    }

    public ElectronicSignature() {
    }


}
