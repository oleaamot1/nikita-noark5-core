package app.domain.noark5.secondary;

import app.domain.annotation.LinksObject;
import app.domain.annotation.LinksPacker;
import app.domain.interfaces.entities.secondary.IAuthorEntity;
import app.domain.noark5.DocumentDescription;
import app.domain.noark5.RecordEntity;
import app.domain.noark5.SystemIdEntity;
import app.webapp.payload.builder.noark5.secondary.AuthorLinksBuilder;
import app.webapp.payload.deserializers.noark5.secondary.AuthorDeserializer;
import app.webapp.payload.links.secondary.AuthorLinks;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import jakarta.persistence.*;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import static app.utils.constants.Constants.*;
import static app.utils.constants.N5ResourceMappings.AUTHOR;
import static jakarta.persistence.FetchType.LAZY;

@Entity
@Table(name = TABLE_CONTACT_AUTHOR)
@JsonDeserialize(using = AuthorDeserializer.class)
@LinksPacker(using = AuthorLinksBuilder.class)
@LinksObject(using = AuthorLinks.class)

public class Author
        extends SystemIdEntity
        implements IAuthorEntity {

    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = FOREIGN_KEY_RECORD_PK)
    private RecordEntity referenceRecordEntity;

    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = FOREIGN_KEY_DOCUMENT_DESCRIPTION_PK)
    private DocumentDescription referenceDocumentDescription;

    /**
     * M024 - forfatter (xs:string)
     */
    @Column(name = "author")
    //@Audited

    private String author;

    /**
     * Used to identify if the current author is associated  with a document
     * description. This can be used to save a potential lookup in the database.
     */
    @Column(name = "is_for_document_description")
    //@Audited
    private Boolean isForDocumentDescription = false;

    /**
     * Used to identify if the current author is associated  with a record
     * This can be used to save a potential lookup in the database.
     */
    @Column(name = "is_for_record")
    //@Audited
    private Boolean isForRecord = false;

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public RecordEntity getReferenceRecordEntity() {
        return referenceRecordEntity;
    }

    public void setReferenceRecord(RecordEntity referenceRecordEntity) {
        this.referenceRecordEntity = referenceRecordEntity;
        isForRecord = true;
    }

    public DocumentDescription getReferenceDocumentDescription() {
        return referenceDocumentDescription;
    }

    public void setReferenceDocumentDescription(
            DocumentDescription referenceDocumentDescription) {
        this.referenceDocumentDescription = referenceDocumentDescription;
        isForDocumentDescription = true;
    }

    public Boolean getForDocumentDescription() {
        return isForDocumentDescription;
    }

    public Boolean getForRecord() {
        return isForRecord;
    }

    @Override
    public String getBaseTypeName() {
        return AUTHOR;
    }

    @Override
    public String getBaseRel() {
        return REL_FONDS_STRUCTURE_AUTHOR;
    }

    @Override
    public String toString() {
        return "Author{" + super.toString() +
                "author='" + author + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (other.getClass() != getClass()) {
            return false;
        }
        Author rhs = (Author) other;
        return new EqualsBuilder()
                .appendSuper(super.equals(other))
                .append(author, rhs.author)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .appendSuper(super.hashCode())
                .append(author)
                .toHashCode();
    }
}
