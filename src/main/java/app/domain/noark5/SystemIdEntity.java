package app.domain.noark5;

import app.domain.interfaces.entities.INoarkEntity;
import app.domain.interfaces.entities.ISystemId;
import app.webapp.exceptions.NikitaMalformedInputDataException;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import org.apache.commons.lang3.builder.CompareToBuilder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.annotations.UuidGenerator;
import org.hibernate.envers.Audited;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.io.Serial;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static app.utils.constants.Constants.DISCRIMINATOR_COLUMN_NAME;
import static app.utils.constants.Constants.NOARK_FONDS_STRUCTURE_PATH;
import static app.utils.constants.N5ResourceMappings.*;
import static jakarta.persistence.DiscriminatorType.STRING;
import static jakarta.persistence.InheritanceType.JOINED;
import static java.sql.Types.VARCHAR;
import static org.hibernate.annotations.UuidGenerator.Style.TIME;
import static org.hibernate.envers.RelationTargetAuditMode.NOT_AUDITED;


@Entity
@Inheritance(strategy = JOINED)
@DiscriminatorColumn(
        name = DISCRIMINATOR_COLUMN_NAME,
        discriminatorType = STRING
)
@EntityListeners(AuditingEntityListener.class)
@Audited(targetAuditMode = NOT_AUDITED)
public class SystemIdEntity
        extends NoarkEntity
        implements ISystemId, Comparable<SystemIdEntity> {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * M001 - systemID (xs:string)
     */
    @Id
    @GeneratedValue(generator = "uuid-gen")
    @Column(name = SYSTEM_ID_ENG, updatable = false, nullable = false)
    @JdbcTypeCode(VARCHAR)
    @UuidGenerator(style = TIME)
    private UUID systemId;

    @Column(name = OBJECT_TYPE_ENG)
    @JsonIgnore
    private String objectType = this.getClass().getName();

    @Column(name = DISCRIMINATOR_COLUMN_NAME, insertable = false, updatable = false)
    @JsonIgnore
    private String discriminator = this.getClass().getName();

    // Links to ChangeLog
    @OneToMany(mappedBy = "referenceSystemIdEntity")
    @JsonIgnore
    private List<ChangeLog> referenceChangeLog = new ArrayList<>();

    @Override
    public UUID getSystemId() {
        return systemId;
    }

    @Override
    public void setSystemId(UUID systemId) {
        this.systemId = systemId;
    }

    @Override
    public String getSystemIdAsString() {
        if (null != systemId)
            return systemId.toString();
        else
            return null;
    }

    @Override
    public String getIdentifier() {
        return getSystemIdAsString();
    }

    @Override
    public String getIdentifierType() {
        return SYSTEM_ID;
    }

    public List<ChangeLog> getChangeLog() {
        return referenceChangeLog;
    }

    public void setChangeLog(List<ChangeLog> referenceChangeLog) {
        this.referenceChangeLog = referenceChangeLog;
    }

    public void addChangeLog(ChangeLog eventLog) {
        this.referenceChangeLog.add(eventLog);
        eventLog.setReferenceArchiveUnit(this);
    }

    public void removeChangeLog(ChangeLog eventLog) {
        referenceChangeLog.remove(eventLog);
        eventLog.setReferenceArchiveUnit(null);
    }

    // Most entities belong to arkivstruktur. These entities pick the value
    // up here
    @Override
    public String getFunctionalTypeName() {
        return NOARK_FONDS_STRUCTURE_PATH;
    }

    @Override
    public void createReference(
            @NotNull INoarkEntity entity,
            @NotNull String referenceType) {
        // I really should be overridden. Currently throwing an Exception if I
        // am not overriden as nikita is unable to process this
        throw new NikitaMalformedInputDataException("Error when trying to " +
                "create a reference between entities");
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .appendSuper(super.hashCode())
                .append(systemId)
                .toHashCode();
    }

    @Override
    public int compareTo(SystemIdEntity otherEntity) {
        if (null == otherEntity) {
            return -1;
        }
        return new CompareToBuilder()
                .append(this.systemId, otherEntity.systemId)
                .toComparison();
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (other.getClass() != getClass()) {
            return false;
        }
        SystemIdEntity rhs = (SystemIdEntity) other;
        return new EqualsBuilder()
                .appendSuper(super.equals(other))
                .append(systemId, rhs.getSystemId())
                .isEquals();
    }

    @Override
    public String toString() {
        return "NoarkEntity{" +
                "systemId=" + systemId +
                '}';
    }
}
