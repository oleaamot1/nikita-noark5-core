package app.domain.noark5.md_other;

import app.domain.annotation.LinksObject;
import app.domain.annotation.LinksPacker;
import app.domain.annotation.Updatable;
import app.domain.noark5.SystemIdEntity;
import app.webapp.payload.builder.noark5.metadata.BSMMetadataLinksBuilder;
import app.webapp.payload.deserializers.noark5.BSMMetadataDeserializer;
import app.webapp.payload.links.md_other.BSMMetadataLinks;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import jakarta.persistence.Column;
import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;

import java.util.Objects;

import static app.utils.constants.Constants.REL_METADATA_BSM;
import static app.utils.constants.Constants.TABLE_BSM_METDATA;
import static app.utils.constants.N5ResourceMappings.*;

// Noark 5v5 virksomhetsspesifikkeMetadata
// Registered additional metadata
@Entity
@Table(name = TABLE_BSM_METDATA)
@JsonDeserialize(using = BSMMetadataDeserializer.class)
@LinksPacker(using = BSMMetadataLinksBuilder.class)
@LinksObject(using = BSMMetadataLinks.class)
@DiscriminatorValue("BSMMetadata")
public class BSMMetadata
        extends SystemIdEntity {

    private static final long serialVersionUID = 1L;

    @Column(name = NAME_ENG, unique = true, updatable = false,
            nullable = false, length = 80)
    @JsonProperty(NAME)
    private String name;

    @Column(name = TYPE_ENG, nullable = false)
    @JsonProperty(TYPE)
    private String type;

    @Column(name = OUTDATED_ENG, nullable = false)
    @JsonProperty(OUTDATED)
    @Updatable
    private Boolean outdated = false;

    @Column(name = DESCRIPTION_ENG)
    @JsonProperty(DESCRIPTION)
    @Updatable
    private String description;

    @Column(name = SOURCE_ENG)
    @JsonProperty(SOURCE)
    @Updatable
    private String source;

    public BSMMetadata() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Boolean getOutdated() {
        return outdated;
    }

    public void setOutdated(Boolean outdated) {
        this.outdated = outdated;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    @Override
    public String getBaseTypeName() {
        return BSM_DEF;
    }

    @Override
    public String getBaseRel() {
        return REL_METADATA_BSM;
    }

    @Override
    public String toString() {
        return "BSMMetadata{" +
                "name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", outdated=" + outdated +
                ", description='" + description + '\'' +
                ", source='" + source + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        BSMMetadata that = (BSMMetadata) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(type, that.type) &&
                Objects.equals(outdated, that.outdated) &&
                Objects.equals(description, that.description) &&
                Objects.equals(source, that.source);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), name, type, outdated,
                description, source);
    }
}
