package app.domain.noark5.admin;

import app.domain.annotation.LinksObject;
import app.domain.annotation.LinksPacker;
import app.domain.interfaces.entities.admin.IUserEntity;
import app.domain.noark5.SystemIdEntity;
import app.webapp.payload.builder.noark5.admin.UserLinksBuilder;
import app.webapp.payload.deserializers.noark5.admin.UserDeserializer;
import app.webapp.payload.links.admin.UserLinks;
import app.webapp.spring.security.INikitaUserDetails;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.time.OffsetDateTime;
import java.util.*;

import static app.utils.constants.Constants.*;
import static app.utils.constants.N5ResourceMappings.*;
import static jakarta.persistence.CascadeType.PERSIST;
import static jakarta.persistence.FetchType.LAZY;
import static org.springframework.format.annotation.DateTimeFormat.ISO.DATE_TIME;

@Entity
@Table(name = TABLE_USER)
@JsonDeserialize(using = UserDeserializer.class)
@LinksPacker(using = UserLinksBuilder.class)
@LinksObject(using = UserLinks.class)
@DiscriminatorValue("User")
public class User
        extends SystemIdEntity
        implements INikitaUserDetails, IUserEntity {

    @Column(unique = true, length = 100)
    @NotNull
    private String username;

    @Column(name = "password", length = 100)
    @Size(min = 1, max = 100)
    private String password;

    @Column(name = "firstname", length = 50)
    @Size(min = 1, max = 50)
    private String firstname;

    @Column(name = "lastname", length = 50)
    @Size(min = 1, max = 50)
    private String lastname;

    /**
     * M602 - avsluttetDato (xs:dateTime)
     */
    @Column(name = FINALISED_DATE_ENG)
    @DateTimeFormat(iso = DATE_TIME)
    //@Audited
    @JsonProperty(FINALISED_DATE)
    private OffsetDateTime finalisedDate;

    /**
     * M603 - avsluttetAv (xs:string)
     */
    @Column(name = FINALISED_BY_ENG)
    //@Audited
    @JsonProperty(FINALISED_BY)
    private String finalisedBy;

    @NotNull
    @Column(name = "account_non_expired", nullable = false)
    private boolean accountNonExpired = true;

    @NotNull
    @Column(name = "credentials_non_expired", nullable = false)
    private boolean credentialsNonExpired = true;

    @NotNull
    @Column(name = "account_non_locked", nullable = false)
    private boolean accountNonLocked = true;

    @Column(name = "enabled")
    @NotNull
    private Boolean enabled = true;

    @Column(name = "last_password_reset_date")
    @DateTimeFormat(iso = DATE_TIME)
    private OffsetDateTime lastPasswordResetDate;

    @ManyToMany
    @JoinTable(name = TABLE_USER_AUTHORITY,
            joinColumns = {@JoinColumn(name = FOREIGN_KEY_USER_PK,
                    referencedColumnName = PRIMARY_KEY_SYSTEM_ID)},
            inverseJoinColumns = {@JoinColumn(name = "authority_id",
                    referencedColumnName = "id")})
    private Set<Authority> authorities = new HashSet<>();

    @ManyToMany(mappedBy = "users")
    private Set<AdministrativeUnit> administrativeUnits = new HashSet<>();

    @ManyToOne(fetch = LAZY, cascade = PERSIST)
    @JoinColumn(name = USER_ORGANISATION_ID,
            referencedColumnName = PRIMARY_KEY_SYSTEM_ID)
    private Organisation organisation;

    @Override
    public OffsetDateTime getFinalisedDate() {
        return finalisedDate;
    }

    @Override
    public void setFinalisedDate(OffsetDateTime finalisedDate) {
        this.finalisedDate = finalisedDate;
    }

    @Override
    public String getFinalisedBy() {
        return finalisedBy;
    }

    @Override
    public void setFinalisedBy(String finalisedBy) {
        this.finalisedBy = finalisedBy;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public boolean isAccountNonExpired() {
        return accountNonExpired;
    }

    public void setAccountNonExpired(boolean accountNonExpired) {
        this.accountNonExpired = accountNonExpired;
    }

    public boolean isCredentialsNonExpired() {
        return credentialsNonExpired;
    }

    public void setCredentialsNonExpired(boolean credentialsNonExpired) {
        this.credentialsNonExpired = credentialsNonExpired;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isAccountNonLocked() {
        return accountNonLocked;
    }

    public void setAccountNonLocked(boolean accountNonLocked) {
        this.accountNonLocked = accountNonLocked;
    }

    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> authoritiesLocal =
                new ArrayList<>();
        for (Authority authority : authorities) {
            authoritiesLocal.add(new SimpleGrantedAuthority(
                    authority.getAuthorityName().name()));
        }
        return authoritiesLocal;
    }

    public void addAuthority(Authority authority) {
        authorities.add(authority);
        authority.getUsers().add(this);
    }

    public void removeAuthority(Authority authority) {
        authorities.remove(authority);
        authority.getUsers().remove(this);
    }

    public Set<AdministrativeUnit> getAdministrativeUnits() {
        return administrativeUnits;
    }

    public void addAdministrativeUnit(AdministrativeUnit administrativeUnit) {
        this.administrativeUnits.add(administrativeUnit);
    }

    public void removeAdministrativeUnit(
            AdministrativeUnit administrativeUnit) {
        this.administrativeUnits.remove(administrativeUnit);
        administrativeUnit.getUsers().remove(this);
    }

    public OffsetDateTime getLastPasswordResetDate() {
        return lastPasswordResetDate;
    }

    public void setLastPasswordResetDate(OffsetDateTime lastPasswordResetDate) {
        this.lastPasswordResetDate = lastPasswordResetDate;
    }

    public Organisation getOrganisation() {
        return organisation;
    }

    @Override
    public User getUser() {
        return this;
    }

    public void setOrganisation(Organisation organisation) {
        this.organisation = organisation;
    }

    @Override
    public String getFunctionalTypeName() {
        return NOARK_ADMINISTRATION_PATH;
    }

    @Override
    public String getBaseTypeName() {
        return USER;
    }

    @Override
    public String getBaseRel() {
        return REL_ADMIN_USER;
    }

    @Override
    public String toString() {
        return "User{" + super.toString() +
                "username='" + username + '\'' +
                ", finalisedDate=" + finalisedDate +
                ", finalisedBy='" + finalisedBy + '\'' +
                ", accountNonExpired=" + accountNonExpired +
                ", credentialsNonExpired=" + credentialsNonExpired +
                ", accountNonLocked=" + accountNonLocked +
                ", password='" + password + '\'' +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", enabled=" + enabled +
                ", lastPasswordResetDate=" + lastPasswordResetDate +
                '}';
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (other.getClass() != getClass()) {
            return false;
        }
        User rhs = (User) other;
        return new EqualsBuilder()
                .appendSuper(super.equals(other))
                .append(finalisedBy, rhs.finalisedBy)
                .append(finalisedDate, rhs.finalisedDate)
                .append(accountNonExpired, rhs.accountNonExpired)
                .append(accountNonLocked, rhs.accountNonLocked)
                .append(username, rhs.username)
                .append(password, rhs.password)
                .append(firstname, rhs.firstname)
                .append(lastname, rhs.lastname)
                .append(enabled, rhs.enabled)
                .append(lastPasswordResetDate, rhs.lastPasswordResetDate)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .appendSuper(super.hashCode())
                .append(finalisedBy)
                .append(finalisedDate)
                .append(accountNonExpired)
                .append(accountNonLocked)
                .append(username)
                .append(password)
                .append(firstname)
                .append(lastname)
                .append(enabled)
                .append(lastPasswordResetDate)
                .toHashCode();
    }
}
