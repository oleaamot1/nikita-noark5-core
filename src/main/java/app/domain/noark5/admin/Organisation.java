package app.domain.noark5.admin;

import app.domain.annotation.LinksObject;
import app.domain.annotation.LinksPacker;
import app.domain.noark5.NoarkGeneralEntity;
import app.webapp.payload.builder.noark5.admin.OrganisationLinksBuilder;
import app.webapp.payload.deserializers.noark5.admin.OrganisationDeserializer;
import app.webapp.payload.links.admin.OrganisationLinks;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;

import static app.utils.constants.Constants.TABLE_ORGANISATION;
import static app.utils.constants.N5ResourceMappings.*;

@Entity
@Table(name = TABLE_ORGANISATION)
@JsonDeserialize(using = OrganisationDeserializer.class)
@LinksPacker(using = OrganisationLinksBuilder.class)
@LinksObject(using = OrganisationLinks.class)
//@DiscriminatorValue("Organisation")
public class Organisation
        extends NoarkGeneralEntity {

    @Column(name = ORGANISATION_NAME_ENG)
    @JsonProperty(ORGANISATION_NAME)
    private String organisationName;

    @Column(name = ORGANISATION_STATUS_ENG)
    @JsonProperty(ORGANISATION_STATUS)
    private String organisationStatus;

    public String getOrganisationName() {
        return organisationName;
    }

    public void setOrganisationName(String organisationName) {
        this.organisationName = organisationName;
    }

    public String getOrganisationStatus() {
        return organisationStatus;
    }

    public void setOrganisationStatus(String organisationStatus) {
        this.organisationStatus = organisationStatus;
    }
}
