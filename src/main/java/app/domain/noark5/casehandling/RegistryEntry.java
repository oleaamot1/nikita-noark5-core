package app.domain.noark5.casehandling;

import app.domain.annotation.LinksObject;
import app.domain.annotation.LinksPacker;
import app.domain.interfaces.entities.IRegistryEntryEntity;
import app.domain.noark5.RecordEntity;
import app.domain.noark5.metadata.RegistryEntryStatus;
import app.domain.noark5.metadata.RegistryEntryType;
import app.domain.noark5.secondary.DocumentFlow;
import app.domain.noark5.secondary.ElectronicSignature;
import app.domain.noark5.secondary.Precedence;
import app.domain.noark5.secondary.SignOff;
import app.utils.constants.Constants;
import app.webapp.payload.builder.noark5.casehandling.RegistryEntryLinksBuilder;
import app.webapp.payload.deserializers.noark5.casehandling.RegistryEntryDeserializer;
import app.webapp.payload.links.casehandling.RegistryEntryLinks;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static app.utils.constants.Constants.*;
import static app.utils.constants.N5ResourceMappings.*;
import static jakarta.persistence.CascadeType.*;
import static jakarta.persistence.FetchType.LAZY;
import static jakarta.persistence.InheritanceType.JOINED;
import static org.springframework.format.annotation.DateTimeFormat.ISO.DATE_TIME;

@Entity
@Table(name = TABLE_REGISTRY_ENTRY)
@Inheritance(strategy = JOINED)
@JsonDeserialize(using = RegistryEntryDeserializer.class)
@LinksPacker(using = RegistryEntryLinksBuilder.class)
@LinksObject(using = RegistryEntryLinks.class)

@DiscriminatorValue("RegistryEntry")
public class RegistryEntry
        extends RecordEntity
        implements IRegistryEntryEntity {

    // Links to DocumentFlow
    @OneToMany(mappedBy = "referenceRegistryEntry")
    private final List<DocumentFlow> referenceDocumentFlow = new ArrayList<>();
    // Links to SignOff
    @ManyToMany(cascade = {PERSIST, MERGE})
    @JoinTable(name = TABLE_REGISTRY_ENTRY_SIGN_OFF,
            joinColumns = @JoinColumn(
                    name = FOREIGN_KEY_RECORD_PK,
                    referencedColumnName = PRIMARY_KEY_SYSTEM_ID),
            inverseJoinColumns = @JoinColumn(
                    name = FOREIGN_KEY_SIGN_OFF_PK,
                    referencedColumnName = PRIMARY_KEY_SYSTEM_ID))
    private final Set<SignOff> referenceSignOff = new HashSet<>();
    // Links to Precedence
    @ManyToMany(cascade = {PERSIST, MERGE})
    @JoinTable(name = TABLE_REGISTRY_ENTRY_PRECEDENCE,
            joinColumns = @JoinColumn(
                    name = FOREIGN_KEY_RECORD_PK,
                    referencedColumnName = PRIMARY_KEY_SYSTEM_ID),
            inverseJoinColumns = @JoinColumn(
                    name = FOREIGN_KEY_PRECEDENCE_PK,
                    referencedColumnName = PRIMARY_KEY_SYSTEM_ID))
    private final Set<Precedence> referencePrecedence = new HashSet<>();
    /**
     * M013 - journalaar (xs:integer)
     */
    @Column(name = REGISTRY_ENTRY_YEAR_ENG, nullable = false)
    //@Audited
    @JsonProperty(REGISTRY_ENTRY_YEAR)
    private Integer recordYear;
    /**
     * M014 - journalsekvensnummer (xs:integer)
     */
    @Column(name = REGISTRY_ENTRY_SEQUENCE_NUMBER_ENG, nullable = false)
    //@Audited
    @JsonProperty(REGISTRY_ENTRY_SEQUENCE_NUMBER)
    private Integer recordSequenceNumber;
    /**
     * M?? - journalposttype code (xs:string)
     */
    @NotNull
    @Column(name = REGISTRY_ENTRY_TYPE_CODE_ENG, nullable = false)
    //@Audited
    @JsonProperty(REGISTRY_ENTRY_TYPE_CODE)
    private String registryEntryTypeCode;
    /**
     * M082 - journalposttype code name (xs:string)
     */
    @NotNull
    @Column(name = REGISTRY_ENTRY_TYPE_CODE_NAME_ENG, nullable = false)
    //@Audited
    @JsonProperty(REGISTRY_ENTRY_TYPE_CODE_NAME)
    private String registryEntryTypeCodeName;
    /**
     * M??? - journalstatus code (xs:string)
     */
    @NotNull
    @Column(name = REGISTRY_ENTRY_STATUS_CODE_ENG)
    //@Audited
    @JsonProperty(REGISTRY_ENTRY_STATUS_CODE)
    private String registryEntryStatusCode;
    /**
     * M053 - journalstatus code name (xs:string, nullable = false)
     */
    @NotNull
    @Column(name = REGISTRY_ENTRY_STATUS_CODE_NAME_ENG, nullable = false)
    //@Audited
    @JsonProperty(REGISTRY_ENTRY_STATUS_CODE_NAME)
    private String registryEntryStatusCodeName;
    /**
     * M015 - journalpostnummer (xs:integer)
     */
    @Column(name = REGISTRY_ENTRY_NUMBER_ENG, nullable = false)
    //@Audited
    @JsonProperty(REGISTRY_ENTRY_NUMBER)
    private Integer registryEntryNumber;
    /**
     * M101 - journaldato (xs:date)
     */
    @NotNull
    @Column(name = REGISTRY_ENTRY_DATE_ENG, nullable = false)
    @DateTimeFormat(iso = DATE_TIME)
    //@Audited
    @JsonProperty(REGISTRY_ENTRY_DATE)
    private OffsetDateTime recordDate;
    /**
     * M103 - dokumentetsDato (xs:date)
     */
    @Column(name = REGISTRY_ENTRY_DOCUMENT_DATE_ENG)
    @DateTimeFormat(iso = DATE_TIME)
    //@Audited
    @JsonProperty(REGISTRY_ENTRY_DOCUMENT_DATE)
    private OffsetDateTime documentDate;
    /**
     * M104 - mottattDato (xs:dateTime)
     */
    @Column(name = REGISTRY_ENTRY_RECEIVED_DATE_ENG)
    @DateTimeFormat(iso = DATE_TIME)
    //@Audited
    @JsonProperty(REGISTRY_ENTRY_RECEIVED_DATE)
    private OffsetDateTime receivedDate;
    /**
     * M105 - sendtDato (xs:dateTime)
     */
    @Column(name = REGISTRY_ENTRY_SENT_DATE_ENG)
    @DateTimeFormat(iso = DATE_TIME)
    //@Audited
    @JsonProperty(REGISTRY_ENTRY_SENT_DATE)
    private OffsetDateTime sentDate;
    /**
     * M109 - forfallsdato (xs:date)
     */
    @Column(name = REGISTRY_ENTRY_DUE_DATE_ENG)
    @DateTimeFormat(iso = DATE_TIME)
    //@Audited
    @JsonProperty(REGISTRY_ENTRY_DUE_DATE)
    private OffsetDateTime dueDate;
    /**
     * M110 - offentlighetsvurdertDato (xs:date)
     */
    @Column(name = REGISTRY_ENTRY_RECORD_FREEDOM_ASSESSMENT_DATE_ENG)
    @DateTimeFormat(iso = DATE_TIME)
    //@Audited
    @JsonProperty(REGISTRY_ENTRY_RECORD_FREEDOM_ASSESSMENT_DATE)
    private OffsetDateTime freedomAssessmentDate;
    /**
     * M304 - antallVedlegg (xs:integer)
     */
    @Column(name = REGISTRY_ENTRY_NUMBER_OF_ATTACHMENTS_ENG)
    //@Audited
    @JsonProperty(REGISTRY_ENTRY_NUMBER_OF_ATTACHMENTS)
    private Integer numberOfAttachments;
    /**
     * M106 - utlaantDato (xs:date)
     */
    @Column(name = CASE_LOANED_DATE_ENG)
    @DateTimeFormat(iso = DATE_TIME)
    //@Audited
    @JsonProperty(CASE_LOANED_DATE)
    private OffsetDateTime loanedDate;
    /**
     * M309 - utlaantTil (xs:string)
     */
    @Column(name = CASE_LOANED_TO_ENG)
    //@Audited
    @JsonProperty(CASE_LOANED_TO)

    private String loaneLinks;
    /**
     * M308 - journalenhet (xs:string)
     */
    @Column(name = CASE_RECORDS_MANAGEMENT_UNIT_ENG)
    //@Audited
    @JsonProperty(CASE_RECORDS_MANAGEMENT_UNIT)

    private String recordsManagementUnit;
    // Link to ElectronicSignature
    @OneToOne(mappedBy = REFERENCE_REGISTRY_ENTRY_DB, fetch = LAZY, cascade = ALL)
    private ElectronicSignature referenceElectronicSignature;

    public RegistryEntry() {
        super();
    }

    public RegistryEntry(RecordEntity recordEntity) {
        super();
        setVersion(recordEntity.getVersion(), true);
        this.setRecordId(recordEntity.getRecordId());
        this.setTitle(recordEntity.getTitle());
        this.setPublicTitle(recordEntity.getPublicTitle());
        this.setDescription(recordEntity.getDescription());
        this.setDocumentMedium(recordEntity.getDocumentMedium());
    }

    public Integer getRecordYear() {
        return recordYear;
    }

    public void setRecordYear(Integer recordYear) {
        this.recordYear = recordYear;
    }

    public Integer getRecordSequenceNumber() {
        return recordSequenceNumber;
    }

    public void setRecordSequenceNumber(Integer recordSequenceNumber) {
        this.recordSequenceNumber = recordSequenceNumber;
    }

    public Integer getRegistryEntryNumber() {
        return registryEntryNumber;
    }

    public void setRegistryEntryNumber(Integer registryEntryNumber) {
        this.registryEntryNumber = registryEntryNumber;
    }

    public RegistryEntryType getRegistryEntryType() {
        if (null == registryEntryTypeCode)
            return null;
        return new RegistryEntryType(registryEntryTypeCode,
                registryEntryTypeCodeName);
    }

    public void setRegistryEntryType(RegistryEntryType registryEntryType) {
        if (null != registryEntryType) {
            this.registryEntryTypeCode = registryEntryType.getCode();
            this.registryEntryTypeCodeName = registryEntryType.getCodeName();
        } else {
            this.registryEntryTypeCode = null;
            this.registryEntryTypeCodeName = null;
        }
    }

    public RegistryEntryStatus getRegistryEntryStatus() {
        if (null == registryEntryStatusCode)
            return null;
        return new RegistryEntryStatus(registryEntryStatusCode,
                registryEntryStatusCodeName);
    }

    public void setRegistryEntryStatus(RegistryEntryStatus registryEntryStatus) {
        if (null != registryEntryStatus) {
            this.registryEntryStatusCode = registryEntryStatus.getCode();
            this.registryEntryStatusCodeName = registryEntryStatus.getCodeName();
        } else {
            this.registryEntryStatusCode = null;
            this.registryEntryStatusCodeName = null;
        }
    }

    public OffsetDateTime getRecordDate() {
        return recordDate;
    }

    public void setRecordDate(OffsetDateTime recordDate) {
        this.recordDate = recordDate;
    }

    public OffsetDateTime getDocumentDate() {
        return documentDate;
    }

    public void setDocumentDate(OffsetDateTime documentDate) {
        this.documentDate = documentDate;
    }

    public OffsetDateTime getReceivedDate() {
        return receivedDate;
    }

    public void setReceivedDate(OffsetDateTime receivedDate) {
        this.receivedDate = receivedDate;
    }

    public OffsetDateTime getSentDate() {
        return sentDate;
    }

    public void setSentDate(OffsetDateTime sentDate) {
        this.sentDate = sentDate;
    }

    public OffsetDateTime getDueDate() {
        return dueDate;
    }

    public void setDueDate(OffsetDateTime dueDate) {
        this.dueDate = dueDate;
    }

    public OffsetDateTime getFreedomAssessmentDate() {
        return freedomAssessmentDate;
    }

    public void setFreedomAssessmentDate(OffsetDateTime freedomAssessmentDate) {
        this.freedomAssessmentDate = freedomAssessmentDate;
    }

    public Integer getNumberOfAttachments() {
        return numberOfAttachments;
    }

    public void setNumberOfAttachments(Integer numberOfAttachments) {
        this.numberOfAttachments = numberOfAttachments;
    }

    public OffsetDateTime getLoanedDate() {
        return loanedDate;
    }

    public void setLoanedDate(OffsetDateTime loanedDate) {
        this.loanedDate = loanedDate;
    }

    public String getLoaneLinks() {
        return loaneLinks;
    }

    public void setLoaneLinks(String loaneLinks) {
        this.loaneLinks = loaneLinks;
    }

    public String getRecordsManagementUnit() {
        return recordsManagementUnit;
    }

    public void setRecordsManagementUnit(String recordsManagementUnit) {
        this.recordsManagementUnit = recordsManagementUnit;
    }

    @Override
    public String getBaseTypeName() {
        return REGISTRY_ENTRY;
    }

    @Override
    public String getBaseRel() {
        return REL_CASE_HANDLING_REGISTRY_ENTRY;
    }

    @Override
    public String getFunctionalTypeName() {
        return Constants.NOARK_CASE_HANDLING_PATH;
    }

    @Override
    public List<DocumentFlow> getReferenceDocumentFlow() {
        return referenceDocumentFlow;
    }

    @Override
    public void addDocumentFlow(DocumentFlow documentFlow) {
        this.referenceDocumentFlow.add(documentFlow);
        documentFlow.setReferenceRegistryEntry(this);
    }

    @Override
    public void removeDocumentFlow(DocumentFlow documentFlow) {
        referenceDocumentFlow.remove(documentFlow);
        documentFlow.setReferenceRegistryEntry(null);
    }

    @Override
    public Set<SignOff> getReferenceSignOff() {
        return referenceSignOff;
    }

    @Override
    public void addSignOff(SignOff signOff) {
        this.referenceSignOff.add(signOff);
        signOff.getReferenceRegistryEntry().add(this);
    }

    public void removeSignOff(SignOff signOff) {
        this.referenceSignOff.remove(signOff);
        signOff.getReferenceRegistryEntry().remove(this);
    }

    @Override
    public Set<Precedence> getReferencePrecedence() {
        return referencePrecedence;
    }

    @Override
    public void addPrecedence(Precedence precedence) {
        this.referencePrecedence.add(precedence);
        precedence.getReferenceRegistryEntry().add(this);
    }

    @Override
    public void removePrecedence(Precedence precedence) {
        this.referencePrecedence.remove(precedence);
        precedence.getReferenceRegistryEntry().remove(this);
    }

    public ElectronicSignature getReferenceElectronicSignature() {
        return referenceElectronicSignature;
    }

    public void setReferenceElectronicSignature(
            ElectronicSignature referenceElectronicSignature) {
        this.referenceElectronicSignature = referenceElectronicSignature;
    }

    @Override
    public String toString() {
        return super.toString() + " RegistryEntry{" + super.toString() +
                "recordsManagementUnit='" + recordsManagementUnit + '\'' +
                ", loaneLinks=" + loaneLinks +
                ", loanedDate=" + loanedDate +
                ", numberOfAttachments=" + numberOfAttachments +
                ", freedomAssessmentDate=" + freedomAssessmentDate +
                ", dueDate=" + dueDate +
                ", sentDate=" + sentDate +
                ", receivedDate=" + receivedDate +
                ", documentDate=" + documentDate +
                ", recordDate=" + recordDate +
                ", registryEntryStatusCode='" + registryEntryStatusCode + '\'' +
                ", registryEntryStatusCodeName='" + registryEntryStatusCodeName + '\'' +
                ", registryEntryTypeCode='" + registryEntryTypeCode + '\'' +
                ", registryEntryTypeCodeName='" + registryEntryTypeCodeName + '\'' +
                ", registryEntryNumber=" + registryEntryNumber +
                ", recordSequenceNumber=" + recordSequenceNumber +
                ", recordYear=" + recordYear +
                '}';
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (other.getClass() != getClass()) {
            return false;
        }
        RegistryEntry rhs = (RegistryEntry) other;
        return new EqualsBuilder()
                .appendSuper(super.equals(other))
                .append(recordsManagementUnit, rhs.recordsManagementUnit)
                .append(loaneLinks, rhs.loaneLinks)
                .append(loanedDate, rhs.loanedDate)
                .append(numberOfAttachments, rhs.numberOfAttachments)
                .append(freedomAssessmentDate, rhs.freedomAssessmentDate)
                .append(dueDate, rhs.dueDate)
                .append(sentDate, rhs.sentDate)
                .append(receivedDate, rhs.receivedDate)
                .append(documentDate, rhs.documentDate)
                .append(recordDate, rhs.recordDate)
                .append(registryEntryStatusCode, rhs.registryEntryStatusCode)
                .append(registryEntryStatusCodeName, rhs.registryEntryStatusCodeName)
                .append(registryEntryTypeCode, rhs.registryEntryTypeCode)
                .append(registryEntryTypeCodeName, rhs.registryEntryTypeCodeName)
                .append(registryEntryNumber, rhs.registryEntryNumber)
                .append(recordSequenceNumber, rhs.recordSequenceNumber)
                .append(recordYear, rhs.recordYear)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .appendSuper(super.hashCode())
                .append(recordsManagementUnit)
                .append(loaneLinks)
                .append(loanedDate)
                .append(numberOfAttachments)
                .append(freedomAssessmentDate)
                .append(dueDate)
                .append(sentDate)
                .append(receivedDate)
                .append(documentDate)
                .append(recordDate)
                .append(registryEntryStatusCode)
                .append(registryEntryStatusCodeName)
                .append(registryEntryTypeCode)
                .append(registryEntryTypeCodeName)
                .append(registryEntryNumber)
                .append(recordSequenceNumber)
                .append(recordYear)
                .toHashCode();
    }
}
