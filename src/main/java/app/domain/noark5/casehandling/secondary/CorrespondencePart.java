package app.domain.noark5.casehandling.secondary;

import app.domain.annotation.LinksObject;
import app.domain.annotation.LinksPacker;
import app.domain.interfaces.entities.secondary.ICorrespondencePartEntity;
import app.domain.noark5.RecordEntity;
import app.domain.noark5.SystemIdEntity;
import app.domain.noark5.bsm.BSMBase;
import app.domain.noark5.metadata.CorrespondencePartType;
import app.webapp.payload.builder.noark5.casehandling.CorrespondencePartLinksBuilder;
import app.webapp.payload.deserializers.noark5.casehandling.CorrespondencePartUnitDeserializer;
import app.webapp.payload.links.casehandling.CorrespondencePartLinks;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;

import java.util.ArrayList;
import java.util.List;

import static app.utils.constants.Constants.*;
import static app.utils.constants.N5ResourceMappings.*;
import static jakarta.persistence.CascadeType.*;
import static jakarta.persistence.FetchType.LAZY;

@Entity
@Table(name = TABLE_CORRESPONDENCE_PART)
@JsonDeserialize(using = CorrespondencePartUnitDeserializer.class)
@LinksPacker(using = CorrespondencePartLinksBuilder.class)
@LinksObject(using = CorrespondencePartLinks.class)
//@Audited

public class CorrespondencePart
        extends SystemIdEntity
        implements ICorrespondencePartEntity {

    /**
     * M??? - korrespondansepartTypeKode kode (xs:string)
     */
    @NotNull
    @Column(name = CORRESPONDENCE_PART_TYPE_CODE, nullable = false)
    //@Audited
    private String correspondencePartTypeCode;

    /**
     * M??? - korrespondansepartTypeKodenavn name (xs:string)
     */
    @Column(name = CORRESPONDENCE_PART_TYPE_CODE_NAME)
    //@Audited
    private String correspondencePartTypeCodeName;

    // Link to Record
    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = FOREIGN_KEY_RECORD_PK)
    private RecordEntity referenceRecordEntity;

    // Links to businessSpecificMetadata (virksomhetsspesifikkeMetadata)
    @OneToMany(mappedBy = REFERENCE_CORRESPONDENCE_PART,
            cascade = {PERSIST, MERGE, REMOVE})
    private final List<BSMBase> referenceBSMBase = new ArrayList<>();

    @Override
    public CorrespondencePartType getCorrespondencePartType() {
        if (null == correspondencePartTypeCode)
            return null;
        return new CorrespondencePartType(correspondencePartTypeCode,
                correspondencePartTypeCodeName);
    }

    @Override
    public void setCorrespondencePartType(
            CorrespondencePartType correspondencePartType) {
        if (null != correspondencePartType) {
            this.correspondencePartTypeCode = correspondencePartType.getCode();
            this.correspondencePartTypeCodeName = correspondencePartType.getCodeName();
        } else {
            this.correspondencePartTypeCode = null;
            this.correspondencePartTypeCodeName = null;
        }
    }

    @Override
    public List<BSMBase> getReferenceBSMBase() {
        return referenceBSMBase;
    }

    @Override
    public void addReferenceBSMBase(List<BSMBase> bSMBase) {
        this.referenceBSMBase.addAll(bSMBase);
        for (BSMBase bsm : referenceBSMBase) {
            bsm.setReferenceCorrespondencePart(this);
        }
    }

    @Override
    public void addBSMBase(BSMBase bsmBase) {
        this.referenceBSMBase.add(bsmBase);
        bsmBase.setReferenceCorrespondencePart(this);
    }

    @Override
    public void removeBSMBase(BSMBase bsmBase) {
        this.referenceBSMBase.remove(bsmBase);
        bsmBase.setReferenceCorrespondencePart(null);
    }

    public RecordEntity getReferenceRecordEntity() {
        return referenceRecordEntity;
    }

    public void setReferenceRecord(RecordEntity referenceRecordEntity) {
        this.referenceRecordEntity = referenceRecordEntity;
    }

    @Override
    public String getFunctionalTypeName() {
        return NOARK_FONDS_STRUCTURE_PATH;
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public boolean equals(Object other) {
        return super.equals(other);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
