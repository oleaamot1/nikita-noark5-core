package app.domain.noark5.casehandling.secondary;

import app.domain.annotation.LinksObject;
import app.domain.annotation.LinksPacker;
import app.domain.interfaces.entities.secondary.ICorrespondencePartInternalEntity;
import app.domain.noark5.admin.AdministrativeUnit;
import app.domain.noark5.admin.User;
import app.webapp.payload.builder.noark5.casehandling.CorrespondencePartInternalLinksBuilder;
import app.webapp.payload.deserializers.noark5.casehandling.CorrespondencePartInternalDeserializer;
import app.webapp.payload.links.casehandling.CorrespondencePartInternalLinks;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import static app.utils.constants.Constants.REL_FONDS_STRUCTURE_CORRESPONDENCE_PART_INTERNAL;
import static app.utils.constants.Constants.TABLE_CORRESPONDENCE_PART_INTERNAL;
import static app.utils.constants.N5ResourceMappings.ADMINISTRATIVE_UNIT_FIELD_ENG;
import static app.utils.constants.N5ResourceMappings.CORRESPONDENCE_PART_INTERNAL;
import static jakarta.persistence.FetchType.LAZY;

@Entity
@Table(name = TABLE_CORRESPONDENCE_PART_INTERNAL)
@JsonDeserialize(using = CorrespondencePartInternalDeserializer.class)
@LinksPacker(using = CorrespondencePartInternalLinksBuilder.class)
@LinksObject(using = CorrespondencePartInternalLinks.class)

public class CorrespondencePartInternal
        extends CorrespondencePart
        implements ICorrespondencePartInternalEntity {

    /**
     * M305 - administrativEnhet (xs:string)
     */
    @Column(name = ADMINISTRATIVE_UNIT_FIELD_ENG)
    //@Audited
    private String administrativeUnit;

    /**
     * M307 - saksbehandler (xs:string)
     */
    @Column(name = "case_handler")
//@Audited
    private String caseHandler;

    @ManyToOne(fetch = LAZY)
    private AdministrativeUnit referenceAdministrativeUnit;

    @ManyToOne(fetch = LAZY)
    private User user;

    public String getAdministrativeUnit() {
        return administrativeUnit;
    }

    public void setAdministrativeUnit(String administrativeUnit) {
        this.administrativeUnit = administrativeUnit;
    }

    public AdministrativeUnit getReferenceAdministrativeUnit() {
        return referenceAdministrativeUnit;
    }

    public void setReferenceAdministrativeUnit(
            AdministrativeUnit referenceAdministrativeUnit) {
        this.referenceAdministrativeUnit = referenceAdministrativeUnit;
    }

    public String getCaseHandler() {
        return caseHandler;
    }

    public void setCaseHandler(String caseHandler) {
        this.caseHandler = caseHandler;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String getBaseTypeName() {
        return CORRESPONDENCE_PART_INTERNAL;
    }

    @Override
    public String getBaseRel() {
        return REL_FONDS_STRUCTURE_CORRESPONDENCE_PART_INTERNAL;
    }

    @Override
    public String toString() {
        return "CorrespondencePartInternal{" + super.toString() +
                "administrativeUnit='" + administrativeUnit + '\'' +
                ", caseHandler='" + caseHandler + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (other.getClass() != getClass()) {
            return false;
        }
        CorrespondencePartInternal rhs = (CorrespondencePartInternal) other;
        return new EqualsBuilder()
                .appendSuper(super.equals(other))
                .append(administrativeUnit, rhs.administrativeUnit)
                .append(caseHandler, rhs.caseHandler)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .appendSuper(super.hashCode())
                .append(administrativeUnit)
                .append(caseHandler)
                .toHashCode();
    }
}
