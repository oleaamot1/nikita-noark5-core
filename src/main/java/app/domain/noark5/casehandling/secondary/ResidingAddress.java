package app.domain.noark5.casehandling.secondary;

import app.domain.interfaces.entities.ISystemId;
import app.domain.noark5.SystemIdEntity;
import app.domain.noark5.secondary.PartPerson;
import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;

import static app.utils.constants.Constants.TABLE_RESIDING_ADDRESS;
import static jakarta.persistence.FetchType.LAZY;

@Entity
@Table(name = TABLE_RESIDING_ADDRESS)

public class ResidingAddress
        extends SystemIdEntity
        implements ISystemId {

    @Embedded
    private SimpleAddress simpleAddress;

    @OneToOne(fetch = LAZY)
    CorrespondencePartPerson referenceCorrespondencePartPerson;

    @OneToOne(fetch = LAZY)
    private PartPerson partPerson;

    public SimpleAddress getSimpleAddress() {
        return simpleAddress;
    }

    public void setSimpleAddress(SimpleAddress simpleAddress) {
        this.simpleAddress = simpleAddress;
    }

    public CorrespondencePartPerson getCorrespondencePartPerson() {
        return referenceCorrespondencePartPerson;
    }

    public void setCorrespondencePartPerson(
            CorrespondencePartPerson referenceCorrespondencePartPerson) {
        this.referenceCorrespondencePartPerson = referenceCorrespondencePartPerson;
    }

    public PartPerson getPartPerson() {
        return partPerson;
    }

    public void setPartPerson(PartPerson partPerson) {
        this.partPerson = partPerson;
    }
}
