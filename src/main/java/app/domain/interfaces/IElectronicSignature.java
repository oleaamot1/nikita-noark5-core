package app.domain.interfaces;

import app.domain.noark5.secondary.ElectronicSignature;

/**
 * Created by tsodring on 12/7/16.
 */
public interface IElectronicSignature {
    ElectronicSignature getReferenceElectronicSignature();

    void setReferenceElectronicSignature(ElectronicSignature electronicSignature);
}
