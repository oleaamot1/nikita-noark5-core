package app.domain.interfaces.entities.secondary;

import app.domain.interfaces.entities.INoarkEntity;
import app.domain.noark5.admin.User;
import app.domain.noark5.casehandling.RecordNote;
import app.domain.noark5.casehandling.RegistryEntry;
import app.domain.noark5.metadata.FlowStatus;

import java.time.OffsetDateTime;
import java.util.UUID;

public interface IDocumentFlowEntity
        extends INoarkEntity {

    String getFlowTo();
    void setFlowTo(String flowTo);

    UUID getReferenceFlowToSystemID();
    void setReferenceFlowToSystemID(UUID referenceFlowToSystemID);
    User getReferenceFlowTo();
    void setReferenceFlowTo(User referenceFlowTo);

    String getFlowFrom();
    void setFlowFrom(String flowFrom);

    UUID getReferenceFlowFromSystemID();
    void setReferenceFlowFromSystemID(UUID referenceFlowFromSystemID);
    User getReferenceFlowFrom();
    void setReferenceFlowFrom(User referenceFlowFrom);

    OffsetDateTime getFlowReceivedDate();
    void setFlowReceivedDate(OffsetDateTime flowReceivedDate);

    OffsetDateTime getFlowSentDate();
    void setFlowSentDate(OffsetDateTime flowSentDate);

    FlowStatus getFlowStatus();

    void setFlowStatus(FlowStatus flowStatus);

    String getFlowComment();

    void setFlowComment(String flowComment);

    RegistryEntry getReferenceRegistryEntry();

    void setReferenceRegistryEntry(RegistryEntry referenceRegistryEntry);

    void removeReferenceRegistryEntry();

    RecordNote getReferenceRecordNote();

    void setReferenceRecordNote(RecordNote referenceRecordNote);

    void removeReferenceRecordNote();
}
