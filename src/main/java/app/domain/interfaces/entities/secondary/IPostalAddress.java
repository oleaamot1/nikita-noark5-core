package app.domain.interfaces.entities.secondary;


import app.domain.noark5.casehandling.secondary.PostalAddress;

/**
 * Created by tsodring on 31/03/19.
 */
public interface IPostalAddress {

    PostalAddress getPostalAddress();

    void setPostalAddress(PostalAddress PostalAddress);
}
