package app.domain.interfaces.entities.secondary;

/**
 * Created by tsodring on 11/07/19.
 */
public interface IPartUnitEntity
        extends IGenericUnitEntity, IPartEntity {
}
