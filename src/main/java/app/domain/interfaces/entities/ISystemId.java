package app.domain.interfaces.entities;

import java.util.UUID;

public interface ISystemId
        extends INoarkEntity {
    UUID getSystemId();

    String getSystemIdAsString();

    void setSystemId(UUID systemId);
}
