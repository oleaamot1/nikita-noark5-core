package app.domain.interfaces.entities;

import app.domain.noark5.metadata.AccessRestriction;
import app.domain.noark5.metadata.ScreeningDocument;
import app.domain.noark5.secondary.ScreeningMetadataLocal;

import java.io.Serializable;
import java.time.OffsetDateTime;
import java.util.Set;

/**
 * Created by tsodring on 12/7/16.
 */
public interface IScreeningEntity extends Serializable {
    AccessRestriction getAccessRestriction();

    void setAccessRestriction(AccessRestriction accessRestriction);

    String getScreeningAuthority();

    void setScreeningAuthority(String screeningAuthority);

    Set<ScreeningMetadataLocal> getReferenceScreeningMetadata();

    void addReferenceScreeningMetadata(ScreeningMetadataLocal screeningMetadata);

    void removeReferenceScreeningMetadata(ScreeningMetadataLocal screeningMetadata);

    ScreeningDocument getScreeningDocument();

    void setScreeningDocument(ScreeningDocument screeningDocument);

    OffsetDateTime getScreeningExpiresDate();

    void setScreeningExpiresDate(OffsetDateTime screeningExpiresDate);

    Integer getScreeningDuration();

    void setScreeningDuration(Integer screeningDuration);
}
