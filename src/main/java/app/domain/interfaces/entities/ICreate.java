package app.domain.interfaces.entities;

import java.time.OffsetDateTime;

public interface ICreate {

    OffsetDateTime getCreatedDate();

    void setCreatedDate(OffsetDateTime createdDate);

    String getCreatedBy();

    void setCreatedBy(String createdBy);
}
