package app.domain.interfaces.entities;

import java.io.Serializable;
import java.time.OffsetDateTime;

public interface IDisposalUndertakenEntity extends Serializable {
    String getDisposalBy();
    void setDisposalBy(String disposalBy);
    OffsetDateTime getDisposalDate();
    void setDisposalDate(OffsetDateTime disposalDate);
}
