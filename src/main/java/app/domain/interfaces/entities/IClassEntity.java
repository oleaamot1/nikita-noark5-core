package app.domain.interfaces.entities;

import app.domain.interfaces.*;
import app.domain.noark5.Class;
import app.domain.noark5.ClassificationSystem;
import app.domain.noark5.File;
import app.domain.noark5.RecordEntity;
import app.domain.noark5.secondary.Keyword;

import java.util.List;

public interface IClassEntity
        extends INoarkGeneralEntity, IKeyword, IDisposal, IScreening,
        IClassified, ICrossReference {

    String getClassId();

    void setClassId(String fileId);

    void removeKeyword(Keyword keyword);

    ClassificationSystem getReferenceClassificationSystem();

    void setReferenceClassificationSystem(
            ClassificationSystem referenceClassificationSystem);

    Class getReferenceParentClass();

    void setReferenceParentClass(Class referenceParentClass);

    List<Class> getReferenceChildClass();

    void setReferenceChildClass(List<Class> referenceChildClass);

    List<File> getReferenceFile();

    void setReferenceFile(List<File> referenceFile);

    List<RecordEntity> getReferenceRecordEntity();

    void setReferenceRecord(List<RecordEntity> referenceRecordEntity);
}
