package app.domain.interfaces.entities.admin;

import app.domain.interfaces.entities.ICreate;
import app.domain.interfaces.entities.IFinalise;
import app.domain.interfaces.entities.ISystemId;
import app.domain.noark5.admin.AdministrativeUnit;

import java.io.Serializable;
import java.util.List;

/**
 * Created by tsodring on 1/16/17.
 */
public interface IAdministrativeUnitEntity
        extends ISystemId, ICreate, IFinalise, Serializable {

    String getShortName();

    void setShortName(String shortName);

    String getAdministrativeUnitName();

    void setAdministrativeUnitName(String administrativeUnitName);

    String getAdministrativeUnitStatus();

    void setAdministrativeUnitStatus(String administrativeUnitStatus);

    AdministrativeUnit getParentAdministrativeUnit();

    void setParentAdministrativeUnit(AdministrativeUnit referenceParentAdministrativeUnit);

    List<AdministrativeUnit> getReferenceChildAdministrativeUnit();

    void setReferenceChildAdministrativeUnit(List<AdministrativeUnit> referenceChildAdministrativeUnit);
}
