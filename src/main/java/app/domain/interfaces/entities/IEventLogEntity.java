package app.domain.interfaces.entities;

import app.domain.noark5.metadata.EventType;

import java.time.OffsetDateTime;

public interface IEventLogEntity
        extends IChangeLogEntity {

    EventType getEventType();

    void setEventType(EventType eventType);

    String getDescription();

    void setDescription(String description);

    OffsetDateTime getEventDate();

    void setEventDate(OffsetDateTime eventDate);
}
