package app.domain.interfaces;

import app.domain.noark5.secondary.Keyword;

import java.util.Set;

public interface IKeyword {
    Set<Keyword> getReferenceKeyword();

    void addKeyword(Keyword keyword);
}
