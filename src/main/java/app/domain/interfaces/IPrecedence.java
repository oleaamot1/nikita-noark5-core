package app.domain.interfaces;

import app.domain.noark5.secondary.Precedence;

import java.util.Set;

public interface IPrecedence {
    Set<Precedence> getReferencePrecedence();
    void addPrecedence(Precedence precedence);

    void removePrecedence(Precedence precedence);
}
