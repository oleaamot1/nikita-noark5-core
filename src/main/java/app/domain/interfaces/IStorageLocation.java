package app.domain.interfaces;

import app.domain.noark5.secondary.StorageLocation;

import java.util.Set;

public interface IStorageLocation {
    Set<StorageLocation> getReferenceStorageLocation();

    void addReferenceStorageLocation(StorageLocation storageLocation);

    void removeReferenceStorageLocation(StorageLocation storageLocation);
}
