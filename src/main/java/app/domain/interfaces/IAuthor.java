package app.domain.interfaces;

import app.domain.noark5.secondary.Author;

import java.util.List;

public interface IAuthor {
    List<Author> getReferenceAuthor();
    void addAuthor(Author author);
    void removeAuthor(Author author);
}
