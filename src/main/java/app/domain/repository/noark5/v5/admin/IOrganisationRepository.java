package app.domain.repository.noark5.v5.admin;

import app.domain.noark5.admin.Organisation;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.ListCrudRepository;

import java.util.UUID;

public interface IOrganisationRepository
        extends CrudRepository<Organisation, UUID>,
        ListCrudRepository<Organisation, UUID> {

    Organisation findBySystemId(UUID systemId);
}
