package app.domain.repository.noark5.v5.metadata;

import app.domain.noark5.metadata.FileType;

public interface IFileTypeRepository
        extends IMetadataRepository<FileType, String> {

}
