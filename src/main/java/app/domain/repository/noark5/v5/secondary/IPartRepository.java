package app.domain.repository.noark5.v5.secondary;

import app.domain.noark5.secondary.Part;
import app.domain.repository.noark5.v5.NoarkEntityRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;


@Repository
public interface IPartRepository extends
        NoarkEntityRepository<Part, UUID> {

}
