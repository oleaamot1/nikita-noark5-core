package app.domain.repository.noark5.v5;

import app.domain.noark5.DocumentDescription;
import app.domain.noark5.DocumentObject;
import app.domain.noark5.admin.Organisation;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface IDocumentObjectRepository extends
        CrudRepository<DocumentObject, UUID> {

    DocumentObject findBySystemId(UUID systemId);

    long deleteByOwnedBy(Organisation ownedBy);

    Long countByReferenceDocumentDescriptionAndVariantFormatCode(
            DocumentDescription documentDescription, String variantFormatCode);
}
