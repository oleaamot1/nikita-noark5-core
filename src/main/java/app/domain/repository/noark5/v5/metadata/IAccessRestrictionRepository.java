package app.domain.repository.noark5.v5.metadata;

import app.domain.noark5.metadata.AccessRestriction;

public interface IAccessRestrictionRepository
        extends IMetadataRepository<AccessRestriction, String> {

}
