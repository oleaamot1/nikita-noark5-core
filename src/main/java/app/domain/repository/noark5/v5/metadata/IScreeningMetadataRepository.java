package app.domain.repository.noark5.v5.metadata;

import app.domain.noark5.metadata.ScreeningMetadata;

public interface IScreeningMetadataRepository
        extends IMetadataRepository<ScreeningMetadata, String> {

}
