package app.domain.repository.noark5.v5.metadata;

import app.domain.noark5.metadata.SeriesStatus;

public interface ISeriesStatusRepository
        extends IMetadataRepository<SeriesStatus, String> {

}
