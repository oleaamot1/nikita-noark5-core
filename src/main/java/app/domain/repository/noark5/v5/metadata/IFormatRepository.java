package app.domain.repository.noark5.v5.metadata;

import app.domain.noark5.metadata.Format;

public interface IFormatRepository
        extends IMetadataRepository<Format, String> {
}
