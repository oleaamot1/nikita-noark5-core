package app.domain.repository.noark5.v5.metadata;

import app.domain.noark5.metadata.FondsStatus;

public interface IFondsStatusRepository
        extends IMetadataRepository<FondsStatus, String> {

}
