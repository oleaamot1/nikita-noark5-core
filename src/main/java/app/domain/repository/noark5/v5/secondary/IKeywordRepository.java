package app.domain.repository.noark5.v5.secondary;

import app.domain.noark5.secondary.Keyword;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface IKeywordRepository
        extends CrudRepository<Keyword, UUID> {
    Keyword findBySystemId(UUID systemId);
}
