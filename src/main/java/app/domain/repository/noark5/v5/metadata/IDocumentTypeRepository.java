package app.domain.repository.noark5.v5.metadata;

import app.domain.noark5.metadata.DocumentType;

public interface IDocumentTypeRepository
        extends IMetadataRepository<DocumentType, String> {

}
