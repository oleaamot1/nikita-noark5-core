package app.domain.repository.noark5.v5.metadata;

import app.domain.noark5.metadata.ElectronicSignatureSecurityLevel;

public interface IElectronicSignatureSecurityLevelRepository
        extends IMetadataRepository<ElectronicSignatureSecurityLevel, String> {

}
