package app.controller.noark5.secondary;

import app.domain.noark5.secondary.StorageLocation;
import app.service.interfaces.secondary.IStorageLocationService;
import app.webapp.exceptions.NikitaException;
import app.webapp.payload.links.secondary.StorageLocationLinks;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

import static app.utils.constants.Constants.*;
import static app.utils.constants.HATEOASConstants.*;
import static app.utils.constants.N5ResourceMappings.*;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping(value = HREF_BASE_FONDS_STRUCTURE + SLASH + STORAGE_LOCATION,
        produces = NOARK5_V5_CONTENT_TYPE_JSON)
public class StorageLocationController {

    private final IStorageLocationService storageLocationService;

    public StorageLocationController(
            IStorageLocationService storageLocationService) {
        this.storageLocationService = storageLocationService;
    }

    // API - All GET Requests (CRUD - READ)
    // GET [contextPath][api]/arkivstruktur/oppbevaringssted/
    // https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/oppbevaringssted/
    @Operation(summary = "Retrieves multiple StorageLocation entities limited " +
            "by ownership rights")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = OK_VAL,
                    description = "StorageLocation found"),
            @ApiResponse(
                    responseCode = UNAUTHORIZED_VAL,
                    description = API_MESSAGE_UNAUTHENTICATED_USER),
            @ApiResponse(
                    responseCode = FORBIDDEN_VAL,
                    description = API_MESSAGE_UNAUTHORISED_FOR_USER),
            @ApiResponse(
                    responseCode = INTERNAL_SERVER_ERROR_VAL,
                    description = API_MESSAGE_INTERNAL_SERVER_ERROR)})
    @GetMapping()
    public ResponseEntity<StorageLocationLinks> findAllStorageLocation() {
        StorageLocationLinks storageLocationLinks =
                storageLocationService.findAll();
        return ResponseEntity.status(OK)
                .body(storageLocationLinks);
    }

    // GET [contextPath][api]/arkivstruktur/oppbevaringssted/{systemID}
    @Operation(summary = "Retrieves a single StorageLocation entity given a systemID")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = OK_VAL,
                    description = "StorageLocation returned"),
            @ApiResponse(
                    responseCode = UNAUTHORIZED_VAL,
                    description = API_MESSAGE_UNAUTHENTICATED_USER),
            @ApiResponse(
                    responseCode = FORBIDDEN_VAL,
                    description = API_MESSAGE_UNAUTHORISED_FOR_USER),
            @ApiResponse(
                    responseCode = INTERNAL_SERVER_ERROR_VAL,
                    description = API_MESSAGE_INTERNAL_SERVER_ERROR)})
    @GetMapping(value = SLASH + SYSTEM_ID_PARAMETER)
    public ResponseEntity<StorageLocationLinks> findStorageLocationBySystemId(
            @Parameter(name = SYSTEM_ID,
                    description = "systemID of the StorageLocation to retrieve",
                    required = true)
            @PathVariable(SYSTEM_ID) final UUID systemID) {
        StorageLocationLinks storageLocationLinks =
                storageLocationService.findBySystemId(systemID);
        return ResponseEntity.status(OK)
                .body(storageLocationLinks);
    }

    // PUT [contextPath][api]/arkivstruktur/oppbevaringssted/{systemID}
    @Operation(summary = "Updates a StorageLocation identified by a given systemID",
            description = "Returns the newly updated nationalIdentifierPerson")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = OK_VAL,
                    description = "StorageLocation " +
                            API_MESSAGE_OBJECT_ALREADY_PERSISTED),
            @ApiResponse(
                    responseCode = CREATED_VAL,
                    description = "StorageLocation " +
                            API_MESSAGE_OBJECT_SUCCESSFULLY_CREATED),
            @ApiResponse(
                    responseCode = UNAUTHORIZED_VAL,
                    description = API_MESSAGE_UNAUTHENTICATED_USER),
            @ApiResponse(
                    responseCode = FORBIDDEN_VAL,
                    description = API_MESSAGE_UNAUTHORISED_FOR_USER),
            @ApiResponse(
                    responseCode = NOT_FOUND_VAL,
                    description = API_MESSAGE_PARENT_DOES_NOT_EXIST +
                            " of type StorageLocation"),
            @ApiResponse(
                    responseCode = CONFLICT_VAL,
                    description = API_MESSAGE_CONFLICT),
            @ApiResponse(
                    responseCode = INTERNAL_SERVER_ERROR_VAL,
                    description = API_MESSAGE_INTERNAL_SERVER_ERROR)})
    @PutMapping(value = SLASH + SYSTEM_ID_PARAMETER,
            consumes = NOARK5_V5_CONTENT_TYPE_JSON)
    public ResponseEntity<StorageLocationLinks> updateStorageLocationBySystemId(
            @Parameter(name = SYSTEM_ID,
                    description = "systemID of StorageLocation to update",
                    required = true)
            @PathVariable(SYSTEM_ID) final UUID systemID,
            @Parameter(name = "StorageLocation",
                    description = "Incoming StorageLocation object",
                    required = true)
            @RequestBody StorageLocation storageLocation) throws NikitaException {
        StorageLocationLinks storageLocationLinks =
                storageLocationService.updateStorageLocationBySystemId(systemID,
                        storageLocation);
        return ResponseEntity.status(OK)
                .body(storageLocationLinks);
    }

    // DELETE [contextPath][api]/arkivstruktur/oppbevaringssted/{systemID}/
    @Operation(summary = "Deletes a single StorageLocation entity identified by " +
            "systemID")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = OK_VAL,
                    description = "StorageLocation deleted"),
            @ApiResponse(
                    responseCode = UNAUTHORIZED_VAL,
                    description = API_MESSAGE_UNAUTHENTICATED_USER),
            @ApiResponse(
                    responseCode = FORBIDDEN_VAL,
                    description = API_MESSAGE_UNAUTHORISED_FOR_USER),
            @ApiResponse(
                    responseCode = INTERNAL_SERVER_ERROR_VAL,
                    description = API_MESSAGE_INTERNAL_SERVER_ERROR)})
    @DeleteMapping(value = SLASH + SYSTEM_ID_PARAMETER)
    public ResponseEntity<String> deleteStorageLocationBySystemId(
            @Parameter(name = SYSTEM_ID,
                    description = "systemID of the storageLocation to delete",
                    required = true)
            @PathVariable(SYSTEM_ID) final UUID systemID) {
        storageLocationService.deleteStorageLocationBySystemId(systemID);
        return ResponseEntity.status(NO_CONTENT)
                .body(DELETE_RESPONSE);
    }
}
