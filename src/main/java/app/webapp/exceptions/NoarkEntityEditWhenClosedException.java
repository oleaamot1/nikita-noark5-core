package app.webapp.exceptions;

public class NoarkEntityEditWhenClosedException extends NikitaException {

    public NoarkEntityEditWhenClosedException(final String message) {
        super(message);
    }
}
