package app.webapp.exceptions;

public class NoarkODataSyntaxProcessException extends NikitaException {

    public NoarkODataSyntaxProcessException(final String message) {
        super(message);
    }
}
