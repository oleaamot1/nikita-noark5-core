package app.webapp.payload.links.secondary;

import app.domain.interfaces.entities.INoarkEntity;
import app.webapp.model.SearchResultsPage;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.secondary.PartSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import static app.utils.constants.N5ResourceMappings.PART;

@JsonSerialize(using = PartSerializer.class)
public class PartLinks
        extends LinksNoarkObject
        implements ILinksNoarkObject {

    public PartLinks(INoarkEntity entity) {
        super(entity);
    }

    public PartLinks(SearchResultsPage page) {
        super(page, PART);
    }
}
