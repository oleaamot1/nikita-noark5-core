package app.webapp.payload.links;

import app.domain.interfaces.entities.INoarkEntity;
import app.webapp.model.SearchResultsPage;
import app.webapp.payload.serializers.noark5.ChangeLogSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import static app.utils.constants.N5ResourceMappings.CHANGE_LOG;

@JsonSerialize(using = ChangeLogSerializer.class)
public class ChangeLogLinks
        extends LinksNoarkObject
        implements ILinksNoarkObject {

    public ChangeLogLinks(INoarkEntity entity) {
        super(entity);
    }

    public ChangeLogLinks(SearchResultsPage page) {
        super(page, CHANGE_LOG);
    }
}
