package app.webapp.payload.links.admin;

import app.domain.interfaces.entities.INoarkEntity;
import app.webapp.model.SearchResultsPage;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.admin.OrganisationSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import static app.utils.constants.N5ResourceMappings.ORGANISATION;

@JsonSerialize(using = OrganisationSerializer.class)
public class OrganisationLinks
        extends LinksNoarkObject
        implements ILinksNoarkObject {

    public OrganisationLinks(INoarkEntity entity) {
        super(entity);
    }

    public OrganisationLinks(SearchResultsPage page) {
        super(page, ORGANISATION);
    }
}
