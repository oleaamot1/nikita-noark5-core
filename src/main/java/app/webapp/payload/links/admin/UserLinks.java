package app.webapp.payload.links.admin;

import app.domain.interfaces.entities.INoarkEntity;
import app.webapp.model.SearchResultsPage;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.admin.UserSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import static app.utils.constants.N5ResourceMappings.USER;

@JsonSerialize(using = UserSerializer.class)
public class UserLinks
        extends LinksNoarkObject
        implements ILinksNoarkObject {

    public UserLinks(INoarkEntity entity) {
        super(entity);
    }

    public UserLinks(SearchResultsPage page) {
        super(page, USER);
    }
}
