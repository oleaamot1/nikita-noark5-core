package app.webapp.payload.links.nationalidentifier;

import app.domain.interfaces.entities.INoarkEntity;
import app.webapp.model.SearchResultsPage;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.nationalidentifier.SocialSecurityNumberSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import static app.utils.constants.N5ResourceMappings.SOCIAL_SECURITY_NUMBER;

@JsonSerialize(using = SocialSecurityNumberSerializer.class)
public class SocialSecurityNumberLinks
        extends LinksNoarkObject
        implements ILinksNoarkObject {

    public SocialSecurityNumberLinks(INoarkEntity entity) {
        super(entity);
    }

    public SocialSecurityNumberLinks(SearchResultsPage page) {
        super(page, SOCIAL_SECURITY_NUMBER);
    }
}
