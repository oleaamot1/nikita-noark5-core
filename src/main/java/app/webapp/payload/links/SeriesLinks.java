package app.webapp.payload.links;

import app.domain.interfaces.entities.INoarkEntity;
import app.webapp.model.SearchResultsPage;
import app.webapp.payload.serializers.noark5.SeriesSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import static app.utils.constants.N5ResourceMappings.SERIES;

@JsonSerialize(using = SeriesSerializer.class)
public class SeriesLinks
        extends LinksNoarkObject
        implements ILinksNoarkObject {

    public SeriesLinks(INoarkEntity entity) {
        super(entity);
    }

    public SeriesLinks(SearchResultsPage page) {
        super(page, SERIES);
    }
}
