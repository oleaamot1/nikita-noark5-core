package app.webapp.payload.deserializers.noark5;

import app.domain.noark5.ChangeLog;
import app.webapp.payload.deserializers.SystemIdEntityDeserializer;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;
import java.util.UUID;

import static app.utils.constants.N5ResourceMappings.*;

public class ChangeLogDeserializer<C extends ChangeLog>
        extends SystemIdEntityDeserializer<ChangeLog> {
    @Override
    public ChangeLog deserialize(JsonParser jsonParser, DeserializationContext dc)
            throws IOException {
               ChangeLog changeLog = new ChangeLog();
        ObjectNode objectNode = mapper.readTree(jsonParser);
        deserializeChangeLog(changeLog, objectNode, errors);
        check_payload_at_end(errors, objectNode);
        return changeLog;
    }

    public void deserializeChangeLog(ChangeLog changeLog, ObjectNode objectNode, StringBuilder errors) {
        deserializeSystemIdEntity(changeLog, objectNode, errors);
        UUID referenceArchiveUnit = deserializeUUID(REFERENCE_ARCHIVE_UNIT, objectNode, errors, false);
        if (null != referenceArchiveUnit) {
            changeLog.setReferenceArchiveUnitSystemId(referenceArchiveUnit);
        }
        JsonNode currentNode = objectNode.get(REFERENCE_METADATA);
        if (null != currentNode) {
            changeLog.setReferenceMetadata(currentNode.textValue());
            objectNode.remove(REFERENCE_METADATA);
        }
        changeLog.setChangedDate(deserializeDateTime(CHANGED_DATE, objectNode, errors));
        currentNode = objectNode.get(CHANGED_BY);
        if (null != currentNode) {
            changeLog.setChangedBy(currentNode.textValue());
            objectNode.remove(CHANGED_BY);
        }
        currentNode = objectNode.get(REFERENCE_CHANGED_BY);
        if (null != currentNode) {
            changeLog.setReferenceChangedBy(currentNode.textValue());
            objectNode.remove(REFERENCE_CHANGED_BY);
        }
        currentNode = objectNode.get(OLD_VALUE);
        if (null != currentNode) {
            changeLog.setOldValue(currentNode.textValue());
            objectNode.remove(OLD_VALUE);
        }
        currentNode = objectNode.get(NEW_VALUE);
        if (null != currentNode) {
            changeLog.setNewValue(currentNode.textValue());
            objectNode.remove(NEW_VALUE);
        }
    }

    @Override
    protected String getType() {
        return CHANGE_LOG;
    }
}