package app.webapp.payload.deserializers.noark5;

import app.domain.noark5.EventLog;
import app.domain.noark5.SystemIdEntity;
import app.domain.noark5.metadata.EventType;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.*;

public class EventLogDeserializer<C extends SystemIdEntity>
        extends ChangeLogDeserializer<EventLog> {

    @Override
    public EventLog deserialize(JsonParser jsonParser, DeserializationContext dc) throws IOException {
               EventLog eventLog = new EventLog();
        ObjectNode objectNode = mapper.readTree(jsonParser);
        deserializeChangeLog(eventLog, objectNode, errors);
        EventType eventType = (EventType)
                deserializeMetadataValue(objectNode, EVENT_TYPE,
                        new EventType(), errors, true);
        eventLog.setEventType(eventType);
        JsonNode currentNode = objectNode.get(DESCRIPTION);
        if (null != currentNode) {
            eventLog.setDescription(currentNode.textValue());
            objectNode.remove(DESCRIPTION);
        }
        eventLog.setEventDate(deserializeDateTime(EVENT_DATE, objectNode, errors));
        check_payload_at_end(errors, objectNode);
        return eventLog;
    }

    @Override
    protected String getType() {
        return EVENT_LOG;
    }
}