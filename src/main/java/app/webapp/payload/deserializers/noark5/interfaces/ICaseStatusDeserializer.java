package app.webapp.payload.deserializers.noark5.interfaces;

import app.domain.interfaces.entities.ICaseFileEntity;
import app.domain.noark5.metadata.CaseStatus;
import com.fasterxml.jackson.databind.node.ObjectNode;

import static app.utils.constants.N5ResourceMappings.CASE_STATUS;

public interface ICaseStatusDeserializer
        extends IDeserializer {
    default void deserializeCaseStatus(ICaseFileEntity caseFile,
                                       ObjectNode objectNode, StringBuilder errors) {
        CaseStatus caseStatus = (CaseStatus)
                deserializeMetadataValue(objectNode,
                        CASE_STATUS,
                        new CaseStatus(),
                        errors, false);
        caseFile.setCaseStatus(caseStatus);
    }
}
