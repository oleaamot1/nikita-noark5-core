package app.webapp.payload.deserializers.noark5.interfaces;

import app.domain.interfaces.entities.secondary.ICorrespondencePartEntity;
import app.domain.interfaces.entities.secondary.ICorrespondencePartPersonEntity;
import app.domain.interfaces.entities.secondary.ICorrespondencePartUnitEntity;
import app.domain.interfaces.entities.secondary.IPartEntity;
import app.domain.noark5.casehandling.secondary.*;
import app.domain.noark5.metadata.CorrespondencePartType;
import app.domain.noark5.metadata.PartRole;
import app.utils.constants.N5ResourceMappings;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import static app.utils.constants.N5ResourceMappings.*;

public interface ICorrespondencePartDeserializer
        extends IPartDeserializer {

    default void deserializeCorrespondencePartPersonEntity(
            ICorrespondencePartPersonEntity partPerson,
            ObjectNode objectNode, StringBuilder errors) {
        deserializeCorrespondencePartType(
                partPerson, objectNode, errors);
        deserializeGenericPersonEntity(partPerson, objectNode);
        // Deserialize postalAddress
        JsonNode currentNode = objectNode.get(POSTAL_ADDRESS);
        if (null != currentNode) {
            PostalAddress postalAddress = new PostalAddress();
            SimpleAddress simpleAddress = new SimpleAddress();
            deserializeSimpleAddressEntity(POSTAL_ADDRESS,
                    simpleAddress, currentNode.deepCopy(), errors);
            postalAddress.setSimpleAddress(simpleAddress);
            postalAddress.setReferenceCorrespondencePartPerson(
                    (CorrespondencePartPerson) partPerson);
            partPerson.setPostalAddress(postalAddress);
            objectNode.remove(N5ResourceMappings.POSTAL_ADDRESS);
        }
        // Deserialize residingAddress
        currentNode = objectNode.get(RESIDING_ADDRESS);
        if (null != currentNode) {
            ResidingAddress residingAddress =
                    new ResidingAddress();
            SimpleAddress simpleAddress = new SimpleAddress();
            deserializeSimpleAddressEntity(RESIDING_ADDRESS,
                    simpleAddress, currentNode.deepCopy(), errors);
            residingAddress.setSimpleAddress(simpleAddress);
            residingAddress.
                    setCorrespondencePartPerson(
                            (CorrespondencePartPerson) partPerson);
            partPerson.setResidingAddress(residingAddress);
            objectNode.remove(RESIDING_ADDRESS);
        }
        // Deserialize kontaktinformasjon
        currentNode = objectNode.get(CONTACT_INFORMATION);
        if (null != currentNode) {
            ContactInformation contactInformation =
                    new ContactInformation();
            deserializeContactInformationEntity(
                    contactInformation, currentNode.deepCopy());
            partPerson.
                    setContactInformation(contactInformation);
            objectNode.remove(CONTACT_INFORMATION);
        }
    }


    default void deserializeCorrespondencePartUnitEntity(
            ICorrespondencePartUnitEntity correspondencePartUnit,
            ObjectNode objectNode, StringBuilder errors) {
        deserializeCorrespondencePartType(correspondencePartUnit,
                objectNode, errors);
        // Deserialize enhetsidentifikator
        JsonNode currentNode = objectNode.get(UNIT_IDENTIFIER);
        // { "enhetsidentifikator": { "organisasjonsnummer": "123458"}}
        if (null != currentNode) {
            currentNode = currentNode.get(ORGANISATION_NUMBER);
            if (null != currentNode) {
                correspondencePartUnit.setUnitIdentifier(
                        currentNode.textValue());
            }
            objectNode.remove(UNIT_IDENTIFIER);
        }
        // Deserialize kontaktperson
        currentNode = objectNode.get(CONTACT_PERSON);
        if (null != currentNode) {
            correspondencePartUnit.setContactPerson(
                    currentNode.textValue());
            objectNode.remove(CONTACT_PERSON);
        }
        // Deserialize navn
        currentNode = objectNode.get(NAME);
        if (null != currentNode) {
            correspondencePartUnit.setName(currentNode.textValue());
            objectNode.remove(NAME);
        }
        // Deserialize postadresse
        currentNode = objectNode.get(POSTAL_ADDRESS);
        if (null != currentNode) {
            PostalAddress postalAddressEntity = new PostalAddress();
            SimpleAddress simpleAddress = new SimpleAddress();
            deserializeSimpleAddressEntity(POSTAL_ADDRESS,
                    simpleAddress, currentNode.deepCopy(), errors);
            postalAddressEntity.setSimpleAddress(simpleAddress);
            correspondencePartUnit.
                    setPostalAddress(postalAddressEntity);
            objectNode.remove(N5ResourceMappings.POSTAL_ADDRESS);
        }
        // Deserialize forretningsadresse
        currentNode = objectNode.get(BUSINESS_ADDRESS);
        if (null != currentNode) {
            BusinessAddress businessAddressEntity =
                    new BusinessAddress();
            SimpleAddress simpleAddress = new SimpleAddress();
            deserializeSimpleAddressEntity(BUSINESS_ADDRESS,
                    simpleAddress, currentNode.deepCopy(), errors);
            businessAddressEntity.setSimpleAddress(simpleAddress);
            correspondencePartUnit.setBusinessAddress(
                    businessAddressEntity);
            objectNode.remove(BUSINESS_ADDRESS);
        }
        // Deserialize kontaktinformasjon
        currentNode = objectNode.get(CONTACT_INFORMATION);
        if (null != currentNode) {
            ContactInformation contactInformation =
                    new ContactInformation();
            deserializeContactInformationEntity(
                    contactInformation, currentNode.deepCopy());
            correspondencePartUnit.
                    setContactInformation(contactInformation);
            objectNode.remove(CONTACT_INFORMATION);
        }
    }

    default void deserializeCorrespondencePartType(
            ICorrespondencePartEntity correspondencePart,
            ObjectNode objectNode, StringBuilder errors) {
        // Deserialize korrespondanseparttype
        CorrespondencePartType entity = (CorrespondencePartType)
                deserializeMetadataValue(objectNode,
                        CORRESPONDENCE_PART_TYPE,
                        new CorrespondencePartType(),
                        errors, true);
        correspondencePart.setCorrespondencePartType(entity);
    }

    default void deserializePartRole(
            IPartEntity part,
            ObjectNode objectNode, StringBuilder errors) {
        // Deserialize partrole
        PartRole entity = (PartRole)
                deserializeMetadataValue(objectNode,
                        PART_ROLE_FIELD,
                        new PartRole(),
                        errors, true);
        part.setPartRole(entity);
    }
}
