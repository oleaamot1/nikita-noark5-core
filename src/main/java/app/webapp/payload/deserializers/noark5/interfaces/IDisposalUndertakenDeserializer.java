package app.webapp.payload.deserializers.noark5.interfaces;

import app.domain.interfaces.entities.IDisposalUndertakenEntity;
import app.domain.noark5.secondary.DisposalUndertaken;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import static app.utils.constants.N5ResourceMappings.*;

public interface IDisposalUndertakenDeserializer
        extends IDeserializer {

    default DisposalUndertaken deserializeDisposalUndertaken(
            ObjectNode objectNode, StringBuilder errors) {
        DisposalUndertaken disposalUndertaken = null;
        JsonNode disposalUndertakenNode = objectNode.get(DISPOSAL_UNDERTAKEN);
        if (disposalUndertakenNode != null) {
            disposalUndertaken = new DisposalUndertaken();
            deserializeDisposalUndertakenEntity(disposalUndertaken, objectNode, errors);
            objectNode.remove(DISPOSAL_UNDERTAKEN);
        }
        return disposalUndertaken;
    }

    default void deserializeDisposalUndertakenEntity(IDisposalUndertakenEntity disposalUndertakenEntity,
                                                     ObjectNode objectNode, StringBuilder errors) {
        // Deserialize disposalBy
        JsonNode currentNode = objectNode.get(DISPOSAL_UNDERTAKEN_BY);
        if (null != currentNode) {
            disposalUndertakenEntity.setDisposalBy(currentNode.textValue());
            objectNode.remove(DISPOSAL_UNDERTAKEN_BY);
        }
        // Deserialize disposalDate
        disposalUndertakenEntity.setDisposalDate(deserializeDate(DISPOSAL_UNDERTAKEN_DATE, objectNode, errors));
    }

}
