package app.webapp.payload.deserializers.noark5.nationalidentifier;

import app.domain.noark5.nationalidentifier.Unit;
import app.webapp.payload.deserializers.SystemIdEntityDeserializer;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;

import static app.utils.constants.Constants.UNIT;
import static app.utils.constants.N5ResourceMappings.ORGANISATION_NUMBER;

public class UnitDeserializer
        extends SystemIdEntityDeserializer<Unit> {
    @Override
    public Unit deserialize(JsonParser jsonParser, DeserializationContext dc) throws IOException {
               Unit unit = new Unit();
        ObjectNode objectNode = mapper.readTree(jsonParser);
        // Deserialize systemID
        deserializeNoarkSystemIdEntity(unit, objectNode);
        // Deserialize organisasjonsnummer
        JsonNode currentNode = objectNode.get(ORGANISATION_NUMBER);
        if (null != currentNode) {
            unit.setUnitIdentifier(currentNode.textValue());
            objectNode.remove(ORGANISATION_NUMBER);
        }
        check_payload_at_end(errors, objectNode);
        return unit;
    }

    @Override
    protected String getType() {
        return UNIT;
    }
}
