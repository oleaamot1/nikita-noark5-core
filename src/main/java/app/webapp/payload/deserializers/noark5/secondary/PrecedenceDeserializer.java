package app.webapp.payload.deserializers.noark5.secondary;

import app.domain.noark5.metadata.PrecedenceStatus;
import app.domain.noark5.secondary.Precedence;
import app.webapp.payload.deserializers.NoarkGeneralEntityDeserializer;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;
import java.util.UUID;

import static app.utils.constants.N5ResourceMappings.*;

public class PrecedenceDeserializer
        extends NoarkGeneralEntityDeserializer<Precedence> {

    @Override
    public Precedence deserialize(JsonParser jsonParser, DeserializationContext dc) throws IOException {
               Precedence precedence = new Precedence();
        ObjectNode objectNode = mapper.readTree(jsonParser);
        deserializeNoarkSystemIdEntity(precedence, objectNode);
        deserializeNoarkCreateEntity(precedence, objectNode, errors);
        deserializeNoarkTitleDescriptionEntity(precedence, objectNode, errors);
        deserializeNoarkFinaliseEntity(precedence, objectNode, errors);
        // Deserialize precedenceDate
        precedence.setPrecedenceDate(
                deserializeDateTime(PRECEDENCE_DATE, objectNode, errors, true));

        // Deserialize precedenceAuthority
        JsonNode currentNode = objectNode.get(PRECEDENCE_AUTHORITY);
        if (null != currentNode) {
            precedence.setPrecedenceAuthority(currentNode.textValue());
            objectNode.remove(PRECEDENCE_AUTHORITY);
        }
        // Deserialize sourceOfLaw
        currentNode = objectNode.get(PRECEDENCE_SOURCE_OF_LAW);
        if (null != currentNode) {
            precedence.setSourceOfLaw(currentNode.textValue());
            objectNode.remove(PRECEDENCE_SOURCE_OF_LAW);
        } else {
            errors.append(PRECEDENCE_SOURCE_OF_LAW + " is missing. ");
        }
        // Deserialize precedenceApprovedBy
        currentNode = objectNode.get(PRECEDENCE_APPROVED_BY);
        if (null != currentNode) {
            precedence.setPrecedenceApprovedBy(currentNode.textValue());
            objectNode.remove(PRECEDENCE_APPROVED_BY);
        }
        // Deserialize referansePresedensGodkjentAv
        UUID referencePrecedenceApprovedBy =
                deserializeUUID(PRECEDENCE_REFERENCE_APPROVED_BY,
                        objectNode, errors, false);
        if (null != referencePrecedenceApprovedBy) {
            precedence.setReferencePrecedenceApprovedBySystemID
                    (referencePrecedenceApprovedBy);
        }
        // Deserialize presedensStatus
        PrecedenceStatus precedenceStatus = (PrecedenceStatus)
                deserializeMetadataValue(
                        objectNode,
                        PRECEDENCE_PRECEDENCE_STATUS,
                        new PrecedenceStatus(),
                        errors, false);
        precedence.setPrecedenceStatus(precedenceStatus);
        // Deserialize precedenceApprovedDate
        precedence.setPrecedenceApprovedDate(deserializeDateTime(PRECEDENCE_APPROVED_DATE, objectNode, errors));
        check_payload_at_end(errors, objectNode);
        return precedence;
    }

    @Override
    protected String getType() {
        return PRECEDENCE;
    }
}