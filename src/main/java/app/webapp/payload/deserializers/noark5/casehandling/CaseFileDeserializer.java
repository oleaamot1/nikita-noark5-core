package app.webapp.payload.deserializers.noark5.casehandling;

import app.domain.noark5.Series;
import app.domain.noark5.casehandling.CaseFile;
import app.webapp.payload.deserializers.NoarkGeneralEntityDeserializer;
import app.webapp.payload.deserializers.noark5.interfaces.*;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;
import java.util.UUID;

import static app.utils.constants.N5ResourceMappings.*;

/**
 * Deserialize an incoming CaseFile JSON object.
 */
public class CaseFileDeserializer
        extends NoarkGeneralEntityDeserializer<CaseFile>
        implements ICaseStatusDeserializer, IClassifiedDeserializer, IDeletionDeserializer, IDisposalDeserializer,
        IDisposalUndertakenDeserializer, IDocumentMediumDeserializer, IElectronicSignatureDeserializer,
        IKeywordDeserializer, IScreeningDeserializer, IStorageLocationDeserializer, ITitleAndDescriptionDeserializer {

    @Override
    public CaseFile deserialize(JsonParser jsonParser, DeserializationContext dc) throws IOException {
        CaseFile caseFile = new CaseFile();
        ObjectNode objectNode = mapper.readTree(jsonParser);
        // Deserialize properties for File
        deserializeNoarkGeneralEntity(caseFile, objectNode, errors);
        deserializeDocumentMedium(caseFile, objectNode, errors);
        deserializeStorageLocation(caseFile, objectNode);
        deserializeKeyword(caseFile, objectNode);
        // Deserialize fileId
        JsonNode currentNode = objectNode.get(FILE_ID);
        if (null != currentNode) {
            caseFile.setFileId(currentNode.textValue());
            objectNode.remove(FILE_ID);
        }
        // Deserialize publicTitle
        currentNode = objectNode.get(FILE_PUBLIC_TITLE);
        if (null != currentNode) {
            caseFile.setPublicTitle(currentNode.textValue());
            objectNode.remove(FILE_PUBLIC_TITLE);
        }
        // Deserialize general properties for CaseFile
        // Deserialize caseYear
        caseFile.setCaseYear(deserializeInteger(CASE_YEAR, objectNode, errors, false));
        // Deserialize caseSequenceNumber
        caseFile.setCaseSequenceNumber(deserializeInteger(CASE_SEQUENCE_NUMBER, objectNode, errors, false));
        // Deserialize caseDate
        caseFile.setCaseDate(deserializeDateTime(CASE_DATE, objectNode, errors));
        // Deserialize caseResponsible
        currentNode = objectNode.get(CASE_RESPONSIBLE);
        if (null != currentNode) {
            caseFile.setCaseResponsible(currentNode.textValue());
            objectNode.remove(CASE_RESPONSIBLE);
        }
        // Deserialize recordsManagementUnit
        currentNode = objectNode.get(CASE_RECORDS_MANAGEMENT_UNIT);
        if (null != currentNode) {
            caseFile.setRecordsManagementUnit(currentNode.textValue());
            objectNode.remove(CASE_RECORDS_MANAGEMENT_UNIT);
        }
        deserializeCaseStatus(caseFile, objectNode, errors);
        // Deserialize loanedDate
        caseFile.setLoanedDate(deserializeDateTime(CASE_LOANED_DATE, objectNode, errors));
        // Deserialize loaneLinks
        currentNode = objectNode.get(CASE_LOANED_TO);
        if (null != currentNode) {
            caseFile.setLoaneLinks(currentNode.textValue());
            objectNode.remove(CASE_LOANED_TO);
        }
        caseFile.setReferenceDisposal(deserializeDisposal(objectNode, errors));
        caseFile.setReferenceScreening(deserializeScreening(objectNode, errors));
        caseFile.setReferenceClassified(deserializeClassified(objectNode, errors));
        // Deserialize referenceSeries
        currentNode = objectNode.get(REFERENCE_SERIES);
        if (null != currentNode) {
            Series series = new Series();
            String systemID = currentNode.textValue();
            if (systemID != null) {
                series.setSystemId(UUID.fromString(systemID));
            }
            caseFile.setReferenceSeries(series);
            objectNode.remove(REFERENCE_SERIES);
        }
        check_payload_at_end(errors, objectNode);
        return caseFile;
    }

    @Override
    protected String getType() {
        return CASE_FILE;
    }
}