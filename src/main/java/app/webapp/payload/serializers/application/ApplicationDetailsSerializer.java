package app.webapp.payload.serializers.application;

import app.webapp.model.ApplicationDetails;
import app.webapp.model.ConformityLevel;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;

import static app.utils.constants.HATEOASConstants.*;

public class ApplicationDetailsSerializer
        extends StdSerializer<ApplicationDetails> {

    public ApplicationDetailsSerializer() {
        super(ApplicationDetails.class);
    }

    @Override
    public void serialize(ApplicationDetails applicationDetails, JsonGenerator jgen, SerializerProvider provider)
            throws IOException {
        jgen.writeStartObject();
        jgen.writeObjectFieldStart(LINKS);
        for (ConformityLevel conformityLevel : applicationDetails.getConformityLevels()) {
            jgen.writeObjectFieldStart(conformityLevel.getRel());
            jgen.writeStringField(HREF, conformityLevel.getHref());
            jgen.writeEndObject();
        }
        jgen.writeObjectFieldStart(SELF);
        jgen.writeStringField(HREF, applicationDetails.getSelfHref());
        jgen.writeEndObject();
        jgen.writeEndObject();
        jgen.writeEndObject();
    }
}
