package app.webapp.payload.serializers.noark5;

import app.domain.annotation.LinksObject;
import app.domain.annotation.LinksPacker;
import app.domain.interfaces.entities.IEventLogEntity;
import app.domain.interfaces.entities.INoarkEntity;
import app.webapp.payload.builder.noark5.EventLogLinksBuilder;
import app.webapp.payload.links.EventLogLinks;
import app.webapp.payload.links.LinksNoarkObject;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.*;

@LinksPacker(using = EventLogLinksBuilder.class)
@LinksObject(using = EventLogLinks.class)
public class EventLogSerializer
        extends SystemIdEntitySerializer {
    @Override
    public void serializeNoarkEntity(INoarkEntity noarkEntity, LinksNoarkObject eventLogLinks, JsonGenerator jgen)
            throws IOException {
        IEventLogEntity eventLog = (IEventLogEntity) noarkEntity;
        jgen.writeStartObject();

        if (eventLog != null) {
            serializeEventLog(eventLog, jgen);
        }
        printLinks(jgen, eventLogLinks.getLinks(eventLog));
        jgen.writeEndObject();
    }

    public void serializeEventLog(IEventLogEntity eventLog, JsonGenerator jgen) throws IOException {
        printNullableMetadata(jgen, EVENT_TYPE, eventLog.getEventType());
        printNullable(jgen, DESCRIPTION, eventLog.getDescription());
        printDateTime(jgen, EVENT_DATE, eventLog.getEventDate());
    }
}
