package app.webapp.payload.serializers.noark5.interfaces;

import app.domain.interfaces.entities.IRecordNoteEntity;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;

/**
 * A RegistryEntry is very similar to RecordNote. The main difference is that in a RecordNote, the fields are
 * voluntary, while in a RegistryEntry most fields are obligatory.
 */
public interface IRegistryEntryPrint
        extends IRecordNotePrint {

    default void printRegistryEntryEntity(JsonGenerator jgen, IRecordNoteEntity recordNote) throws IOException {
        printRecordNoteEntity(jgen, recordNote);
    }
}
