
package app.webapp.payload.serializers.noark5.secondary;

import app.domain.interfaces.entities.INoarkEntity;
import app.domain.noark5.secondary.Precedence;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.NoarkGeneralEntitySerializer;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.*;

/**
 * Serialize an outgoing Precedence object as JSON.
 */
public class PrecedenceSerializer
        extends NoarkGeneralEntitySerializer {

    @Override
    public void serializeNoarkEntity(INoarkEntity noarkEntity, LinksNoarkObject precedenceLinks, JsonGenerator jgen)
            throws IOException {
        Precedence precedence = (Precedence) noarkEntity;
        jgen.writeStartObject();
        printSystemIdEntity(jgen, precedence);
        printNullableDateTime(jgen, PRECEDENCE_DATE, precedence.getPrecedenceDate());
        printCreateEntity(jgen, precedence);
        printTitleAndDescription(jgen, precedence);
        printNullable(jgen, PRECEDENCE_AUTHORITY, precedence.getPrecedenceAuthority());
        printNullable(jgen, PRECEDENCE_SOURCE_OF_LAW, precedence.getSourceOfLaw());
        printNullableDateTime(jgen, PRECEDENCE_APPROVED_DATE, precedence.getPrecedenceApprovedDate());
        printNullable(jgen, PRECEDENCE_APPROVED_BY, precedence.getPrecedenceApprovedBy());
        printFinaliseEntity(jgen, precedence);
        printNullableMetadata(jgen, PRECEDENCE_PRECEDENCE_STATUS, precedence.getPrecedenceStatus());
        printLinks(jgen, precedenceLinks.getLinks(precedence));
        jgen.writeEndObject();
    }
}
