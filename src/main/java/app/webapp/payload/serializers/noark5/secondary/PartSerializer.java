package app.webapp.payload.serializers.noark5.secondary;

import app.domain.interfaces.entities.INoarkEntity;
import app.domain.interfaces.entities.secondary.IGenericUnitEntity;
import app.domain.interfaces.entities.secondary.IPartEntity;
import app.domain.interfaces.entities.secondary.IPartPersonEntity;
import app.domain.interfaces.entities.secondary.IPartUnitEntity;
import app.domain.noark5.secondary.Part;
import app.domain.noark5.secondary.PartPerson;
import app.domain.noark5.secondary.PartUnit;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.SystemIdEntitySerializer;
import app.webapp.payload.serializers.noark5.interfaces.IPartPrint;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.PART_ROLE_FIELD;

/**
 * Serialize an outgoing Part object as JSON.
 */
public class PartSerializer
        extends SystemIdEntitySerializer
        implements IPartPrint {

    @Override
    public void serializeNoarkEntity(INoarkEntity noarkEntity, LinksNoarkObject partLinks, JsonGenerator jgen)
            throws IOException {
        Part part = (Part) noarkEntity;
        jgen.writeStartObject();
        if (part instanceof PartPerson) {
            printPartPerson(jgen, (IPartPersonEntity) part);
        }
        if (part instanceof PartUnit) {
            printPartUnit(jgen, (IPartUnitEntity) part);
        }
        printCreateEntity(jgen, part);
        printModifiedEntity(jgen, part);
        printBSM(jgen, part);
        printLinks(jgen, partLinks.getLinks(part));
        jgen.writeEndObject();
    }

    public void printPartPerson(JsonGenerator jgen, IPartPersonEntity person) throws IOException {
        printPart(jgen, person);
        printGenericPerson(jgen, person);
    }

    public void printPartUnit(JsonGenerator jgen, IGenericUnitEntity unit) throws IOException {
        printPart(jgen, (IPartEntity) unit);
        printGenericUnit(jgen, unit);
    }

    protected void printPart(JsonGenerator jgen, IPartEntity part) throws IOException {
        if (part != null) {
            printSystemIdEntity(jgen, part);
            printNullableMetadata(jgen, PART_ROLE_FIELD, part.getPartRole());
        }
    }
}
