package app.webapp.payload.serializers.noark5.nationalidentifier;

import app.domain.interfaces.entities.INoarkEntity;
import app.domain.noark5.nationalidentifier.Unit;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.SystemIdEntitySerializer;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.ORGANISATION_NUMBER;

public class UnitSerializer
        extends SystemIdEntitySerializer {

    @Override
    public void serializeNoarkEntity(INoarkEntity noarkEntity, LinksNoarkObject unitLinks,
                                     JsonGenerator jgen) throws IOException {
        Unit unit = (Unit) noarkEntity;
        jgen.writeStartObject();
        printSystemIdEntity(jgen, unit);
        printNullable(jgen, ORGANISATION_NUMBER, unit.getUnitIdentifier());
        printLinks(jgen, unitLinks.getLinks(unit));
        jgen.writeEndObject();
    }
}
