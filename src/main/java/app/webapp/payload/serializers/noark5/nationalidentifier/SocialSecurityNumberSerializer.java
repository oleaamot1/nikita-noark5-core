package app.webapp.payload.serializers.noark5.nationalidentifier;

import app.domain.interfaces.entities.INoarkEntity;
import app.domain.noark5.nationalidentifier.SocialSecurityNumber;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.SystemIdEntitySerializer;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.SOCIAL_SECURITY_NUMBER;

public class SocialSecurityNumberSerializer
        extends SystemIdEntitySerializer {

    @Override
    public void serializeNoarkEntity(INoarkEntity noarkEntity, LinksNoarkObject socialSecurityNumberLinks,
                                     JsonGenerator jgen) throws IOException {

        SocialSecurityNumber socialSecurityNumber = (SocialSecurityNumber) noarkEntity;
        jgen.writeStartObject();
        printSystemIdEntity(jgen, socialSecurityNumber);
        printNullable(jgen, SOCIAL_SECURITY_NUMBER, socialSecurityNumber.getSocialSecurityNumber());
        printLinks(jgen, socialSecurityNumberLinks.getLinks(socialSecurityNumber));
        jgen.writeEndObject();
    }
}
