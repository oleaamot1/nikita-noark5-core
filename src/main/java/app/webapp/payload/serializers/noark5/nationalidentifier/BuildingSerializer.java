package app.webapp.payload.serializers.noark5.nationalidentifier;

import app.domain.interfaces.entities.INoarkEntity;
import app.domain.noark5.nationalidentifier.Building;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.SystemIdEntitySerializer;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.BUILDING_CHANGE_NUMBER;
import static app.utils.constants.N5ResourceMappings.BUILDING_NUMBER;

public class BuildingSerializer
        extends SystemIdEntitySerializer {

    @Override
    public void serializeNoarkEntity(INoarkEntity noarkEntity, LinksNoarkObject buildingLinks,
                                     JsonGenerator jgen) throws IOException {

        Building building = (Building) noarkEntity;
        jgen.writeStartObject();
        printSystemIdEntity(jgen, building);
        printNullable(jgen, BUILDING_NUMBER, building.getBuildingNumber());
        printNullable(jgen, BUILDING_CHANGE_NUMBER, building.getRunningChangeNumber());
        printLinks(jgen, buildingLinks.getLinks(building));
        jgen.writeEndObject();
    }
}
