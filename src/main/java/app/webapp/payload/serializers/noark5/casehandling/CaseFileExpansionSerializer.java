package app.webapp.payload.serializers.noark5.casehandling;

import app.domain.interfaces.entities.INoarkEntity;
import app.domain.noark5.casehandling.CaseFile;
import app.webapp.payload.links.LinksNoarkObject;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;

/**
 * Serialize an outgoing expanded CaseFile object as JSON.
 */
public class CaseFileExpansionSerializer
        extends CaseFileSerializer {

    @Override
    public void serializeNoarkEntity(INoarkEntity noarkEntity, LinksNoarkObject caseFileLinks, JsonGenerator jgen)
            throws IOException {
        CaseFile caseFile = (CaseFile) noarkEntity;
        jgen.writeStartObject();
        printCaseFileEntity(jgen, caseFile);
        printLinks(jgen, caseFileLinks.getLinks(caseFile));
        jgen.writeEndObject();
    }
}
