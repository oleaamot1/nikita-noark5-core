package app.webapp.payload.serializers.noark5;

import app.domain.interfaces.entities.INoarkEntity;
import app.domain.interfaces.entities.IRecordEntity;
import app.domain.noark5.RecordEntity;
import app.domain.noark5.casehandling.RecordNote;
import app.domain.noark5.casehandling.RegistryEntry;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.interfaces.*;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.*;

/**
 * Serialize an outgoing record object as JSON.
 */
public class RecordSerializer
        extends NoarkGeneralEntitySerializer
        implements IRecordNotePrint, IRegistryEntryPrint, IClassifiedPrint, ICrossReferencePrint, IDisposalPrint,
        IDocumentMediumPrint, IElectronicSignaturePrint, IScreeningPrint {

    @Override
    public void serializeNoarkEntity(INoarkEntity noarkEntity, LinksNoarkObject recordLinks, JsonGenerator jgen)
            throws IOException {
        RecordEntity record = (RecordEntity) noarkEntity;
        jgen.writeStartObject();
        printRecordEntity(jgen, record);
        if (record instanceof RegistryEntry) {
            printRecordNoteEntity(jgen, (RegistryEntry) record);
            printRegistryEntryEntity(jgen, (RegistryEntry) record);
        } else if (record instanceof RecordNote) {
            printRecordNoteEntity(jgen, (RecordNote) record);
        }
        printDisposal(jgen, record);
        printScreening(jgen, record);
        printClassified(jgen, record);
        printDocumentMedium(jgen, record);
        // TODO: FIX THIS
        //  printCrossReference(jgen, record);
        if (record instanceof RegistryEntry) {
            printElectronicSignature(jgen, (RegistryEntry) record);
        }
        printBSM(jgen, record);
        printLinks(jgen, recordLinks.getLinks(record));
        jgen.writeEndObject();
    }

    public void printRecordEntity(JsonGenerator jgen,
                                  IRecordEntity record)
            throws IOException {
        printSystemIdEntity(jgen, record);
        printCreateEntity(jgen, record);
        printNullableDateTime(jgen, RECORD_ARCHIVED_DATE,
                record.getArchivedDate());
        printNullable(jgen, RECORD_ARCHIVED_BY,
                record.getArchivedBy());
        printNullableDateTime(jgen, RECORD_ARCHIVED_DATE, record.getArchivedDate());
        printNullable(jgen, RECORD_ARCHIVED_BY, record.getArchivedBy());
        printNullable(jgen, RECORD_ID, record.getRecordId());
        printTitleAndDescription(jgen, record);
        printNullable(jgen, FILE_PUBLIC_TITLE, record.getPublicTitle());
    }
}
