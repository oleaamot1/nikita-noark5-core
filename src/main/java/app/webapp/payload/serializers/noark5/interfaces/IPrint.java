package app.webapp.payload.serializers.noark5.interfaces;

import app.domain.interfaces.entities.IMetadataEntity;
import app.webapp.exceptions.NikitaMisconfigurationException;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;
import java.time.OffsetDateTime;
import java.util.UUID;

import static app.utils.constants.N5ResourceMappings.CODE;
import static app.utils.constants.N5ResourceMappings.CODE_NAME;
import static java.time.format.DateTimeFormatter.ISO_OFFSET_DATE;
import static java.time.format.DateTimeFormatter.ISO_OFFSET_DATE_TIME;

public interface IPrint
        extends ISystemIDPrint {

    default void checkNull(String fieldName, Object value) {
        if (null == value) {
            String msg = "When serialising [" + fieldName + "], there was no " +
                    "associated value. This field is mandatory";
            throw new NikitaMisconfigurationException(msg);
        }
    }

    default String formatDate(OffsetDateTime value) {
        return value.format(ISO_OFFSET_DATE);
    }

    default String formatDateTime(OffsetDateTime value) {
        return value.format(ISO_OFFSET_DATE_TIME);
    }

    default void printCode(JsonGenerator jgen, String code, String name) throws IOException {
        jgen.writeStringField(CODE, code);
        if (name != null) {
            jgen.writeStringField(CODE_NAME, name);
        }
    }

    default void printCode(JsonGenerator jgen, IMetadataEntity metadataEntity) throws IOException {
        printCode(jgen, metadataEntity.getCode(), metadataEntity.getCodeName());
    }

    default void printNullable(JsonGenerator jgen, String fieldName, String value) throws IOException {
        if (null != value)
            jgen.writeStringField(fieldName, value);
    }

    default void printNullable(JsonGenerator jgen, String fieldName, Boolean value) throws IOException {
        if (null != value)
            jgen.writeBooleanField(fieldName, value);
    }

    default void printNullable(JsonGenerator jgen, String fieldName, Integer value) throws IOException {
        if (null != value)
            jgen.writeNumberField(fieldName, value);
    }

    default void printNullable(JsonGenerator jgen, String fieldName, Long value) throws IOException {
        if (null != value)
            jgen.writeNumberField(fieldName, value);
    }

    default void printNullable(JsonGenerator jgen, String fieldName, Double value) throws IOException {
        if (null != value)
            jgen.writeNumberField(fieldName, value);
    }

    default void printNullable(JsonGenerator jgen, String fieldName, UUID value) throws IOException {
        if (null != value)
            jgen.writeStringField(fieldName, value.toString());
    }

    default void printNullableMetadata(JsonGenerator jgen, String fieldName, IMetadataEntity m) throws IOException {
        if (null != m && null != m.getCode()) {
            jgen.writeObjectFieldStart(fieldName);
            printCode(jgen, m);
            jgen.writeEndObject();
        }
    }

    default void printNullableDate(JsonGenerator jgen, String fieldName, OffsetDateTime value) throws IOException {
        if (null != value)
            jgen.writeStringField(fieldName, formatDate(value));
    }

    default void printNullableDateTime(JsonGenerator jgen, String fieldName, OffsetDateTime value) throws IOException {
        if (null != value)
            jgen.writeStringField(fieldName, formatDateTime(value));
    }
}
