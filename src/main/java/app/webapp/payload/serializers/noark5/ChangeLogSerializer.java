package app.webapp.payload.serializers.noark5;

import app.domain.annotation.LinksObject;
import app.domain.annotation.LinksPacker;
import app.domain.interfaces.entities.IChangeLogEntity;
import app.domain.interfaces.entities.IEventLogEntity;
import app.domain.interfaces.entities.INoarkEntity;
import app.webapp.payload.builder.noark5.ChangeLogLinksBuilder;
import app.webapp.payload.links.ChangeLogLinks;
import app.webapp.payload.links.LinksNoarkObject;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.*;

@LinksPacker(using = ChangeLogLinksBuilder.class)
@LinksObject(using = ChangeLogLinks.class)
public class ChangeLogSerializer
        extends EventLogSerializer {
    @Override
    public void serializeNoarkEntity(INoarkEntity noarkEntity, LinksNoarkObject changeLogLinks, JsonGenerator jgen)
            throws IOException {
        IChangeLogEntity changeLog = (IChangeLogEntity) noarkEntity;
        jgen.writeStartObject();
        if (changeLog != null) {
            serializeChangeLog(changeLog, jgen);
            if (changeLog instanceof IEventLogEntity) {
                serializeEventLog((IEventLogEntity) changeLog, jgen);
            }
        }
        printLinks(jgen, changeLogLinks.getLinks(changeLog));
        jgen.writeEndObject();
    }

    public void serializeChangeLog(IChangeLogEntity changeLog, JsonGenerator jgen) throws IOException {
        printSystemIdEntity(jgen, changeLog);
        printNullable(jgen, REFERENCE_ARCHIVE_UNIT, changeLog.getReferenceArchiveUnitSystemId());
        printNullable(jgen, REFERENCE_METADATA, changeLog.getReferenceMetadata());
        printDateTime(jgen, CHANGED_DATE, changeLog.getChangedDate());
        printNullable(jgen, CHANGED_BY, changeLog.getChangedBy());
        printNullable(jgen, REFERENCE_CHANGED_BY, changeLog.getReferenceChangedBy());
        printNullable(jgen, OLD_VALUE, changeLog.getOldValue());
        printNullable(jgen, NEW_VALUE, changeLog.getNewValue());
    }
}
