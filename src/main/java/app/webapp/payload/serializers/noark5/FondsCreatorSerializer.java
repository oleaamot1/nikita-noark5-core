package app.webapp.payload.serializers.noark5;

import app.domain.interfaces.entities.IFondsCreatorEntity;
import app.domain.interfaces.entities.INoarkEntity;
import app.domain.noark5.FondsCreator;
import app.webapp.payload.links.LinksNoarkObject;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.*;

/**
 * Serialize an outgoing FondsCreator object as JSON.
 */
public class FondsCreatorSerializer
        extends SystemIdEntitySerializer {

    @Override
    public void serializeNoarkEntity(INoarkEntity noarkEntity,
                                     LinksNoarkObject fondsCreatorLinks, JsonGenerator jgen)
            throws IOException {
        FondsCreator fondsCreator = (FondsCreator) noarkEntity;
        jgen.writeStartObject();
        printFondsCreator(jgen, fondsCreator);
        printLinks(jgen, fondsCreatorLinks.getLinks(fondsCreator));
        jgen.writeEndObject();
    }

    public void printFondsCreator(JsonGenerator jgen,
                                  IFondsCreatorEntity fondsCreatorEntity)
            throws IOException {
        if (fondsCreatorEntity != null) {
            printNullable(jgen, SYSTEM_ID, fondsCreatorEntity.getSystemIdAsString());
            printNullable(jgen, FONDS_CREATOR_ID, fondsCreatorEntity.getFondsCreatorId());
            printNullable(jgen, FONDS_CREATOR_NAME, fondsCreatorEntity.getFondsCreatorName());
            printNullable(jgen, DESCRIPTION, fondsCreatorEntity.getDescription());
        }
    }
}
