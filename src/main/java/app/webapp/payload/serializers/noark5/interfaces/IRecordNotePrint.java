package app.webapp.payload.serializers.noark5.interfaces;

import app.domain.interfaces.entities.IRecordNoteEntity;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;

import static app.utils.constants.N5ResourceMappings.*;

public interface IRecordNotePrint
        extends IPrint {

    default void printRecordNoteEntity(JsonGenerator jgen, IRecordNoteEntity recordNote) throws IOException {
        if (recordNote != null) {
            printNullableDateTime(jgen, REGISTRY_ENTRY_DOCUMENT_DATE, recordNote.getDocumentDate());
            printNullableDateTime(jgen, REGISTRY_ENTRY_RECEIVED_DATE, recordNote.getReceivedDate());
            printNullableDateTime(jgen, REGISTRY_ENTRY_SENT_DATE, recordNote.getSentDate());
            printNullableDateTime(jgen, REGISTRY_ENTRY_DUE_DATE, recordNote.getDueDate());
            printNullableDateTime(jgen, REGISTRY_ENTRY_RECORD_FREEDOM_ASSESSMENT_DATE,
                    recordNote.getFreedomAssessmentDate());
            if (recordNote.getNumberOfAttachments() != null) {
                jgen.writeNumberField(REGISTRY_ENTRY_NUMBER_OF_ATTACHMENTS, recordNote.getNumberOfAttachments());
            }
            printNullableDateTime(jgen, CASE_LOANED_DATE, recordNote.getLoanedDate());
            printNullable(jgen, CASE_LOANED_TO, recordNote.getLoaneLinks());
        }
    }
}
