package app.webapp.payload.serializers.noark5.metadata;

import app.domain.interfaces.entities.INoarkEntity;
import app.domain.interfaces.entities.ISystemId;
import app.domain.noark5.bsm.BSMBase;
import app.webapp.payload.links.LinksNoarkObject;
import app.webapp.payload.serializers.noark5.SystemIdEntitySerializer;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;

import static app.utils.constants.Constants.*;
import static app.utils.constants.N5ResourceMappings.TYPE;

public class BSMBaseSerializer
        extends SystemIdEntitySerializer {

    @Override
    public void serializeNoarkEntity(INoarkEntity noarkEntity, LinksNoarkObject bsmLinks, JsonGenerator jgen)
            throws IOException {
        BSMBase bsm = (BSMBase) noarkEntity;
        jgen.writeStartObject();
        printSystemIdEntity(jgen, (ISystemId) bsm);
        String type = bsm.getDataType();
        printNullable(jgen, TYPE, bsm.getDataType());
        if (type.equals(TYPE_STRING)) {
            printNullable(jgen, bsm.getValueName(), bsm.getStringValue());
        } else if (type.equals(TYPE_BOOLEAN)) {
            printNullable(jgen, bsm.getValueName(), bsm.getBooleanValue());
        } else if (type.equals(TYPE_DOUBLE)) {
            printNullable(jgen, bsm.getValueName(), bsm.getDoubleValue());
        } else if (type.equals(TYPE_INTEGER)) {
            printNullable(jgen, bsm.getValueName(), bsm.getIntegerValue());
        } else if (type.equals(TYPE_DATE)) {
            printNullableDate(jgen, bsm.getValueName(), bsm.getDateTimeValue());
        } else if (type.equals(TYPE_DATE_TIME)) {
            printNullableDateTime(jgen, bsm.getValueName(),
                    bsm.getDateTimeValue());
        } else if (type.equals(TYPE_URI)) {
            printNullable(jgen, bsm.getValueName(), bsm.getUriValue());
        }
        printLinks(jgen, bsmLinks.getLinks(bsm));
        jgen.writeEndObject();
    }
}
