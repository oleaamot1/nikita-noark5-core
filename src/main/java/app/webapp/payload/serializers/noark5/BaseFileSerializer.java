package app.webapp.payload.serializers.noark5;

import app.domain.interfaces.entities.ICaseFileEntity;
import app.domain.interfaces.entities.IMetadataEntity;
import app.webapp.payload.serializers.noark5.interfaces.ICaseFilePrint;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;
import java.time.OffsetDateTime;
import java.util.UUID;

/**
 * Java does not support multiple inheritance. However, when dealing with Serialising,  multiple inheritance
 * would solve the problems of combining functionality from different classes. However, this code becomes
 * messy so this base class is here to hide the 'mess'
 */
public class BaseFileSerializer
        extends NoarkGeneralEntitySerializer
        implements ICaseFilePrint {
    @Override
    public void printCaseFileEntity(JsonGenerator jgen, ICaseFileEntity caseFile) throws IOException {
        ICaseFilePrint.super.printCaseFileEntity(jgen, caseFile);
    }

    @Override
    public void checkNull(String fieldName, Object value) {
        ICaseFilePrint.super.checkNull(fieldName, value);
    }

    @Override
    public void printNullable(JsonGenerator jgen, String fieldName, String value) throws IOException {
        ICaseFilePrint.super.printNullable(jgen, fieldName, value);
    }

    @Override
    public void printNullable(JsonGenerator jgen, String fieldName, Boolean value) throws IOException {
        ICaseFilePrint.super.printNullable(jgen, fieldName, value);
    }

    @Override
    public void printNullable(JsonGenerator jgen, String fieldName, Integer value) throws IOException {
        ICaseFilePrint.super.printNullable(jgen, fieldName, value);
    }

    @Override
    public void printNullable(JsonGenerator jgen, String fieldName, Long value) throws IOException {
        ICaseFilePrint.super.printNullable(jgen, fieldName, value);
    }

    @Override
    public void printNullable(JsonGenerator jgen, String fieldName, Double value) throws IOException {
        ICaseFilePrint.super.printNullable(jgen, fieldName, value);
    }

    @Override
    public void printNullable(JsonGenerator jgen, String fieldName, UUID value) throws IOException {
        ICaseFilePrint.super.printNullable(jgen, fieldName, value);
    }

    @Override
    public void printNullableMetadata(JsonGenerator jgen, String fieldName, IMetadataEntity m) throws IOException {
        ICaseFilePrint.super.printNullableMetadata(jgen, fieldName, m);
    }

    @Override
    public void printNullableDate(JsonGenerator jgen, String fieldName, OffsetDateTime value) throws IOException {
        ICaseFilePrint.super.printNullableDate(jgen, fieldName, value);
    }

    @Override
    public void printNullableDateTime(JsonGenerator jgen, String fieldName, OffsetDateTime value) throws IOException {
        ICaseFilePrint.super.printNullableDateTime(jgen, fieldName, value);
    }
}
