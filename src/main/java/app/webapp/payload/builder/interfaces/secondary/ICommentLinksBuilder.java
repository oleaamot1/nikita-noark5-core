package app.webapp.payload.builder.interfaces.secondary;

import app.domain.interfaces.entities.secondary.ICommentEntity;
import app.webapp.payload.builder.interfaces.ILinksBuilder;
import app.webapp.payload.links.ILinksNoarkObject;

public interface ICommentLinksBuilder
        extends ILinksBuilder {

    void addCommentType(ICommentEntity comment,
                        ILinksNoarkObject linksNoarkObject);

    void addFile(ICommentEntity comment,
                 ILinksNoarkObject linksNoarkObject);

    void addRecordEntity(ICommentEntity comment,
                         ILinksNoarkObject linksNoarkObject);

    void addDocumentDescription(ICommentEntity comment,
                                ILinksNoarkObject linksNoarkObject);

}
