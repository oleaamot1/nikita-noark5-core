package app.webapp.payload.builder.interfaces.secondary;

import app.domain.interfaces.entities.ISystemId;
import app.webapp.payload.builder.interfaces.ILinksBuilder;
import app.webapp.payload.links.ILinksNoarkObject;

public interface ICrossReferenceLinksBuilder
        extends ILinksBuilder {

    void addRecordEntity(ISystemId entity,
                         ILinksNoarkObject linksNoarkObject);

    void addFile(ISystemId entity,
                 ILinksNoarkObject linksNoarkObject);

    void addClass(ISystemId entity,
                  ILinksNoarkObject linksNoarkObject);
}
