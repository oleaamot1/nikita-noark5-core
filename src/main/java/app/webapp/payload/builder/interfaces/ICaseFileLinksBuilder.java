package app.webapp.payload.builder.interfaces;

import app.domain.interfaces.entities.ISystemId;
import app.webapp.payload.links.ILinksNoarkObject;

/**
 * Created by tsodring on 2/6/17.
 * <p>
 * Describe Hateoas links handler
 */
public interface ICaseFileLinksBuilder
        extends ILinksBuilder {

    void addNewClass(ISystemId entity,
                     ILinksNoarkObject linksNoarkObject);

    void addClass(ISystemId entity,
                  ILinksNoarkObject linksNoarkObject);

    void addNewPrecedence(ISystemId entity,
                          ILinksNoarkObject linksNoarkObject);

    void addPrecedence(ISystemId entity,
                       ILinksNoarkObject linksNoarkObject);

    void addSecondaryClassification(ISystemId entity,
                                    ILinksNoarkObject linksNoarkObject);

    void addNewSecondaryClassification(ISystemId entity,
                                       ILinksNoarkObject linksNoarkObject);

    void addNewRegistryEntry(ISystemId entity,
                             ILinksNoarkObject linksNoarkObject);

    void addRegistryEntry(ISystemId entity,
                          ILinksNoarkObject linksNoarkObject);

    void addNewRecordNote(ISystemId entity,
                          ILinksNoarkObject linksNoarkObject);

    void addRecordEntityNote(ISystemId entity,
                             ILinksNoarkObject linksNoarkObject);

    void addNewSubCaseFile(ISystemId entity,
                           ILinksNoarkObject linksNoarkObject);

    void addMetadataCaseStatus(ISystemId entity,
                               ILinksNoarkObject linksNoarkObject);
}
