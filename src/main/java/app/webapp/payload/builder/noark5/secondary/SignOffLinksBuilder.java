package app.webapp.payload.builder.noark5.secondary;

import app.domain.interfaces.entities.ISystemId;
import app.domain.interfaces.entities.secondary.ISignOffEntity;
import app.domain.noark5.secondary.SignOff;
import app.webapp.payload.builder.interfaces.secondary.ISignOffLinksBuilder;
import app.webapp.payload.builder.noark5.SystemIdLinksBuilder;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.Link;
import org.springframework.stereotype.Component;

import static app.utils.constants.Constants.*;
import static app.utils.constants.N5ResourceMappings.*;
import static app.utils.constants.ODataConstants.DOLLAR_FILTER;

/*
 * Used to add SignOffLinks links with SignOff specific information
 */
@Component("signOffLinksBuilder")
public class SignOffLinksBuilder
        extends SystemIdLinksBuilder
        implements ISignOffLinksBuilder {

    public SignOffLinksBuilder() {
    }

    @Override
    public void addSelfLink(ISystemId entity,
                            ILinksNoarkObject linksNoarkObject) {
        String selfHref = getOutgoingAddress() +
                HREF_BASE_CASE_HANDLING + SLASH + SIGN_OFF + SLASH +
                entity.getSystemId();
        linksNoarkObject.addLink(entity,
                new Link(selfHref, getRelSelfLink()));
        linksNoarkObject.addLink(entity,
                new Link(selfHref, entity.getBaseRel()));
        linksNoarkObject.addLink(entity,
                new Link(selfHref, getRelSelfLink()));
        linksNoarkObject.addLink(entity,
                new Link(selfHref, entity.getBaseRel()));
    }

    @Override
    public void addEntityLinks(ISystemId entity,
                               ILinksNoarkObject linksNoarkObject) {
        SignOff signOff = (SignOff) entity;
        addRegistryEntry(signOff, linksNoarkObject);
        addReferenceSignedOffRegistryEntry(signOff, linksNoarkObject);
        addReferenceSignedOffCorrespondenceParty(signOff, linksNoarkObject);
        addSignOffMethod(signOff, linksNoarkObject);
    }

    @Override
    public void addEntityLinksOnTemplate
            (ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        SignOff signOff = (SignOff) entity;
        addSignOffMethod(signOff, linksNoarkObject);
    }

    /**
     * journalpost?$filter=avskrivning/systemID eq '7f000101-7309-1658-8173-09a829470034'
     * or URL encoded:
     * http://localhost:8092/noark5v5/api/sakarkiv/journalpost?$filter%3Davskrivning%2FsystemID+eq+%277f000101-7309-1658-8173-09a829470034%27
     *
     * @param entity
     * @param linksNoarkObject
     */
    @Override
    public void addRegistryEntry(ISignOffEntity entity,
                                 ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity,
                new Link(getOutgoingAddress() + HREF_BASE_CASE_HANDLING +
                        SLASH + REGISTRY_ENTRY + "?" + urlEncode(DOLLAR_FILTER)
                        + "=" + SIGN_OFF + SLASH + SYSTEM_ID +
                        urlEncode(" eq '" + entity.getSystemId() + "'"),
                        REL_CASE_HANDLING_REGISTRY_ENTRY));
    }

    @Override
    public void addReferenceSignedOffRegistryEntry(ISignOffEntity entity,
                                                   ILinksNoarkObject linksNoarkObject) {
        if (null != entity.getReferenceSignedOffRecord()) {
            linksNoarkObject.addLink(entity,
                    new Link(getOutgoingAddress() +
                            HREF_BASE_CASE_HANDLING + SLASH + REGISTRY_ENTRY + SLASH +
                            entity.getReferenceSignedOffRecord().getSystemId(),
                            REL_CASE_HANDLING_SIGN_OFF_REFERENCE_RECORD));
        }
    }

    @Override
    public void addReferenceSignedOffCorrespondenceParty
            (ISignOffEntity entity, ILinksNoarkObject linksNoarkObject) {
        if (null != entity.getReferenceSignedOffCorrespondencePart()) {
            linksNoarkObject.addLink(entity,
                    new Link(getOutgoingAddress() +
                            HREF_BASE_CASE_HANDLING + SLASH + CORRESPONDENCE_PART + SLASH + entity.getReferenceSignedOffCorrespondencePart().getSystemId(),
                            REL_FONDS_STRUCTURE_CORRESPONDENCE_PART));
        }
    }

    @Override
    public void addSignOffMethod(ISignOffEntity entity,
                                 ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_METADATA + SLASH + SIGN_OFF_METHOD,
                REL_METADATA_SIGN_OFF_METHOD, false));
    }
}
