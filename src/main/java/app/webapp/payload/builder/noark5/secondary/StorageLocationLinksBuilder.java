package app.webapp.payload.builder.noark5.secondary;

import app.domain.interfaces.entities.ISystemId;
import app.domain.noark5.secondary.StorageLocation;
import app.webapp.payload.builder.interfaces.secondary.IStorageLocationLinksBuilder;
import app.webapp.payload.builder.noark5.SystemIdLinksBuilder;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.Link;
import org.springframework.stereotype.Component;

import static app.utils.constants.Constants.*;
import static app.utils.constants.N5ResourceMappings.STORAGE_LOCATION;
import static app.utils.constants.N5ResourceMappings.SYSTEM_ID;
import static app.utils.constants.ODataConstants.DOLLAR_FILTER;

/**
 * Used to add StorageLocationLinks links with StorageLocation specific
 * information
 */
@Component("storageLocationLinksBuilder")
public class StorageLocationLinksBuilder
        extends SystemIdLinksBuilder
        implements IStorageLocationLinksBuilder {

    public StorageLocationLinksBuilder() {
    }

    @Override
    public void addSelfLink(ISystemId entity,
                            ILinksNoarkObject linksNoarkObject) {
        String selfhref = getOutgoingAddress() + HREF_BASE_FONDS_STRUCTURE +
                SLASH + STORAGE_LOCATION + SLASH + entity.getSystemId();
        linksNoarkObject.addLink(entity,
                new Link(selfhref, getRelSelfLink()));
        linksNoarkObject.addLink(entity,
                new Link(selfhref, entity.getBaseRel()));
    }

    @Override
    public void addEntityLinks(ISystemId entity,
                               ILinksNoarkObject linksNoarkObject) {
        addFonds(entity, linksNoarkObject);
        addSeries(entity, linksNoarkObject);
        addFile(entity, linksNoarkObject);
        addRecordEntity(entity, linksNoarkObject);
    }

    /**
     * Create a REL/HREF pair for the parent Fonds associated
     * with the given StorageLocation
     * <p>
     * arkiv?$filter=oppbevaringssted/systemID eq '{systemID}'
     * https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/arkiv/
     *
     * @param storageLocation  The StorageLocation object
     * @param linksNoarkObject linksDocumentObject
     */
    @Override
    public void addFonds(ISystemId storageLocation,
                         ILinksNoarkObject linksNoarkObject) {
        if (((StorageLocation) storageLocation).getReferenceFonds().size() > 0) {
            linksNoarkObject.addLink(storageLocation,
                    new Link(getOutgoingAddress() +
                            HREF_BASE_FONDS + "?" + urlEncode(DOLLAR_FILTER) +
                            "=" + STORAGE_LOCATION + SLASH + SYSTEM_ID +
                            urlEncode(" eq '" + storageLocation.getSystemId() + "'"),
                            REL_FONDS_STRUCTURE_FONDS));
        }
    }

    /**
     * Create a REL/HREF pair for the parent Series associated
     * with the given StorageLocation
     * <p>
     * arkivdel?$filter=oppbevaringssted/systemID eq '{systemID}'
     * https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/arkivdel/
     *
     * @param storageLocation  The StorageLocation object
     * @param linksNoarkObject linksDocumentObject
     */
    @Override
    public void addSeries(ISystemId storageLocation,
                          ILinksNoarkObject linksNoarkObject) {
        if (((StorageLocation) storageLocation).getReferenceSeries().size() > 0) {
            linksNoarkObject.addLink(storageLocation,
                    new Link(getOutgoingAddress() +
                            HREF_BASE_SERIES + "?" + urlEncode(DOLLAR_FILTER) +
                            "=" + STORAGE_LOCATION + SLASH + SYSTEM_ID +
                            urlEncode(" eq '" + storageLocation.getSystemId() + "'"),
                            REL_FONDS_STRUCTURE_SERIES));
        }
    }

    /**
     * Create a REL/HREF pair for the parent File associated
     * with the given StorageLocation
     * <p>
     * mappe?$filter=oppbevaringssted/systemID eq '{systemID}'
     * "https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/mappe/"
     *
     * @param storageLocation    The StorageLocation object
     * @param linksNoarkObject linksDocumentObject
     */
    @Override
    public void addFile(ISystemId storageLocation,
                        ILinksNoarkObject linksNoarkObject) {
        if (((StorageLocation) storageLocation).getReferenceFile().size() > 0) {
            linksNoarkObject.addLink(storageLocation,
                    new Link(getOutgoingAddress() +
                            HREF_BASE_FILE + "?" + urlEncode(DOLLAR_FILTER) +
                            "=" + STORAGE_LOCATION + SLASH + SYSTEM_ID +
                            urlEncode(" eq '" + storageLocation.getSystemId() + "'"),
                            REL_FONDS_STRUCTURE_FILE));
        }
    }

    /**
     * Create a REL/HREF pair for the parent Record associated with the given
     * StorageLocation
     * registrering?$filter=oppbevaringssted/systemID eq '{systemID}'
     * <p>
     * "https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/registrering/"
     *
     * @param storageLocation    StorageLocation
     * @param linksNoarkObject linksDocumentObject
     */
    @Override
    public void addRecordEntity(ISystemId storageLocation,
                                ILinksNoarkObject linksNoarkObject) {
        if (((StorageLocation) storageLocation).getReferenceRecordEntity().size() > 0) {
            linksNoarkObject.addLink(storageLocation,
                    new Link(getOutgoingAddress() +
                            "?" + urlEncode(DOLLAR_FILTER) +
                            "=" + STORAGE_LOCATION + SLASH + SYSTEM_ID +
                            urlEncode(" eq '" + storageLocation.getSystemId() + "'"),
                            REL_FONDS_STRUCTURE_RECORD));
        }
    }
}
