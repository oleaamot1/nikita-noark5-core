package app.webapp.payload.builder.noark5;

import app.domain.interfaces.entities.ISystemId;
import app.domain.noark5.Class;
import app.webapp.payload.builder.interfaces.IClassLinksBuilder;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.Link;
import jakarta.validation.constraints.NotNull;
import org.springframework.stereotype.Component;

import static app.utils.constants.Constants.*;
import static app.utils.constants.N5ResourceMappings.*;

/**
 * Used to add ClassLinks links with Class specific information
 */
@Component("classLinksBuilder")
public class ClassLinksBuilder
        extends SystemIdLinksBuilder
        implements IClassLinksBuilder {

    public ClassLinksBuilder() {
    }

    @Override
    public void addEntityLinks(ISystemId entity,
                               ILinksNoarkObject linksNoarkObject) {

        // links for primary entities
        addClass(entity, linksNoarkObject);
        addNewClass(entity, linksNoarkObject);
        addRegistration(entity, linksNoarkObject);
        addNewRegistration(entity, linksNoarkObject);
        addFile(entity, linksNoarkObject);
        addNewFile(entity, linksNoarkObject);
        addNewCaseFile(entity, linksNoarkObject);
        addClassificationSystem(entity, linksNoarkObject);
        addParentClass(entity, linksNoarkObject);
        addSubClass(entity, linksNoarkObject);
        // links for secondary entities (non-embeddable)
        addCrossReference(entity, linksNoarkObject);
        addNewCrossReference(entity, linksNoarkObject);
        // links for metadata entities
        addAccessRestriction(entity, linksNoarkObject);
        addScreeningDocument(entity, linksNoarkObject);
        addDisposalDecision(entity, linksNoarkObject);
        addNewKeyword(entity, linksNoarkObject);
        addKeyword(entity, linksNoarkObject);
        addClassifiedCodeMetadata(entity, linksNoarkObject);
        addScreeningMetadata(entity, linksNoarkObject);
        addScreeningMetadataLocal(entity, linksNoarkObject);
        addNewScreeningMetadataLocal(entity, linksNoarkObject);
    }

    @Override
    public void addEntityLinksOnTemplate(
            ISystemId entity,
            ILinksNoarkObject linksNoarkObject) {
        super.addEntityLinksOnTemplate(entity, linksNoarkObject);
        addClassifiedCodeMetadata(entity, linksNoarkObject);
        addAccessRestriction(entity, linksNoarkObject);
        addScreeningDocument(entity, linksNoarkObject);
        addScreeningMetadata(entity, linksNoarkObject);
        addAccessRestriction(entity, linksNoarkObject);
        addDisposalDecision(entity, linksNoarkObject);
    }

    /**
     * Create a REL/HREF pair for the parent ClassificationSystem associated
     * with the given Class. Checks if the Class is actually associated with
     * a ClassificationSystem. Note every Class should actually be associated
     * with a ClassificationSystem, but we are not doing that check here.
     * <p>
     * "../api/arkivstruktur/klassifikasjonssystem/1234"
     * "https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/arkivdel/"
     *
     * @param entity           class
     * @param linksNoarkObject linksClass
     */
    @Override
    public void addClassificationSystem(ISystemId entity,
                                        ILinksNoarkObject linksNoarkObject) {
        Class klass = getClass(entity);
        if (klass.getReferenceClassificationSystem() != null) {
            linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                    HREF_BASE_CLASSIFICATION_SYSTEM + SLASH + klass.getReferenceClassificationSystem().getSystemId(),
                    REL_FONDS_STRUCTURE_CLASSIFICATION_SYSTEM));
        }
    }

    /**
     * Create a REL/HREF pair for the parent Class associated with the given
     * Class. Checks if the Class is actually associated with a Class.
     * <p>
     * "../api/arkivstruktur/klasse/1234"
     * "https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/klasse/"
     *
     * @param entity           class
     * @param linksNoarkObject linksClass
     */
    @Override
    public void addClass(ISystemId entity,
                         ILinksNoarkObject linksNoarkObject) {
        Class klass = getClass(entity);
        if (klass.getReferenceParentClass() != null) {
            linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                    HREF_BASE_CLASS + SLASH + klass.getReferenceParentClass().getSystemId(),
                    REL_FONDS_STRUCTURE_CLASS));
        }
    }

    @Override
    public void addNewClass(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_CLASS + SLASH + entity.getSystemId() + SLASH + NEW_CLASS,
                REL_FONDS_STRUCTURE_NEW_CLASS, false));
    }

    @Override
    public void addParentClass(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        Class cls = getClass(entity).getReferenceParentClass();
        if (cls != null) {
            linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                    HREF_BASE_CLASS + SLASH + cls.getSystemId(),
                    REL_FONDS_STRUCTURE_PARENT_CLASS, false));
        }
    }

    @Override
    public void addSubClass(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_CLASS + SLASH + entity.getSystemId() + SLASH + SUB_CLASS,
                REL_FONDS_STRUCTURE_SUB_CLASS, false));
    }

    @Override
    public void addRegistration(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_CLASS + SLASH + entity.getSystemId() + SLASH + RECORD,
                REL_FONDS_STRUCTURE_RECORD, false));
    }

    @Override
    public void addNewRegistration(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_CLASS + SLASH + entity.getSystemId() + SLASH + NEW_RECORD,
                REL_FONDS_STRUCTURE_NEW_RECORD, false));
    }

    @Override
    public void addFile(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_CLASS + SLASH + entity.getSystemId() + SLASH + FILE,
                REL_FONDS_STRUCTURE_FILE, false));
    }

    @Override
    public void addNewFile(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_CLASS + SLASH + entity.getSystemId() + SLASH + NEW_FILE,
                REL_FONDS_STRUCTURE_NEW_FILE, false));
    }

    @Override
    public void addNewCaseFile(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_CLASS + SLASH + entity.getSystemId() + SLASH + NEW_CASE_FILE,
                REL_CASE_HANDLING_NEW_CASE_FILE, false));
    }

    @Override
    public void addCrossReference(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_CLASS + SLASH + entity.getSystemId() + SLASH + CROSS_REFERENCE,
                REL_FONDS_STRUCTURE_CROSS_REFERENCE, false));
    }

    @Override
    public void addNewCrossReference(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_CLASS + SLASH + entity.getSystemId() + SLASH + NEW_CROSS_REFERENCE,
                REL_FONDS_STRUCTURE_NEW_CROSS_REFERENCE, false));
    }

    @Override
    public void addAccessRestriction(ISystemId entity,
                                     ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_METADATA + SLASH + ACCESS_RESTRICTION,
                REL_METADATA_ACCESS_RESTRICTION, false));
    }

    @Override
    public void addScreeningDocument(ISystemId entity,
                                     ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_METADATA + SLASH + SCREENING_DOCUMENT,
                REL_METADATA_SCREENING_DOCUMENT, false));
    }

    @Override
    public void addDisposalDecision(ISystemId entity,
                                    ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_METADATA + SLASH + DISPOSAL_DECISION,
                REL_METADATA_DISPOSAL_DECISION, false));
    }

    @Override
    public void addKeyword(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        if (((Class) entity).getReferenceKeyword().size() > 0) {
            linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                    HREF_BASE_CLASS + SLASH + entity.getSystemIdAsString() +
                    SLASH + KEYWORD, REL_FONDS_STRUCTURE_KEYWORD, true));
        }
    }

    @Override
    public void addNewKeyword(ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_CLASS + SLASH + entity.getSystemIdAsString() + SLASH + NEW_KEYWORD,
                REL_FONDS_STRUCTURE_NEW_KEYWORD, false));
    }

    @Override
    public void addClassifiedCodeMetadata(ISystemId entity,
                                          ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_METADATA + SLASH + CLASSIFIED_CODE,
                REL_METADATA_CLASSIFIED_CODE));
    }

    @Override
    public void addScreeningMetadata(ISystemId entity,
                                     ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_METADATA + SLASH + SCREENING_METADATA,
                REL_METADATA_SCREENING_METADATA));
    }

    @Override
    public void addScreeningMetadataLocal(ISystemId entity,
                                          ILinksNoarkObject linksNoarkObject) {
        if (null != ((Class) entity).getReferenceScreening()) {
            linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                    HREF_BASE_CLASS + SLASH +
                    entity.getSystemId() + SLASH + SCREENING_METADATA,
                    REL_FONDS_STRUCTURE_SCREENING_METADATA));
        }
    }

    @Override
    public void addNewScreeningMetadataLocal(ISystemId entity,
                                             ILinksNoarkObject linksNoarkObject) {
        if (null != ((Class) entity).getReferenceScreening()) {
            linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                    HREF_BASE_CLASS + SLASH +
                    entity.getSystemId() + SLASH + NEW_SCREENING_METADATA,
                    REL_FONDS_STRUCTURE_NEW_SCREENING_METADATA));
        }
    }

    /**
     * Cast the ISystemId entity to a Class
     *
     * @param entity the Class
     * @return a Class object
     */
    private Class getClass(@NotNull ISystemId entity) {
        return (Class) entity;
    }
}
