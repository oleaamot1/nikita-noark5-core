package app.webapp.payload.builder.noark5.casehandling;

import app.domain.interfaces.entities.INoarkEntity;
import app.domain.interfaces.entities.ISystemId;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.Link;
import org.springframework.stereotype.Component;

import static app.utils.constants.Constants.*;
import static app.utils.constants.N5ResourceMappings.CORRESPONDENCE_PART_TYPE;

/**
 * Created by tsodring on 2/6/17.
 * <p>
 * Used to add CorrespondencePartLinks links with CorrespondencePart
 * specific information
 */
@Component("correspondencePartUnitLinksBuilder")
public class CorrespondencePartUnitLinksBuilder
        extends CorrespondencePartLinksBuilder {

    @Override
    public void addEntityLinksOnTemplate(ISystemId entity,
                                         ILinksNoarkObject
                                                 linksNoarkObject) {
        addCorrespondencePartType(entity, linksNoarkObject);
    }

    public void addAdministrativeUnit(
            INoarkEntity entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_METADATA + SLASH + CORRESPONDENCE_PART_TYPE,
                REL_METADATA_CORRESPONDENCE_PART_TYPE, false));
    }

    public void addUser(
            INoarkEntity entity, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(entity, new Link(getOutgoingAddress() +
                HREF_BASE_METADATA + SLASH + CORRESPONDENCE_PART_TYPE,
                REL_METADATA_CORRESPONDENCE_PART_TYPE, false));
    }

}
