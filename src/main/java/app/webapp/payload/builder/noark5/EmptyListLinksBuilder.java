package app.webapp.payload.builder.noark5;

import app.webapp.exceptions.NikitaMisconfigurationException;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.Link;
import jakarta.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.util.UrlPathHelper;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static app.utils.constants.Constants.NOARK_BASE_REL;
import static app.utils.constants.Constants.SLASH;

/**
 * Used to add self link to an empty list being returned
 */
@Component
public class EmptyListLinksBuilder
        extends LinksBuilder {

    private static final Logger logger =
            LoggerFactory.getLogger(EmptyListLinksBuilder.class);

    private Pattern pattern = Pattern.compile(".*\\/(\\w+)\\/(\\w+)\\/?$");

    public EmptyListLinksBuilder() {
    }

    @Override
    public void addLinks(ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addSelfLink(
                new Link(getRequestPathAndQueryString(), getRelSelfLink()));
        linksNoarkObject.addSelfLink(
                new Link(getRequestPathAndQueryString(), getRel()));
    }

    private String getRel() {
        RequestAttributes requestAttributes = RequestContextHolder.
                getRequestAttributes();
        HttpServletRequest request =
                ((ServletRequestAttributes) requestAttributes).getRequest();
        String rel = NOARK_BASE_REL;
        if (request != null) {
            String path = new UrlPathHelper().getPathWithinApplication(request);
            Matcher matcher = pattern.matcher(path);
            if (!matcher.matches()) {
                String errorMessage = "Unable to create rel from " + path;
                logger.error(errorMessage);
                throw new NikitaMisconfigurationException(errorMessage);
            }
            rel += matcher.group(1) + SLASH + matcher.group(2) + SLASH;
        }
        return rel;
    }
}
