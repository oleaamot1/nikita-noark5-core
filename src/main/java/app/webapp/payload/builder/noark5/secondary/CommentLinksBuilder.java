package app.webapp.payload.builder.noark5.secondary;

import app.domain.interfaces.entities.ISystemId;
import app.domain.interfaces.entities.secondary.ICommentEntity;
import app.domain.noark5.secondary.Comment;
import app.webapp.payload.builder.interfaces.secondary.ICommentLinksBuilder;
import app.webapp.payload.builder.noark5.SystemIdLinksBuilder;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.Link;
import org.springframework.stereotype.Component;

import static app.utils.constants.Constants.*;
import static app.utils.constants.N5ResourceMappings.*;
import static app.utils.constants.ODataConstants.DOLLAR_FILTER;

/*
 * Used to add CommentLinks links with Comment specific information
 */
@Component("commentLinksBuilder")
public class CommentLinksBuilder
        extends SystemIdLinksBuilder
        implements ICommentLinksBuilder {

    public CommentLinksBuilder() {
    }

    @Override
    public void addSelfLink(ISystemId entity,
                            ILinksNoarkObject linksNoarkObject) {
        String selfHref = getOutgoingAddress() +
                HREF_BASE_FONDS_STRUCTURE + SLASH + COMMENT + SLASH +
                entity.getSystemIdAsString();
        linksNoarkObject.addLink(entity,
                new Link(selfHref, getRelSelfLink()));
        linksNoarkObject.addLink(entity,
                new Link(selfHref, entity.getBaseRel()));
    }

    @Override
    public void addEntityLinks(ISystemId entity,
                               ILinksNoarkObject linksNoarkObject) {
        Comment comment = (Comment) entity;
        addDocumentDescription(comment, linksNoarkObject);
        addRecordEntity(comment, linksNoarkObject);
        addFile(comment, linksNoarkObject);
        addCommentType(comment, linksNoarkObject);
    }

    @Override
    public void addEntityLinksOnTemplate
            (ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        Comment comment = (Comment) entity;
        addCommentType(comment, linksNoarkObject);
    }

    @Override
    // This is an OData query that should give a list of File objects that
    // have an association to this comment
    // noark5v5/arkivstruktur/mappe?$filtermerknad/system_id eq 'SYSTEMID'
    public void addFile(ICommentEntity comment,
                        ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(comment,
                new Link(getOutgoingAddress() + HREF_BASE_FILE +
                        "?" + urlEncode(DOLLAR_FILTER) + "=" +
                        COMMENT + SLASH + SYSTEM_ID +
                        urlEncode(" eq '" + comment.getSystemIdAsString() + "'"),
                        REL_FONDS_STRUCTURE_FILE));
    }

    @Override
    // This is an OData query that should give a list of Record objects that
    // have an association to this comment
    // noark5v5/arkivstruktur/registrering?$filter=contains(merknad/system_id, 'SYSTEMID')
    public void addRecordEntity(ICommentEntity comment,
                                ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(comment,
                new Link(getOutgoingAddress() + HREF_BASE_FILE +
                        "?" + urlEncode(DOLLAR_FILTER) + "=" +
                        COMMENT + SLASH + SYSTEM_ID +
                        urlEncode(" eq '" + comment.getSystemId() + "'"),
                        REL_FONDS_STRUCTURE_RECORD));
    }

    @Override
    // This is an OData query that should give a list of DocumentDescription
    // objects that have an association to this comment
    // noark5v5/arkivstruktur/dokumentbeskrivelse?$filter=contains(merknad/system_id, 'SYSTEMID')
    public void addDocumentDescription(ICommentEntity comment,
                                       ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(comment,
                new Link(getOutgoingAddress() +
                        HREF_BASE_DOCUMENT_DESCRIPTION +
                        "?" + urlEncode(DOLLAR_FILTER) + "=" +
                        COMMENT + SLASH + SYSTEM_ID +
                        urlEncode(" eq '" + comment.getSystemId() + "'"),
                        REL_FONDS_STRUCTURE_DOCUMENT_DESCRIPTION));
    }

    @Override
    public void addCommentType(ICommentEntity comment,
                               ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(comment,
                new Link(getOutgoingAddress() +
                        HREF_BASE_METADATA + SLASH + COMMENT_TYPE,
                        REL_METADATA_COMMENT_TYPE, true));
    }
}
