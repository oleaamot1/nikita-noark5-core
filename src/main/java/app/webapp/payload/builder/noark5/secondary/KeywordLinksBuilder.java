package app.webapp.payload.builder.noark5.secondary;

import app.domain.interfaces.entities.ISystemId;
import app.domain.interfaces.entities.secondary.IKeywordEntity;
import app.webapp.payload.builder.interfaces.secondary.IKeywordLinksBuilder;
import app.webapp.payload.builder.noark5.SystemIdLinksBuilder;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.Link;
import org.springframework.stereotype.Component;

import static app.utils.constants.Constants.*;
import static app.utils.constants.N5ResourceMappings.KEYWORD;
import static app.utils.constants.N5ResourceMappings.SYSTEM_ID;
import static app.utils.constants.ODataConstants.DOLLAR_FILTER;

/**
 * Used to add KeywordLinks links with Keyword specific information
 */
@Component("keywordLinksBuilder")
public class KeywordLinksBuilder
        extends SystemIdLinksBuilder
        implements IKeywordLinksBuilder {

    public KeywordLinksBuilder() {
    }

    @Override
    public void addSelfLink(ISystemId entity,
                            ILinksNoarkObject linksNoarkObject) {
        String selfhref = getOutgoingAddress() + HREF_BASE_FONDS_STRUCTURE +
                SLASH + KEYWORD + SLASH + entity.getSystemId();
        linksNoarkObject.addLink(entity,
                new Link(selfhref, getRelSelfLink()));
        linksNoarkObject.addLink(entity,
                new Link(selfhref, entity.getBaseRel()));
    }

    @Override
    public void addEntityLinks(ISystemId entity,
                               ILinksNoarkObject linksNoarkObject) {
        addClass((IKeywordEntity) entity, linksNoarkObject);
        addRecordEntity((IKeywordEntity) entity, linksNoarkObject);
        addFile((IKeywordEntity) entity, linksNoarkObject);
    }

    /**
     * Create a REL/HREF pair for the parent Record associated with the given
     * Keyword
     * registrering?$filter=noekkelord/systemID eq '{systemID}'
     * <p>
     * "https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/registrering/"
     *
     * @param keyword          Keyword
     * @param linksNoarkObject linksDocumentObject
     */
    @Override
    public void addRecordEntity(IKeywordEntity keyword,
                                ILinksNoarkObject linksNoarkObject) {
        if (keyword.getReferenceRecordEntity().size() > 0) {
            linksNoarkObject.addLink(keyword,
                    new Link(getOutgoingAddress() +
                            HREF_BASE_RECORD + "?" + urlEncode(DOLLAR_FILTER) +
                            "=" + KEYWORD + SLASH + SYSTEM_ID +
                            urlEncode(" eq '" + keyword.getSystemId() + "'"),
                            REL_FONDS_STRUCTURE_RECORD));
        }
    }

    /**
     * Create a REL/HREF pair for the parent Class associated
     * with the given Keyword
     * <p>
     * klasse?$filter=noekkelord/systemID eq '{systemID}'
     * "https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/klasse/"
     *
     * @param keyword          The Keyword object
     * @param linksNoarkObject linksDocumentObject
     */
    @Override
    public void addClass(IKeywordEntity keyword,
                         ILinksNoarkObject linksNoarkObject) {
        if (keyword.getReferenceClass().size() > 0) {
            linksNoarkObject.addLink(keyword,
                    new Link(getOutgoingAddress() +
                            HREF_BASE_CLASS + "?" + urlEncode(DOLLAR_FILTER) +
                            "=" + KEYWORD + SLASH + SYSTEM_ID +
                            urlEncode(" eq '" + keyword.getSystemId() + "'"),
                            REL_FONDS_STRUCTURE_CLASS));
        }
    }

    /**
     * Create a REL/HREF pair for the parent File associated
     * with the given Keyword
     * <p>
     * mappe?$filter=noekkelord/systemID eq '{systemID}'
     * "https://rel.arkivverket.no/noark5/v5/api/arkivstruktur/mappe/"
     *
     * @param keyword            The Keyword object
     * @param linksNoarkObject linksDocumentObject
     */
    @Override
    public void addFile(IKeywordEntity keyword,
                        ILinksNoarkObject linksNoarkObject) {
        if (keyword.getReferenceFile().size() > 0) {
            linksNoarkObject.addLink(keyword,
                    new Link(getOutgoingAddress() +
                            HREF_BASE_FILE + "?" + urlEncode(DOLLAR_FILTER) +
                            "=" + KEYWORD + SLASH + SYSTEM_ID +
                            urlEncode(" eq '" + keyword.getSystemId() + "'"),
                            REL_FONDS_STRUCTURE_FILE));
        }
    }
}
