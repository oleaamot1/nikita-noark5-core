package app.webapp.payload.builder.noark5;

import app.domain.interfaces.entities.ISystemId;
import app.domain.noark5.ChangeLog;
import app.domain.noark5.SystemIdEntity;
import app.webapp.payload.builder.interfaces.IChangeLogLinksBuilder;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.Link;
import org.springframework.stereotype.Component;

import static app.utils.constants.Constants.*;
import static app.utils.constants.N5ResourceMappings.CHANGE_LOG;

/*
 * Used to add ChangeLogLinks links with ChangeLog specific information
 */
@Component("changeLogLinksBuilder")
public class ChangeLogLinksBuilder
        extends SystemIdLinksBuilder
        implements IChangeLogLinksBuilder {

    public ChangeLogLinksBuilder() {
    }

    @Override
    public void addSelfLink(ISystemId entity,
                            ILinksNoarkObject linksNoarkObject) {
        String selfHref = getOutgoingAddress() +
                HREF_BASE_LOGGING + SLASH + CHANGE_LOG + SLASH + entity.getSystemId();
        linksNoarkObject.addLink(entity,
                new Link(selfHref, getRelSelfLink()));
        linksNoarkObject.addLink(entity,
                new Link(selfHref, entity.getBaseRel()));
    }

    @Override
    public void addEntityLinks(ISystemId entity,
                               ILinksNoarkObject linksNoarkObject) {
        ChangeLog changeLog = (ChangeLog) entity;
        addReferenceArchiveUnitLink(changeLog, linksNoarkObject);
    }

    @Override
    public void addEntityLinksOnTemplate
            (ISystemId entity, ILinksNoarkObject linksNoarkObject) {
    }

    protected void addReferenceArchiveUnitLink
            (ChangeLog changeLog, ILinksNoarkObject linksNoarkObject) {
        SystemIdEntity refentity = changeLog.getReferenceArchiveUnit();
        if (null != refentity) {
            String relkey = refentity.getBaseRel();
            String section = HREF_BASE_FONDS_STRUCTURE;
            // TODO Avoid hack to return correct href
            if (-1 != relkey.indexOf(NOARK_CASE_HANDLING_PATH)) {
                section = HREF_BASE_CASE_HANDLING;
            } else if (-1 != relkey.indexOf(NOARK_ADMINISTRATION_PATH)) {
                section = HREF_BASE_ADMIN;
            }
            String refentityhref = getOutgoingAddress()
                + section + SLASH + refentity.getBaseTypeName()
                + SLASH + refentity.getSystemId();
            linksNoarkObject.addLink
                (changeLog, new Link(refentityhref, refentity.getBaseRel()));
        }
    }
}
