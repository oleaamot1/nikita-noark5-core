package app.webapp.payload.builder.noark5.secondary;

import app.domain.interfaces.entities.ISystemId;
import app.webapp.payload.builder.interfaces.secondary.IScreeningMetadataLinksBuilder;
import app.webapp.payload.builder.noark5.SystemIdLinksBuilder;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.Link;
import org.springframework.stereotype.Component;

import static app.utils.constants.Constants.*;
import static app.utils.constants.N5ResourceMappings.SCREENING_METADATA;

@Component
public class ScreeningMetadataLinksBuilder
        extends SystemIdLinksBuilder
        implements IScreeningMetadataLinksBuilder {

    public ScreeningMetadataLinksBuilder() {
    }

    @Override
    public void addSelfLink(ISystemId entity,
                            ILinksNoarkObject linksNoarkObject) {
        String selfHref = getOutgoingAddress() +
                HREF_BASE_FONDS_STRUCTURE + SLASH + SCREENING_METADATA + SLASH +
                entity.getSystemId();
        linksNoarkObject.addLink(entity, new Link(selfHref,
                getRelSelfLink()));
        linksNoarkObject.addLink(entity, new Link(selfHref,
                entity.getBaseRel()));
    }

    @Override
    public void addLinksOnTemplate(
            ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(new Link(getOutgoingAddress() +
                HREF_BASE_METADATA + SLASH + SCREENING_METADATA,
                REL_METADATA_SCREENING_METADATA));
    }
}
