package app.webapp.payload.builder.noark5;

import app.domain.interfaces.entities.ISystemId;
import app.domain.noark5.ChangeLog;
import app.domain.noark5.EventLog;
import app.webapp.payload.builder.interfaces.IEventLogLinksBuilder;
import app.webapp.payload.links.ILinksNoarkObject;
import app.webapp.payload.links.Link;
import org.springframework.stereotype.Component;

import static app.utils.constants.Constants.*;
import static app.utils.constants.N5ResourceMappings.EVENT_LOG;
import static app.utils.constants.N5ResourceMappings.EVENT_TYPE;

/*
 * Used to add EventLogLinks links with EventLog specific information
 */
@Component("eventLogLinksBuilder")
public class EventLogLinksBuilder
        extends ChangeLogLinksBuilder
        implements IEventLogLinksBuilder {

    public EventLogLinksBuilder() {
    }

    @Override
    public void addSelfLink(ISystemId entity,
                            ILinksNoarkObject linksNoarkObject) {
        String selfHref = getOutgoingAddress() +
                HREF_BASE_LOGGING + SLASH + EVENT_LOG + SLASH + entity.getSystemId();
        linksNoarkObject.addLink(entity,
                new Link(selfHref, getRelSelfLink()));
        linksNoarkObject.addLink(entity,
                new Link(selfHref, entity.getBaseRel()));
    }

    @Override
    public void addEntityLinks(ISystemId entity,
                               ILinksNoarkObject linksNoarkObject) {
        EventLog eventLog = (EventLog) entity;
        addReferenceArchiveUnitLink((ChangeLog) eventLog, linksNoarkObject);
        addEventType(eventLog, linksNoarkObject);
    }

    @Override
    public void addEntityLinksOnTemplate
            (ISystemId entity, ILinksNoarkObject linksNoarkObject) {
        EventLog eventLog = (EventLog) entity;
        addEventType(eventLog, linksNoarkObject);
    }

    public void addEventType
            (EventLog eventLog, ILinksNoarkObject linksNoarkObject) {
        linksNoarkObject.addLink(eventLog, new Link(getOutgoingAddress() +
                HREF_BASE_METADATA + SLASH + EVENT_TYPE,
                REL_METADATA_EVENT_TYPE, true));
    }
}
