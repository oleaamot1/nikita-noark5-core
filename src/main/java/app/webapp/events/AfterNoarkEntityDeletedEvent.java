package app.webapp.events;

import app.domain.interfaces.entities.ISystemId;
import jakarta.validation.constraints.NotNull;

/**
 * Base class that can be used to identify DELETE events that can occur in
 * nikita.
 */
public class AfterNoarkEntityDeletedEvent
        extends AfterNoarkEntityEvent {

    public AfterNoarkEntityDeletedEvent(Object source,
                                        @NotNull ISystemId entity) {
        super(source, entity);
    }
}
