package app.webapp.events;

import app.domain.interfaces.entities.ISystemId;
import jakarta.validation.constraints.NotNull;
import org.springframework.context.ApplicationEvent;

/**
 * Base class that can be used to identify various CRUD events that can occur
 * in nikita.
 */
public class AfterNoarkEntityEvent
        extends ApplicationEvent {

    ISystemId entity;

    public AfterNoarkEntityEvent(Object source,
                                 @NotNull ISystemId entity) {
        super(source);
        this.entity = entity;
    }

    public String toString() {
        return entity.toString();
    }

    public ISystemId getEntity() {
        return entity;
    }
}
