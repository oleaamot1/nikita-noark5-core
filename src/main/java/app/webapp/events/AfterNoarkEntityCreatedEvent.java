package app.webapp.events;

import app.domain.interfaces.entities.ISystemId;
import jakarta.validation.constraints.NotNull;

/**
 * Base class that can be used to identify CREATE events that can occur in
 * nikita.
 */
public class AfterNoarkEntityCreatedEvent
        extends AfterNoarkEntityEvent {

    public AfterNoarkEntityCreatedEvent(Object source,
                                        @NotNull ISystemId entity) {
        super(source, entity);
    }
}
