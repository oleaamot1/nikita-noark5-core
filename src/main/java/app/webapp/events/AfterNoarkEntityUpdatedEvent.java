package app.webapp.events;

import app.domain.interfaces.entities.ISystemId;
import jakarta.validation.constraints.NotNull;

/**
 * Base class that can be used to identify UPDATE events that can occur in
 * nikita.
 */
public class AfterNoarkEntityUpdatedEvent
        extends AfterNoarkEntityEvent {

    public AfterNoarkEntityUpdatedEvent(Object source,
                                        @NotNull ISystemId entity) {
        super(source, entity);
    }
}
