package app.webapp.spring.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

import static app.utils.constants.Constants.ROLE_RECORDS_MANAGER;
import static app.utils.constants.Constants.SLASH;
import static app.utils.constants.N5ResourceMappings.FONDS;
import static app.utils.constants.PATHPatterns.PATTERN_METADATA_PATH;
import static app.utils.constants.PATHPatterns.PATTERN_NEW_FONDS_STRUCTURE_ALL;
import static org.springframework.http.HttpMethod.*;

@EnableWebSecurity
@Configuration
public class NikitaWebSecurityConfig {


    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http)
            throws Exception {

        http
                .authorizeHttpRequests(authorize -> authorize
                        .requestMatchers(GET, "/")
                        .permitAll()
                        // GET [api]/metadata/**, public to read basic structure
                        .requestMatchers(GET, PATTERN_METADATA_PATH)
                        .permitAll()
                        .requestMatchers(GET, "/v3/**")
                        .permitAll()
                        .requestMatchers(GET, "/**well-known/**").permitAll()
                        // POST GET [api]/arkivstruktur/ny-*, need role of record keeper
                        .requestMatchers(HttpMethod.POST, PATTERN_NEW_FONDS_STRUCTURE_ALL)
                        .hasAuthority(ROLE_RECORDS_MANAGER)
                        .requestMatchers(GET, PATTERN_NEW_FONDS_STRUCTURE_ALL)
                        .hasAuthority(ROLE_RECORDS_MANAGER)
                        // POST PUT PATCH [api]/arkivstruktur/**, need role of record keeper
                        .requestMatchers(PUT, FONDS + SLASH + "**")
                        .hasAuthority(ROLE_RECORDS_MANAGER)
                        .requestMatchers(PATCH, FONDS + SLASH + "**")
                        .hasAuthority(ROLE_RECORDS_MANAGER)
                        .requestMatchers(DELETE, "/**")
                        .hasAuthority(ROLE_RECORDS_MANAGER)
                        // POST PUT PATCH DELETE [api]/metadata/**, need role of record keeper
                        .requestMatchers(PATCH, PATTERN_METADATA_PATH)
                        .hasAuthority(ROLE_RECORDS_MANAGER)
                        .requestMatchers(PUT, PATTERN_METADATA_PATH)
                        .hasAuthority(ROLE_RECORDS_MANAGER)
                        .requestMatchers(HttpMethod.POST, PATTERN_METADATA_PATH)
                        .hasAuthority(ROLE_RECORDS_MANAGER)
                        .requestMatchers(DELETE, PATTERN_METADATA_PATH)
                        .hasAuthority(ROLE_RECORDS_MANAGER)
                        .anyRequest().authenticated()
                );
        http.oauth2ResourceServer(oauth2 -> oauth2.jwt(
                jwt -> jwt.jwtAuthenticationConverter(JwtUtil::createJwtUser)));
        return http.build();
    }

    // @formatter:on
    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

}
