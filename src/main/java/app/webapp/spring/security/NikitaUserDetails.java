package app.webapp.spring.security;

import app.domain.noark5.admin.Organisation;
import app.domain.noark5.admin.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

/**
 * NikitaUserDetails
 * <p>
 * Allows for the identification of an organisation that a user belongs to.
 * Nikita supports multiple organisations co-existing with a single database.
 */
public class NikitaUserDetails
        implements UserDetails, INikitaUserDetails {

    private final User user;
    private final Collection<? extends GrantedAuthority> authorities;

    public NikitaUserDetails(
            User user, Collection<? extends GrantedAuthority> authorities) {
        this.user = user;
        this.authorities = authorities;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return user.getPassword();
    }

    @Override
    public String getUsername() {
        return user.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return user.isAccountNonExpired();
    }

    @Override
    public boolean isAccountNonLocked() {
        return user.isAccountNonLocked();
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return user.isCredentialsNonExpired();
    }

    @Override
    public boolean isEnabled() {
        return user.isEnabled();
    }

    @Override
    public Organisation getOrganisation() {
        return user.getOrganisation();
    }

    @Override
    public User getUser() {
        return user;
    }
}
