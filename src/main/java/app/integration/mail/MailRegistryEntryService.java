package app.integration.mail;

import app.domain.noark5.DocumentDescription;
import app.domain.noark5.DocumentObject;
import app.domain.noark5.casehandling.RegistryEntry;
import app.domain.noark5.casehandling.secondary.ContactInformation;
import app.domain.noark5.casehandling.secondary.CorrespondencePart;
import app.domain.noark5.casehandling.secondary.CorrespondencePartPerson;
import app.domain.noark5.casehandling.secondary.CorrespondencePartUnit;
import app.domain.repository.admin.IUserRepository;
import app.domain.repository.noark5.v5.IRegistryEntryRepository;
import app.integration.mail.model.Email;
import app.service.application.IPatchService;
import app.service.interfaces.IRegistryEntryService;
import app.service.interfaces.ISequenceNumberGeneratorService;
import app.service.interfaces.admin.IAdministrativeUnitService;
import app.service.interfaces.casehandling.ISignOffService;
import app.service.interfaces.metadata.IMetadataService;
import app.service.interfaces.secondary.ICorrespondencePartService;
import app.service.interfaces.secondary.IDocumentFlowService;
import app.service.interfaces.secondary.IPrecedenceService;
import app.service.noark5.RegistryEntryService;
import app.webapp.payload.builder.interfaces.IRegistryEntryLinksBuilder;
import jakarta.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.nio.file.Paths;
import java.util.List;
import java.util.Set;

import static java.lang.String.format;

@Service
@Profile("mailintegration")
public class MailRegistryEntryService
        extends RegistryEntryService
        implements IRegistryEntryService {
    private static final Logger logger =
            LoggerFactory.getLogger(MailRegistryEntryService.class);
    private final RabbitTemplate rabbitTemplate;
    private final IMailFileService mailFileService;
    @Value("${nikita.download-dir.outgoing}")
    String pathnameDirIn;
    @Value("${nikita.mail-queue.outgoing:outgoing-email}")
    private String outgoingQueue;

    public MailRegistryEntryService(EntityManager entityManager, ApplicationEventPublisher applicationEventPublisher,
                                    IPatchService patchService,
                                    ICorrespondencePartService correspondencePartService,
                                    IDocumentFlowService documentFlowService, IPrecedenceService precedenceService,
                                    IMetadataService metadataService, IRegistryEntryRepository registryEntryRepository,
                                    IRegistryEntryLinksBuilder registryEntryLinksBuilder,
                                    ISequenceNumberGeneratorService numberGeneratorService,
                                    ISignOffService signOffService, IUserRepository userRepository,
                                    IAdministrativeUnitService administrativeUnitService, RabbitTemplate rabbitTemplate, IMailFileService mailFileService) {
        super(entityManager, applicationEventPublisher, patchService, correspondencePartService,
                documentFlowService, precedenceService, metadataService, registryEntryRepository,
                registryEntryLinksBuilder, numberGeneratorService, signOffService, userRepository,
                administrativeUnitService);
        this.rabbitTemplate = rabbitTemplate;
        this.mailFileService = mailFileService;
    }

    /**
     * When the publish event from super is ready, check to see if this is a RegistryEntry that is ready to send an
     * outgoing email. If it is, package everything up as an Email object and send it to the mail queue.
     *
     * @param registryEntry The registryEntry to create a mail object from
     */
    @Override
    public void publishUpdateEvent(RegistryEntry registryEntry) {
        if (registryEntry.getRegistryEntryType().getCode().equalsIgnoreCase("u") &&
                registryEntry.getRegistryEntryStatus().getCode().equalsIgnoreCase("k")) {
            try {
                Email outgoingEmail = getEmailDetails(registryEntry);
                if (logger.isDebugEnabled()) {
                    logger.debug(format("Attempting to send following message to outgoing email queue [%s]", outgoingEmail));
                }
                rabbitTemplate.convertAndSend(outgoingQueue, outgoingEmail);
            } catch (AmqpException e) {
                logger.error(format("Error serialising outgoing email object when sending to queue. Reason [%s]",
                        e.getMessage()));
            }
        }
        super.publishUpdateEvent(registryEntry);
    }

    /**
     * Create a Nikita Email object that can be put in the outgoing mail queue.
     *
     * @param registryEntry to create an email from
     * @return the outgoing Nikita formatted Email object
     */
    private Email getEmailDetails(RegistryEntry registryEntry) {
        Email outgoingEmail = new Email();
        getCorrespondencePartEmails(outgoingEmail, registryEntry);
        getDocuments(outgoingEmail, registryEntry);
        return outgoingEmail;
    }

    /**
     * Given a RegistryEntry object, retrieve all documents that are in archive format. Note. There is an assumption
     * here that the caller will only use this on outgoing emails.
     *
     * @param outgoingEmail The email object to populate information with
     * @param registryEntry The registryEntry to get documents from
     */
    private void getDocuments(Email outgoingEmail, RegistryEntry registryEntry) {
        Set<DocumentDescription> documentDescriptions = registryEntry.getReferenceDocumentDescription();
        for (DocumentDescription documentDescription : documentDescriptions) {
            List<DocumentObject> documentObjects = documentDescription.getReferenceDocumentObject();
            for (DocumentObject documentObject : documentObjects) {
                String fileLocation = documentObject.getReferenceDocumentFile();
                String filename = documentObject.getOriginalFilename();
                if (documentObject.getVariantFormat().getCode().equalsIgnoreCase("a")) {
                    outgoingEmail.addAttachment(mailFileService.writeFileToStorage(Paths.get(fileLocation), filename));
                }
            }
        }
    }

    /**
     * Given a RegistryEntry object, retrieve all correspondence parts. If it is a type that has an email address, copy
     * the email address from the vale.
     *
     * @param outgoingEmail The email object to populate information with
     * @param registryEntry The registryEntry to get correspondence parts from
     */
    private void getCorrespondencePartEmails(Email outgoingEmail, RegistryEntry registryEntry) {
        List<CorrespondencePart> correspondenceParts = registryEntry.getReferenceCorrespondencePart();
        for (CorrespondencePart correspondencePart : correspondenceParts) {
            if (correspondencePart instanceof CorrespondencePartPerson) {
                ContactInformation contactInformation = ((CorrespondencePartPerson) correspondencePart)
                        .getContactInformation();
                outgoingEmail.addRecipient(contactInformation.getEmailAddress());
            } else if (correspondencePart instanceof CorrespondencePartUnit) {
                ContactInformation contactInformation = ((CorrespondencePartUnit) correspondencePart)
                        .getContactInformation();
                outgoingEmail.addRecipient(contactInformation.getEmailAddress());
            }
        }
    }
}
