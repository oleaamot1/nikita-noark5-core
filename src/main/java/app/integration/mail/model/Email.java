package app.integration.mail.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import jakarta.mail.Address;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static java.lang.String.join;

/**
 * Note. This class does not adhere to any EMAIL standard. It is a class that holds something that looks like an EMail.
 * It is likely it will be replaced by a more Email compliant class later.
 */
@JsonSerialize(using = EmailSerializer.class)
public class Email
        implements Serializable {

    private String subject;
    private String from;
    private List<String> recipients = new ArrayList<>();
    private List<String> carbonCopies = new ArrayList<>();
    private String messageText;
    private List<String> attachments = new ArrayList<>();

    public Email(String subject, String from, List<String> recipients, List<String> carbonCopies,
                 String messageText, List<String> attachments) {
        this.subject = subject;
        this.from = from;
        this.recipients = recipients;
        this.carbonCopies = carbonCopies;
        this.messageText = messageText;
        this.attachments = attachments;
    }

    public Email() {
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public List<String> getRecipients() {
        return recipients;
    }

    public void setRecipients(List<String> recipients) {
        this.recipients = recipients;
    }

    public List<String> getCarbonCopies() {
        return carbonCopies;
    }

    public void setCarbonCopies(List<String> carbonCopies) {
        this.carbonCopies = carbonCopies;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public List<String> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<String> attachments) {
        this.attachments = attachments;
    }

    public boolean hasAttachments() {
        return !attachments.isEmpty();
    }

    public void addAttachment(String attachmentPath) {
        this.attachments.add(attachmentPath);
    }

    /**
     * A list of recipients
     *
     * @param addressRecipients a list of address recipients
     */
    public void addAddressRecipients(Address[] addressRecipients) {
        for (Address recipient : addressRecipients) {
            recipients.add(recipient.toString());
        }
    }

    public void addRecipient(String recipient) {
        recipients.add(recipient);
    }

    public void addCarbonCopy(String recipient) {
        carbonCopies.add(recipient);
    }

    /**
     * The Address array object of from addresses. Normally an email address can only contain a single From, but an
     * email could have multiple From headers, and as such the Java library returns an array rather than a single
     * object. The assumption made here is that the first address object is the one we will use if multiple address
     * objects are in the array. This is based on a hunch.
     * This is a utility function taking the array object to make coding elsewhere simpler. The check for null etc., is
     * undertaken here.
     *
     * @param addressesFrom the array containing the From address
     */
    public void addAddressFrom(Address[] addressesFrom) {
        if (addressesFrom != null) {
            for (Address addressFrom : addressesFrom) {
                recipients.add(addressFrom.toString());
            }
        }
    }

    @Override
    public String toString() {
        return "Email{" +
                "subject='" + subject + '\'' +
                ", from='" + from + '\'' +
                ", recipients=" + join(" ", recipients) + '\'' +
                ", carbonCopies=" + join(" ", carbonCopies) + '\'' +
                ", size of messageText='" + messageText.length() + '\'' +
                ", # attachments=" + attachments.size() +
                '}';
    }
}
