package app.service.noark5;

import app.domain.noark5.Fonds;
import app.domain.noark5.FondsCreator;
import app.domain.noark5.metadata.FondsStatus;
import app.domain.repository.noark5.v5.IFondsCreatorRepository;
import app.domain.repository.noark5.v5.IFondsRepository;
import app.service.application.IPatchService;
import app.service.interfaces.IFondsCreatorService;
import app.service.interfaces.metadata.IMetadataService;
import app.webapp.exceptions.NoarkEntityNotFoundException;
import app.webapp.payload.builder.interfaces.IFondsCreatorLinksBuilder;
import app.webapp.payload.builder.interfaces.IFondsLinksBuilder;
import app.webapp.payload.links.FondsCreatorLinks;
import app.webapp.payload.links.FondsLinks;
import jakarta.persistence.EntityManager;
import jakarta.validation.constraints.NotNull;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

import static app.utils.NoarkUtils.NoarkEntity.Create.setFinaliseEntityValues;
import static app.utils.NoarkUtils.NoarkEntity.Create.validateDocumentMedium;
import static app.utils.constants.Constants.INFO_CANNOT_FIND_OBJECT;
import static app.utils.constants.DatabaseConstants.DELETE_FROM_FONDS_CREATOR_FONDS;
import static app.utils.constants.N5ResourceMappings.FONDS_STATUS;
import static app.utils.constants.N5ResourceMappings.FONDS_STATUS_OPEN_CODE;

@Service
public class FondsCreatorService
        extends NoarkService
        implements IFondsCreatorService {

    private final IFondsCreatorRepository fondsCreatorRepository;
    private final IFondsRepository fondsRepository;
    private final IMetadataService metadataService;
    private final IFondsCreatorLinksBuilder fondsCreatorLinksBuilder;
    private final IFondsLinksBuilder fondsLinksBuilder;

    public FondsCreatorService(
            EntityManager entityManager,
            ApplicationEventPublisher applicationEventPublisher,
            IPatchService patchService,
            IFondsCreatorRepository fondsCreatorRepository,
            IFondsRepository fondsRepository,
            IMetadataService metadataService,
            IFondsCreatorLinksBuilder fondsCreatorLinksBuilder,
            IFondsLinksBuilder fondsLinksBuilder) {
        super(entityManager, applicationEventPublisher, patchService);
        this.fondsCreatorRepository = fondsCreatorRepository;
        this.fondsRepository = fondsRepository;
        this.metadataService = metadataService;
        this.fondsCreatorLinksBuilder = fondsCreatorLinksBuilder;
        this.fondsLinksBuilder = fondsLinksBuilder;
    }

    // All CREATE operations

    /**
     * Persists a new fondsCreator object to the database. Some values are set
     * in the incoming payload (e.g. title) and some are set by the core.
     * owner, createdBy, createdDate are automatically set by the core.
     *
     * @param fondsCreator fondsCreator object with some values set
     * @return the newly persisted fondsCreator object
     */
    @Override
    @Transactional
    public FondsCreatorLinks createNewFondsCreator(FondsCreator fondsCreator) {
        return packAsLinks(fondsCreatorRepository.save(fondsCreator));
    }

    @Override
    @Transactional
    public FondsLinks createFondsAssociatedWithFondsCreator(
            UUID systemId, Fonds fonds) {
        FondsCreator fondsCreator =
                getFondsCreatorOrThrow(systemId);
        validateDocumentMedium(metadataService, fonds);
        FondsStatus fondsStatus = (FondsStatus)
                metadataService.findValidMetadataByEntityTypeOrThrow
                        (FONDS_STATUS, FONDS_STATUS_OPEN_CODE, null);
        fonds.setFondsStatus(fondsStatus);
        setFinaliseEntityValues(fonds);
        fonds.addFondsCreator(fondsCreator);
        fondsCreator.addFonds(fonds);
        return packFondsAsLinks(fondsRepository.save(fonds));
    }

    @Override
    public FondsCreatorLinks findAll() {
        return (FondsCreatorLinks) processODataQueryGet();
    }

    @Override
    public FondsCreatorLinks findBySystemId(@NotNull final UUID systemId) {
        return packAsLinks(getFondsCreatorOrThrow(systemId));
    }

    /**
     * Retrieve the list of FondsLinks object associated with
     * the FondsCreator object identified by systemId
     *
     * @param systemId The systemId of the FondsCreator object to retrieve the
     *                 associated FondsLinks
     * @return A FondsLinks list
     */
    @Override
    public FondsLinks findFondsAssociatedWithFondsCreator(
            @NotNull final UUID systemId) {
        return (FondsLinks) processODataQueryGet();
    }

    // All UPDATE operations

    /**
     * Updates a FondsCreator object in the database. First we try to locate the
     * FondsCreator object. If the FondsCreator object does not exist a
     * NoarkEntityNotFoundException exception is thrown that the caller has
     * to deal with.
     * <br>
     * After this the values you are allowed to update are copied from the
     * incomingFondsCreator object to the existingFondsCreator object and the
     * existingFondsCreator object will be persisted to the database when the
     * transaction boundary is over.
     * <p>
     * Note, the version corresponds to the version number, when the object
     * was initially retrieved from the database. If this number is not the
     * same as the version number when re-retrieving the FondsCreator object
     * from the database a NoarkConcurrencyException is thrown. Note. This
     * happens when the call to FondsCreator.setVersion() occurs.
     * <p>
     * Note: fondsCreatorName and fondsCreatorId are not nullable
     *
     * @param systemId             systemId of the incoming fondsCreator object
     * @param incomingFondsCreator the incoming fondsCreator
     * @return the updated fondsCreator object after it is persisted
     */
    @Override
    @Transactional
    public FondsCreatorLinks handleUpdate(@NotNull final UUID systemId,
                                          @NotNull final FondsCreator
                                                  incomingFondsCreator) {
        FondsCreator existingFondsCreator = getFondsCreatorOrThrow(systemId);
        // Here copy all the values you are allowed to copy ....
        existingFondsCreator.setDescription(
                incomingFondsCreator.getDescription());
        if (null != incomingFondsCreator.getFondsCreatorId()) {
            existingFondsCreator.setFondsCreatorId(
                    incomingFondsCreator.getFondsCreatorId());
        }
        if (null != incomingFondsCreator.getFondsCreatorName()) {
            existingFondsCreator.setFondsCreatorName(
                    incomingFondsCreator.getFondsCreatorName());
        }
        // Note setVersion can potentially result in a NoarkConcurrencyException
        // exception as it checks the ETAG value
        existingFondsCreator.setVersion(getETag());
        return packAsLinks(existingFondsCreator);
    }

    // All DELETE operations
    @Override
    @Transactional
    public void deleteEntity(@NotNull final UUID systemId) {
        FondsCreator fondsCreator =
                getFondsCreatorOrThrow(systemId);
        disassociateForeignKeys(fondsCreator,
                DELETE_FROM_FONDS_CREATOR_FONDS);
        deleteEntity(fondsCreator);
    }

    /**
     * Delete all objects belonging to the user identified by ownedBy
     */
    @Override
    @Transactional
    public void deleteAllByOwnedBy() {
        fondsCreatorRepository.deleteByOwnedBy(getOrganisation());
    }

    @Override
    public FondsCreatorLinks generateDefaultFondsCreator() {
        FondsCreator fondsCreator = new FondsCreator();
        fondsCreator.setVersion(-1L, true);
        return packAsLinks(fondsCreator);
    }

    public FondsCreatorLinks packAsLinks(FondsCreator fondsCreator) {
        FondsCreatorLinks fondsCreatorLinks =
                new FondsCreatorLinks(fondsCreator);
        applyLinksAndHeader(fondsCreatorLinks, fondsCreatorLinksBuilder);
        return fondsCreatorLinks;
    }

    /**
     * Create a FondsLinks object from a Fonds object and apply outgoing links
     * and header values.
     * Note: The method is required in this class so that we can avoid a
     * circular dependency on nikita.service classes (Fonds->FondsCreator->Fonds)
     *
     * @param fonds the fonds object
     * @return a FondsLinks object
     */
    public FondsLinks packFondsAsLinks(Fonds fonds) {
        FondsLinks fondsLinks =
                new FondsLinks(fonds);
        applyLinksAndHeader(fondsLinks, fondsLinksBuilder);
        return fondsLinks;
    }

    // All HELPER operations

    /**
     * Internal helper method. Rather than having a find and try catch in
     * multiple methods, we have it here once. Note. If you call this,  you
     * will only ever get a valid FondsCreator back. If there is no valid
     * FondsCreator, an exception is thrown
     *
     * @param systemId systemId of the fondsCreator object to retrieve
     * @return the fondsCreator object
     */
    protected FondsCreator getFondsCreatorOrThrow(
            @NotNull final UUID systemId) {
        FondsCreator fondsCreator = fondsCreatorRepository.
                findBySystemId(systemId);
        if (fondsCreator == null) {
            String error = INFO_CANNOT_FIND_OBJECT +
                    " FondsCreator, using systemId " + systemId;
            throw new NoarkEntityNotFoundException(error);
        }
        return fondsCreator;
    }
}
