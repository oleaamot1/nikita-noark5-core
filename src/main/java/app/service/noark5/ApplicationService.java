package app.service.noark5;

import app.domain.noark5.admin.Organisation;
import app.domain.noark5.admin.User;
import app.webapp.exceptions.NikitaMisconfigurationException;
import jakarta.persistence.EntityManager;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.util.List;
import java.util.Objects;

import static app.utils.CommonUtils.WebUtils.getMethodsForRequestAsListOrThrow;
import static app.utils.constants.Constants.*;
import static org.springframework.http.HttpHeaders.ALLOW;
import static org.springframework.http.HttpHeaders.ETAG;

@Service
public class ApplicationService {

    protected final EntityManager entityManager;
    @Value("${nikita.server.links.publicAddress}")
    private String publicAddress;
    @Value("${server.servlet.context-path}")
    private String contextPath;

    public ApplicationService(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    protected String getServletPath() {
        return ((ServletRequestAttributes)
                Objects.requireNonNull(
                        RequestContextHolder.getRequestAttributes()))
                .getRequest().getServletPath();
    }

    protected Long getETag() {
        return Long.parseLong(((ServletRequestAttributes)
                Objects.requireNonNull(
                        RequestContextHolder.getRequestAttributes()))
                .getRequest().getHeader(ETAG).replaceAll("^\"|\"$", ""));
    }

    protected HttpServletRequest getRequest() {
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        if (null == requestAttributes) {
            throw new NikitaMisconfigurationException("Unable to get Request object");
        }
        return ((ServletRequestAttributes) RequestContextHolder
                .getRequestAttributes()).getRequest();
    }

    protected HttpServletResponse getResponse() {
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        if (null == requestAttributes) {
            throw new NikitaMisconfigurationException("Unable to get Response object");
        }
        return ((ServletRequestAttributes) requestAttributes).getResponse();
    }

    protected String getMethod() {
        HttpServletRequest request = getRequest();
        String method = request.getMethod().toLowerCase();
        if (method.equals("get")) {
            String url = request.getRequestURL().toString();
            if (url.contains(NEW + DASH) || url.contains(EXPAND_TO)) {
                return "get-template";
            }
        }
        return method;
    }

    protected String getUser() {
        Authentication authentication =
                SecurityContextHolder.getContext().
                        getAuthentication();
        Object userDetailsObject = authentication.getPrincipal();
        if (userDetailsObject instanceof Jwt jwt) {
            String username = (String) jwt.getClaims().get("preferred_username");
            if (username != null && !username.isBlank()) {
                return username;
            } else {
                throw new NikitaMisconfigurationException("Could not find user " + username);
            }
        }
        return SecurityContextHolder.getContext().getAuthentication().getName();
    }

    protected Organisation getOrganisation() {
        String username = getUser();
        List<User> user = entityManager.createQuery("SELECT user from User as user where user.username = ?1")
                .setParameter(1, username)
                .getResultList();
        return user.get(0).getOrganisation();
    }

    /**
     * Get the outgoing address to use when generating links.
     * If we are not running behind a front facing server incoming requests
     * will not have X-Forward-* values set. In this case use the hardcoded
     * value from the properties file.
     * <p>
     * If X-Forward-*  values are set, then use them. At a minimum Host and
     * Proto must be set. If Port is also set use this to.
     *
     * @return the outgoing address
     */
    protected String getOutgoingAddress() {
        HttpServletRequest request = getRequest();
        String address = request.getHeader("X-Forwarded-Host");
        String protocol = request.getHeader("X-Forwarded-Proto");
        String port = request.getHeader("X-Forwarded-Port");

        if (address != null && protocol != null) {
            if (port != null) {
                return protocol + "://" + address + ":" + port + contextPath +
                        SLASH;
            } else {
                return protocol + "://" + address + contextPath + SLASH;
            }
        } else {
            return publicAddress + contextPath + SLASH;
        }
    }

    public String getContextPath() {
        return contextPath;
    }


    /**
     * Set the outgoing ALLOW header
     */
    protected void setOutgoingRequestHeaderList() {
        HttpServletResponse response = ((ServletRequestAttributes)
                Objects.requireNonNull(
                        RequestContextHolder.getRequestAttributes()))
                .getResponse();
        HttpServletRequest request =
                ((ServletRequestAttributes)
                        RequestContextHolder
                                .getRequestAttributes()).getRequest();
        if (response != null) {
            response.addHeader(ALLOW, getMethodsForRequestAsListOrThrow(
                    request.getServletPath()));
        } else {
            throw new NikitaMisconfigurationException("Could not get outgoing respone to set ALLOW headers");
        }
    }
}
