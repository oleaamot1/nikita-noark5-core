package app.service.interfaces;

import app.domain.noark5.ChangeLog;
import app.webapp.payload.links.ChangeLogLinks;
import jakarta.validation.constraints.NotNull;

import java.util.UUID;

public interface IChangeLogService {

    ChangeLogLinks generateDefaultChangeLog();

    ChangeLogLinks createNewChangeLog(@NotNull final ChangeLog changeLog);

    ChangeLogLinks findChangeLogByOwner();

    ChangeLogLinks findSingleChangeLog(@NotNull final UUID systemId);

    ChangeLogLinks handleUpdate(@NotNull final UUID systemId,
                                @NotNull ChangeLog incomingChangeLog);

    void deleteEntity(@NotNull final UUID systemId);
}
