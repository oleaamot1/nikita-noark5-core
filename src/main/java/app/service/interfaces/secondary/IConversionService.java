package app.service.interfaces.secondary;

import app.domain.noark5.DocumentObject;
import app.domain.noark5.secondary.Conversion;
import app.webapp.payload.links.secondary.ConversionLinks;
import jakarta.validation.constraints.NotNull;

import java.util.UUID;

public interface IConversionService {

    void deleteConversion(@NotNull final UUID systemId);

    Conversion findConversionBySystemId(@NotNull final UUID systemId);

    ConversionLinks findBySystemId(@NotNull final UUID systemId);

    ConversionLinks generateDefaultConversion(
            @NotNull final UUID systemId,
            @NotNull final DocumentObject documentObject);

    ConversionLinks packAsLinks(@NotNull final Conversion conversion);

    ConversionLinks save(@NotNull final Conversion conversion);
}
