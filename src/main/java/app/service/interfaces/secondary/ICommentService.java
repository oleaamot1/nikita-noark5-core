package app.service.interfaces.secondary;

import app.domain.noark5.DocumentDescription;
import app.domain.noark5.File;
import app.domain.noark5.RecordEntity;
import app.domain.noark5.secondary.Comment;
import app.webapp.payload.links.secondary.CommentLinks;
import jakarta.validation.constraints.NotNull;

import java.util.UUID;

public interface ICommentService {

    CommentLinks generateDefaultComment(@NotNull final UUID systemId);

    CommentLinks createNewComment(
            @NotNull final Comment comment,
            @NotNull final File file);

    CommentLinks createNewComment(
            @NotNull final Comment comment,
            @NotNull final RecordEntity record);

    CommentLinks createNewComment(
            @NotNull final Comment comment,
            @NotNull final DocumentDescription documentDescription);

    CommentLinks findSingleComment(@NotNull final UUID commentSystemId);

    CommentLinks handleUpdate(@NotNull final UUID systemId,
                              @NotNull final Comment incomingComment);

    void deleteComment(@NotNull final UUID systemId);
}
