package app.service.interfaces;

import app.domain.noark5.File;
import app.domain.noark5.RecordEntity;
import app.domain.noark5.nationalidentifier.*;
import app.webapp.payload.links.nationalidentifier.*;
import jakarta.validation.constraints.NotNull;

import java.util.UUID;

public interface INationalIdentifierService {

    BuildingLinks createNewBuilding(
            @NotNull final Building building,
            @NotNull final RecordEntity record);

    BuildingLinks createNewBuilding(
            @NotNull final Building building,
            @NotNull final File file);

    CadastralUnitLinks createNewCadastralUnit(
            @NotNull final CadastralUnit unit,
            @NotNull final RecordEntity record);

    CadastralUnitLinks createNewCadastralUnit(
            @NotNull final CadastralUnit unit,
            @NotNull final File file);

    DNumberLinks createNewDNumber(
            @NotNull final DNumber dNumber,
            @NotNull final RecordEntity record);

    DNumberLinks createNewDNumber(
            @NotNull final DNumber dNumber,
            @NotNull final File file);

    PlanLinks createNewPlan(
            @NotNull final Plan plan,
            @NotNull final RecordEntity record);

    PlanLinks createNewPlan(
            @NotNull final Plan plan,
            @NotNull final File file);

    PositionLinks createNewPosition(
            @NotNull final Position position,
            @NotNull final RecordEntity record);

    PositionLinks createNewPosition(
            @NotNull final Position position,
            @NotNull final File file);

    SocialSecurityNumberLinks createNewSocialSecurityNumber(
            @NotNull final SocialSecurityNumber socialSecurityNumber,
            @NotNull final RecordEntity record);

    SocialSecurityNumberLinks createNewSocialSecurityNumber(
            @NotNull final SocialSecurityNumber socialSecurityNumber,
            @NotNull final File file);

    UnitLinks createNewUnit(
            @NotNull final Unit unit,
            @NotNull final RecordEntity record);

    UnitLinks createNewUnit(
            @NotNull final Unit unit,
            @NotNull final File file);

    BuildingLinks findBuildingBySystemId(@NotNull final UUID systemId);

    UnitLinks findUnitBySystemId(@NotNull final UUID systemId);

    PositionLinks findPositionBySystemId(@NotNull final UUID systemId);

    PlanLinks findPlanBySystemId(@NotNull final UUID systemId);

    SocialSecurityNumberLinks findSocialSecurityNumberBySystemId(
            @NotNull final UUID systemId);

    DNumberLinks findDNumberBySystemId(@NotNull final UUID systemId);

    CadastralUnitLinks findCadastralUnitBySystemId(
            @NotNull final UUID systemId);

    // All PUT/UPDATE operations

    BuildingLinks updateBuilding(
            @NotNull final UUID systemId, @NotNull final Building incomingBuilding);

    CadastralUnitLinks updateCadastralUnit(
            @NotNull final UUID systemId, @NotNull final CadastralUnit incomingCadastralUnit);

    DNumberLinks updateDNumber(
            @NotNull final UUID systemId, @NotNull final DNumber incomingDNumber);

    PlanLinks updatePlan(
            @NotNull final UUID systemId, @NotNull final Plan incomingPlan);

    PositionLinks updatePosition(
            @NotNull final UUID systemId, @NotNull final Position incomingPosition);

    SocialSecurityNumberLinks updateSocialSecurityNumber(
            @NotNull final UUID systemId, @NotNull final SocialSecurityNumber incomingSocialSecurityNumber);

    UnitLinks updateUnit(
            @NotNull final UUID systemId, @NotNull final Unit incomingUnit);

    // All DELETE operations

    void deleteBuilding(@NotNull final UUID systemId);

    void deleteCadastralUnit(@NotNull final UUID systemId);

    void deleteDNumber(@NotNull final UUID systemId);

    void deletePlan(@NotNull final UUID systemId);

    void deletePosition(@NotNull final UUID systemId);

    void deleteSocialSecurityNumber(@NotNull final UUID systemId);

    void deleteUnit(@NotNull final UUID systemId);

    // All template operations
    BuildingLinks generateDefaultBuilding(@NotNull final UUID systemId);

    CadastralUnitLinks generateDefaultCadastralUnit(
            @NotNull final UUID systemId);

    DNumberLinks generateDefaultDNumber(@NotNull final UUID systemId);

    PlanLinks generateDefaultPlan(@NotNull final UUID systemId);

    PositionLinks generateDefaultPosition(@NotNull final UUID systemId);

    SocialSecurityNumberLinks generateDefaultSocialSecurityNumber(
            @NotNull final UUID systemId);

    UnitLinks generateDefaultUnit(@NotNull final UUID systemId);
}
